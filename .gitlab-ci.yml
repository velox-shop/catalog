image: eclipse-temurin:21.0.4_7-jdk-alpine

variables:
  GRADLE_USER_HOME: "${CI_PROJECT_DIR}/.gradle"

stages:
  - preparation
  - build
  - test
  - release
  - publish
  - pages

services:
  - docker:26-dind

before_script:
  - echo "Start CI/CD"
  - export GRADLE_USER_HOME=`pwd`/.gradle

cache:
  paths:
    - "${GRADLE_USER_HOME}/caches/"
    - "${GRADLE_USER_HOME}/wrapper/"
    - "${GRADLE_USER_HOME}/build-cache/"

##
# download gradle build dependencies to speed up the next build steps
##
download_dependencies:
  stage: preparation
  script:
    - echo "${GRADLE_USER_HOME}"
    - ./gradlew -version
    - ./gradlew dependencies --refresh-dependencies
  interruptible: true


build:
  image: eclipse-temurin:21.0.4_7-jdk
  stage: build
  script:
    - echo "Start build"
    - ./gradlew --refresh-dependencies build --stacktrace
    - ./gradlew publicJar
  artifacts:
    when: always
    paths:
      - build/reports
      - build/libs/*.jar
      - build/libs/api/*.jar
      - documentation/openapi.yaml
    expire_in: 1 week
  when: always

test:
  stage: test
  image:
    name: docker:26
  script:
    - echo $DOCKER_HOST
    - >
      wget --quiet https://cdn.azul.com/public_keys/alpine-signing@azul.com-5d5dc44c.rsa.pub -P /etc/apk/keys/
      && echo "https://repos.azul.com/zulu/alpine" >> /etc/apk/repositories
    - apk update
    - >
      apk add --no-cache
      nodejs
      npm
      yarn
      curl
      zulu11-jdk
    - node --version
    - export JAVA_HOME=/usr/lib/jvm/zulu11-ca
    - java --version
    - npm install -g newman newman-reporter-html redoc-cli
    - newman --version
    - echo "Start application in background"
    - docker-compose up --detach
    - mkdir logs
    - docker-compose logs --follow mongoservices > logs/mongoservices.txt &
    - docker-compose logs --follow catalogwebapp > logs/catalogwebapp.txt &
    - echo $DOCKER_HOST
    - src/main/shell/wait-for-it.sh 8454
    # Use newman to run postman tests
    - for file in src/test/*postman_collection.json; do newman run $file --environment src/test/*-docker.postman_environment.json --reporters cli,html --reporter-html-export "newman-results-$(basename $file .json).html"; done
    # Get Swagger Documentation
    - mkdir build/docs
    - wget http://docker:8454/catalog/v1/api-docs -O build/docs/swagger.json
    - redoc-cli bundle -o swagger/index.html build/docs/swagger.json
    - echo "Stop application"
    - docker-compose down
  artifacts:
    when: always
    paths:
      - swagger
      - newman-results*.html
      - logs
      - build/docs
      - public

release:
  stage: release
  only:
    - main
  image: node:20
  variables:
    GIT_STRATEGY: clone
  before_script:
    - ./src/main/shell/setup_git.sh
  script:
    - npx --yes -p semantic-release@19 -p @semantic-release/exec@6 -p @semantic-release/git@10 semantic-release
  artifacts:
    paths:
      - gradle.properties
    expire_in: 1 day

publish-docker:
  image: slyswiss/dockerjavaimage:java21
  services:
    - docker:26.1-dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  stage: publish
  script:
    - >
      ./gradlew publishDocker
      -PregistryUser="$CI_REGISTRY_USER"
      -PregistryPassword="$CI_REGISTRY_PASSWORD"
      -Pregistry=$CI_REGISTRY
      -PregistryImage="$CI_REGISTRY_IMAGE"
    - apk add curl
    - version=`./gradlew properties -q | awk '/^version:/ {print $2}'`
    - ./src/main/shell/slack.sh "new image available -> $CI_REGISTRY_IMAGE:$version"
  only:
    - main

publish-maven:
  stage: publish
  script:
    - ./gradlew publicJar
    - ./gradlew publish
  only:
    - main

pages:
  stage: pages
  dependencies:
    - test
  script:
    - echo "Publishing Pages"
    - mkdir public
    - mkdir public/postman
    - cp -r swagger public
    - cp src/test/*postman_collection.json public/postman
    - echo "<html><a href="./swagger/index.html">API documentation</a><br/></html>" > public/index.html
  artifacts:
    paths:
      - public
  only:
    - main

after_script:
  - echo "End CI/CD"
