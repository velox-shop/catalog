package shop.velox.catalog.dao;

import static java.time.LocalDateTime.now;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.data.domain.Pageable.unpaged;

import jakarta.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.opentest4j.AssertionFailedError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import shop.velox.catalog.AbstractIntegrationTest;
import shop.velox.catalog.model.CategoryEntity;
import shop.velox.catalog.model.CategoryEntity.Fields;
import shop.velox.catalog.model.CategoryStatus;

@Slf4j
class CategoryRepositoryTest extends AbstractIntegrationTest {

  @Autowired
  CategoryRepository categoryRepository;

  @Nested
  @TestInstance(TestInstance.Lifecycle.PER_CLASS)
  class FindTest extends AbstractIntegrationTest {

    public static final String CATALOG_CODE = "catalogCode";
    public static final String PRODUCT_CODE = "product1";

    public static LocalDateTime start;

    @BeforeAll
    @SneakyThrows
    void setUp() {
      categoryRepository.save(CategoryEntity.builder()
          .code("oldCategory")
          .catalogCode(CATALOG_CODE)
          .build());

      TimeUnit.SECONDS.sleep(1);
      start = now();

      categoryRepository.save(CategoryEntity.builder()
          .code("category1")
          .catalogCode(CATALOG_CODE)
          .productCodes(List.of(PRODUCT_CODE, "otherProduct"))
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code("category2")
          .catalogCode(CATALOG_CODE)
          .productCodes(List.of("otherProduct"))
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code("category3")
          .catalogCode("otherCatalogCode")
          .productCodes(List.of())
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code("category4")
          .catalogCode(CATALOG_CODE)
          .productCodes(List.of(PRODUCT_CODE, "otherProduct"))
          .status(CategoryStatus.INACTIVE)
          .build());
    }

    @ParameterizedTest
    @MethodSource("findArguments")
    void find(String productCode, CategoryStatus status, LocalDateTime after,
        List<String> expectedCategoryCodes) {

      var foundCategories = categoryRepository.find(CATALOG_CODE, productCode, status, after,
          unpaged()).getContent();

      assertThat(foundCategories, Matchers.hasSize(expectedCategoryCodes.size()));

      List<String> foundCategoryCodes = foundCategories.stream()
          .map(CategoryEntity::getCode)
          .toList();

      assertThat(foundCategoryCodes, Matchers.containsInAnyOrder(expectedCategoryCodes.toArray()));
    }

    public Stream<Arguments> findArguments() {
      return Stream.of(
          Arguments.of(null, null, null,
              List.of("oldCategory", "category1", "category2", "category4")),
          Arguments.of(null, null, start, List.of("oldCategory")),
          Arguments.of(PRODUCT_CODE, null, null, List.of("category1", "category4")),
          Arguments.of(PRODUCT_CODE, CategoryStatus.ACTIVE, null, List.of("category1")),
          Arguments.of(PRODUCT_CODE, CategoryStatus.INACTIVE, null, List.of("category4")),
          Arguments.of(null, CategoryStatus.ACTIVE, null,
              List.of("oldCategory", "category1", "category2"))
      );
    }
  }

  @Test
  void saveWithoutCodeShouldFailTest() {

    CategoryEntity category = CategoryEntity.builder()
        .catalogCode("catalogCode")
        .build();

    ConstraintViolationException thrown = assertThrows(
        ConstraintViolationException.class,
        () -> categoryRepository.save(category));

    assertTrue(thrown.getConstraintViolations().stream()
        .anyMatch(constraintViolation -> constraintViolation.getPropertyPath().toString().contains(
            Fields.code)));
  }

  @Test
  void saveWithoutCatalogCodeShouldFailTest() {

    CategoryEntity category = CategoryEntity.builder()
        .code("code")
        .build();

    ConstraintViolationException thrown = assertThrows(
        ConstraintViolationException.class,
        () -> categoryRepository.save(category));

    assertTrue(thrown.getConstraintViolations().stream()
        .anyMatch(constraintViolation -> constraintViolation.getPropertyPath().toString().contains(
            Fields.catalogCode)));
  }

  @Test
  void avoidCodeAndCatalogCodeDuplicates() {

    Supplier<CategoryEntity> categorySupplier = () -> CategoryEntity.builder()
        .code("code")
        .catalogCode("catalogCode")
        .build();

    CategoryEntity category1 = categorySupplier.get();
    categoryRepository.save(category1);

    CategoryEntity category2 = categorySupplier.get();
    DuplicateKeyException thrown = assertThrows(DuplicateKeyException.class,
        () -> categoryRepository.save(category2));

    assertThat(thrown.getMessage(),
        Matchers.containsString(CategoryEntity.CATALOG_CODE_AND_CODE_INDEX_NAME));

  }

  @Nested
  @TestInstance(TestInstance.Lifecycle.PER_CLASS)
  class DocumentReferenceTest extends AbstractIntegrationTest {

    private static final String catalogCode = "catalogCode";

    private static final String CATEGORY_1_CODE = "1";
    private static final String CATEGORY_1_1_CODE = "1_1";
    private static final String CATEGORY_2_CODE = "2";
    private static final String CATEGORY_2_1_CODE = "2_1";
    private static final String CATEGORY_2_2_CODE = "2_2";
    private static final String CATEGORY_2_1_1_CODE = "2_1_1";

    @BeforeAll
    void setUp() {

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_1_CODE)
          .catalogCode(catalogCode)
          // .subCategoryCodes(List.of(CATEGORY_1_1_CODE))
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_1_1_CODE)
          .catalogCode(catalogCode)
          .parentCode(CATEGORY_1_CODE)
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_2_CODE)
          .catalogCode(catalogCode)
          // .subCategoryCodes(List.of(CATEGORY_2_1_CODE, CATEGORY_2_2_CODE))
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_2_1_CODE)
          .catalogCode(catalogCode)
          .parentCode(CATEGORY_2_CODE)
          // .subCategoryCodes(List.of(CATEGORY_2_1_1_CODE))
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_2_1_1_CODE)
          .catalogCode(catalogCode)
          .parentCode(CATEGORY_2_1_CODE)
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_2_2_CODE)
          .catalogCode(catalogCode)
          .parentCode(CATEGORY_2_CODE)
          .build());
    }

    @ParameterizedTest
    @MethodSource("documentReferenceTestArguments")
    void documentReferenceTest(String categoryCode, List<String> expectedeSubCategoriesCodes) {
      CategoryEntity category = categoryRepository.findByCodeAndCatalogCode(
              categoryCode, catalogCode)
          .orElseThrow(() -> new AssertionFailedError("category " + categoryCode + " not found"));
      List<String> subCategoriesCodes = category.getSubCategories()
          .stream()
          .map(CategoryEntity::getCode)
          .collect(Collectors.toList());
      assertEquals(expectedeSubCategoriesCodes, subCategoriesCodes);
    }

    private static Stream<Arguments> documentReferenceTestArguments() {
      return Stream.of(
          Arguments.of(CATEGORY_1_CODE, List.of(CATEGORY_1_1_CODE)),
          Arguments.of(CATEGORY_1_1_CODE, List.of()),
          Arguments.of(CATEGORY_2_CODE, List.of(CATEGORY_2_1_CODE, CATEGORY_2_2_CODE)),
          Arguments.of(CATEGORY_2_1_CODE, List.of(CATEGORY_2_1_1_CODE)),
          Arguments.of(CATEGORY_2_1_1_CODE, List.of())
      );
    }
  }

  @Test
  @SneakyThrows
  void datesTest() {
    String catalogCode = RandomStringUtils.randomAlphanumeric(10);
    String categoryCode = RandomStringUtils.randomAlphanumeric(10);

    categoryRepository.save(CategoryEntity.builder()
        .catalogCode(catalogCode)
        .code(categoryCode)
        .build());

    var firstSave = categoryRepository.findByCodeAndCatalogCode(categoryCode, catalogCode).get();
    log.info("firstSave: {}", firstSave);

    assertNotNull(firstSave);
    assertNotNull(firstSave.getId());
    LocalDateTime createdDateTimeBeforeSecondSave = firstSave.getCreatedDateTime();
    assertNotNull(createdDateTimeBeforeSecondSave);
    LocalDateTime lastModifiedDateTimeBeforeSecondSave = firstSave.getLastModifiedDateTime();
    assertNotNull(lastModifiedDateTimeBeforeSecondSave);

    TimeUnit.SECONDS.sleep(2);

    var secondSave = categoryRepository.save(firstSave);
    log.info("secondSave: {}", secondSave);
    assertThat(secondSave.getLastModifiedDateTime(),
        greaterThan(lastModifiedDateTimeBeforeSecondSave));
    assertEquals(createdDateTimeBeforeSecondSave, secondSave.getCreatedDateTime());
  }


}
