package shop.velox.catalog.dao;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import shop.velox.catalog.AbstractIntegrationTest;
import shop.velox.catalog.model.CategoryEntity;

@TestInstance(Lifecycle.PER_CLASS)
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
class CategoryRepositorySubcategoriesTest extends AbstractIntegrationTest {

  public static final List<String> SUB_CATEGORY_CODES_OF_1_1_1 = List.of(
      "1_1_1_3",
      "1_1_1_2",
      "1_1_1_1");

  @Autowired
  CategoryRepository categoryEntityRepository;

  private static Stream<Arguments> subCategoriesTestArguments() {
    return Stream.of(
        Arguments.of("1", List.of("1_1")),
        Arguments.of("1_1_1", SUB_CATEGORY_CODES_OF_1_1_1),
        Arguments.of("2", emptyList()),
        Arguments.of("3", emptyList()),
        Arguments.of("4", emptyList())
    );
  }

  @ParameterizedTest
  @MethodSource("subCategoriesTestArguments")
  void subCategoriesTest(String categoryCode, List<String> expectedCodesFromSubCategories) {
    List<CategoryEntity> actualSubCategories = getSubCategories(categoryCode);
    List<String> actualSubCategoryCodes = actualSubCategories.stream()
        .map(CategoryEntity::getCode)
        .collect(Collectors.toList());
    assertEquals(expectedCodesFromSubCategories, actualSubCategoryCodes);
  }

  @BeforeAll
  void createAll() {
    categoryEntityRepository.save(
        CategoryEntity.builder()
            .code("0")
            .catalogCode("catalogCode")
            .build());

    CategoryEntity c1 = categoryEntityRepository.save(
        CategoryEntity.builder()
            .code("1")
            .subCategoryCodes(List.of("1_1"))
            .catalogCode("catalogCode")
            .build());

    CategoryEntity c1_1 = categoryEntityRepository.save(
        CategoryEntity.builder()
            .code("1_1")
            .parentCode(c1.getCode())
            .subCategoryCodes(List.of("1_1_1"))
            .catalogCode("catalogCode")
            .build());

    CategoryEntity c1_1_1 = categoryEntityRepository.save(
        CategoryEntity.builder()
            .code("1_1_1")
            .parentCode(c1_1.getCode())
            .subCategoryCodes(SUB_CATEGORY_CODES_OF_1_1_1)
            .catalogCode("catalogCode")
            .build());

    CategoryEntity c1_1_1_1 = categoryEntityRepository.save(
        CategoryEntity.builder()
            .code("1_1_1_1")
            .parentCode(c1_1_1.getCode())
            .catalogCode("catalogCode")
            .build());

    CategoryEntity c1_1_1_2 = categoryEntityRepository.save(
        CategoryEntity.builder()
            .code("1_1_1_2")
            .parentCode(c1_1_1.getCode())
            .catalogCode("catalogCode")
            .build());

    CategoryEntity c1_1_1_3 = categoryEntityRepository.save(
        CategoryEntity.builder()
            .code("1_1_1_3")
            .parentCode(c1_1_1.getCode())
            .catalogCode("catalogCode")
            .build());

    categoryEntityRepository.save(
        CategoryEntity.builder()
            .code("2")
            .catalogCode("catalogCode")
            .build());

    categoryEntityRepository.save(
        CategoryEntity.builder()
            .code("3")
            .catalogCode("catalogCode")
            .parentCode("notExisting")
            .build());

    categoryEntityRepository.save(
        CategoryEntity.builder().code("4")
            .catalogCode("catalogCode")
            .subCategoryCodes(List.of("notExisting"))
            .build());
  }

  List<CategoryEntity> getSubCategories(String code) {
    Optional<CategoryEntity> optionalCategory = categoryEntityRepository.findByCodeAndCatalogCode(
        code, "catalogCode");
    return optionalCategory
        .map(CategoryEntity::getSubCategories)
        .orElse(emptyList());
  }

}
