package shop.velox.catalog.dao;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import jakarta.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import shop.velox.catalog.AbstractIntegrationTest;
import shop.velox.catalog.model.ProductEntity;
import shop.velox.catalog.model.ProductEntity.Fields;
import shop.velox.catalog.model.ProductType;

@Slf4j
class ProductRepositoryTest extends AbstractIntegrationTest {

  @Autowired
  ProductRepository productRepository;


  @Test
  void saveWithoutCodeShouldFailTest() {

    ProductEntity product = ProductEntity.builder()
        .catalogCode("catalogCode")
        .type(ProductType.SIMPLE_PRODUCT)
        .build();

    ConstraintViolationException thrown = assertThrows(
        ConstraintViolationException.class,
        () -> productRepository.save(product));

    assertTrue(thrown.getConstraintViolations().stream()
        .anyMatch(constraintViolation -> constraintViolation.getPropertyPath().toString().contains(
            Fields.code)));
  }

  @Test
  void saveWithoutCatalogCodeShouldFailTest() {

    ProductEntity product = ProductEntity.builder()
        .code("code")
        .type(ProductType.SIMPLE_PRODUCT)
        .build();

    ConstraintViolationException thrown = assertThrows(
        ConstraintViolationException.class,
        () -> productRepository.save(product));

    assertTrue(thrown.getConstraintViolations().stream()
        .anyMatch(constraintViolation -> constraintViolation.getPropertyPath().toString().contains(
            Fields.catalogCode)));
  }

  @Test
  void avoidCodeAndCatalogCodeDuplicates() {

    Supplier<ProductEntity> productSupplier = () -> ProductEntity.builder()
        .code("code")
        .catalogCode("catalogCode")
        .type(ProductType.SIMPLE_PRODUCT)
        .build();

    ProductEntity product1 = productSupplier.get();
    productRepository.save(product1);

    ProductEntity product2 = productSupplier.get();
    DuplicateKeyException thrown = assertThrows(DuplicateKeyException.class,
        () -> productRepository.save(product2));

    assertThat(thrown.getMessage(),
        Matchers.containsString(ProductEntity.CATALOG_CODE_AND_CODE_INDEX_NAME));

  }

  @Test
  @SneakyThrows
  void datesTest() {
    String catalogCode = RandomStringUtils.randomAlphanumeric(10);
    String productCode = RandomStringUtils.randomAlphanumeric(10);

    productRepository.save(ProductEntity.builder()
        .catalogCode(catalogCode)
        .code(productCode)
        .type(ProductType.SIMPLE_PRODUCT)
        .build());

    var firstSave = productRepository.findOneByCodeAndCatalogCode(productCode, catalogCode).get();
    log.info("firstSave: {}", firstSave);

    assertNotNull(firstSave);
    assertNotNull(firstSave.getId());
    LocalDateTime createdDateTimeBeforeSecondSave = firstSave.getCreatedDateTime();
    assertNotNull(createdDateTimeBeforeSecondSave);
    LocalDateTime lastModifiedDateTimeBeforeSecondSave = firstSave.getLastModifiedDateTime();
    assertNotNull(lastModifiedDateTimeBeforeSecondSave);

    TimeUnit.SECONDS.sleep(2);

    var secondSave = productRepository.save(firstSave);
    log.info("secondSave: {}", secondSave);
    assertThat(secondSave.getLastModifiedDateTime(),
        greaterThan(lastModifiedDateTimeBeforeSecondSave));
    assertEquals(createdDateTimeBeforeSecondSave, secondSave.getCreatedDateTime());
  }

}
