package shop.velox.catalog.dao;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import shop.velox.catalog.AbstractIntegrationTest;
import shop.velox.catalog.model.CatalogEntity;

@Slf4j
class CatalogRepositoryTest extends AbstractIntegrationTest {

  @Autowired
  private CatalogRepository catalogRepository;

  @Test
  void avoidCodeAndCatalogCodeDuplicates() {

    Supplier<CatalogEntity> catalogEntitySupplier = () -> CatalogEntity.builder()
        .code("code")
        .build();

    CatalogEntity catalog1 = catalogEntitySupplier.get();
    catalogRepository.save(catalog1);

    CatalogEntity catalog2 = catalogEntitySupplier.get();
    DuplicateKeyException thrown = assertThrows(DuplicateKeyException.class,
        () -> catalogRepository.save(catalog2));

    assertThat(thrown.getMessage(),
        Matchers.containsString(CatalogEntity.UNIQUE_CODE_INDEX_NAME));

  }

  @Test
  @SneakyThrows
  void datesTest() {
    String catalogCode = RandomStringUtils.randomAlphanumeric(10);

    catalogRepository.save(CatalogEntity.builder()
        .code(catalogCode)
        .build());

    var firstSave = catalogRepository.findOneByCode(catalogCode).get();
    log.info("firstSave: {}", firstSave);

    assertNotNull(firstSave);
    assertNotNull(firstSave.getId());
    LocalDateTime createdDateTimeBeforeSecondSave = firstSave.getCreatedDateTime();
    assertNotNull(createdDateTimeBeforeSecondSave);
    LocalDateTime lastModifiedDateTimeBeforeSecondSave = firstSave.getLastModifiedDateTime();
    assertNotNull(lastModifiedDateTimeBeforeSecondSave);

    TimeUnit.SECONDS.sleep(2);

    var secondSave = catalogRepository.save(firstSave);
    log.info("secondSave: {}", secondSave);
    assertThat(secondSave.getLastModifiedDateTime(),
        greaterThan(lastModifiedDateTimeBeforeSecondSave));
    assertEquals(createdDateTimeBeforeSecondSave, secondSave.getCreatedDateTime());
  }

}
