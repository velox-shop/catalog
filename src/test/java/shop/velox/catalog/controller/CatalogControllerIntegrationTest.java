package shop.velox.catalog.controller;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static shop.velox.catalog.controller.utils.CatalogApiTestUtils.HOST_URL;

import jakarta.annotation.Resource;
import java.util.List;
import lombok.SneakyThrows;
import org.apache.commons.lang3.RandomStringUtils;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException.BadRequest;
import org.springframework.web.client.HttpClientErrorException.Conflict;
import org.springframework.web.client.HttpClientErrorException.Forbidden;
import org.springframework.web.client.HttpClientErrorException.NotFound;
import shop.velox.catalog.AbstractWebIntegrationTest;
import shop.velox.catalog.api.controller.client.CatalogApiClient;
import shop.velox.catalog.controller.utils.CatalogApiTestUtils;
import shop.velox.catalog.dao.CatalogRepository;
import shop.velox.catalog.dao.CategoryRepository;
import shop.velox.catalog.dao.ProductRepository;
import shop.velox.catalog.dto.catalog.CatalogDto;
import shop.velox.catalog.dto.catalog.CreateCatalogDto;
import shop.velox.catalog.dto.catalog.UpdateCatalogDto;
import shop.velox.catalog.dto.category.CategoryDto;
import shop.velox.catalog.model.CatalogEntity;
import shop.velox.catalog.model.CategoryEntity;
import shop.velox.catalog.model.ProductEntity;
import shop.velox.catalog.model.ProductType;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CatalogControllerIntegrationTest extends AbstractWebIntegrationTest {

  @Value(value = "${local.server.port}")
  private int port;

  @Resource
  CatalogRepository catalogRepository;

  @Resource
  CategoryRepository categoryRepository;

  @Resource
  ProductRepository productRepository;

  CatalogApiClient anonymousCatalogApiClient;

  CatalogApiClient adminCatalogApiClient;

  @BeforeEach
  void setUpApiClients() {
    anonymousCatalogApiClient = new CatalogApiClient(
        getAnonymousRestTemplate(), String.format(HOST_URL, port));

    adminCatalogApiClient = new CatalogApiClient(
        getAdminRestTemplate(), String.format(HOST_URL, port));
  }

  @Nested
  @TestInstance(TestInstance.Lifecycle.PER_CLASS)
  @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
  class CategoriesAndProductsTreeTest extends AbstractWebIntegrationTest {

    private static final Logger log = LoggerFactory.getLogger(CategoriesAndProductsTreeTest.class);

    private static final String catalogCode = "catalogCode";

    private static final String CATEGORY_1_CODE = "1";
    private static final String CATEGORY_1_1_CODE = "1_1";
    private static final String CATEGORY_2_CODE = "2";
    private static final String CATEGORY_2_1_CODE = "2_1";
    private static final String CATEGORY_2_2_CODE = "2_2";
    private static final String CATEGORY_2_1_1_CODE = "2_1_1";

    private static final String PRODUCT_1_CODE = "product1";
    private static final String PRODUCT_1_1_CODE = "product1_1";

    @BeforeAll
    void setUp() {
      catalogRepository.deleteAll();
      categoryRepository.deleteAll();
      productRepository.deleteAll();

      log.info("Creating catalog with code: {}", catalogCode);
      catalogRepository.save(CatalogEntity.builder()
          .code(catalogCode)
          .rootCategoryCodes(List.of(CATEGORY_1_CODE, CATEGORY_2_CODE))
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_1_CODE)
          .catalogCode(catalogCode)
          // .subCategoryCodes(List.of(CATEGORY_1_1_CODE))
          .productCodes(List.of(PRODUCT_1_CODE))
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_1_1_CODE)
          .catalogCode(catalogCode)
          .parentCode(CATEGORY_1_CODE)
          .productCodes(List.of(PRODUCT_1_1_CODE))
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_2_CODE)
          .catalogCode(catalogCode)
          // .subCategoryCodes(List.of(CATEGORY_2_1_CODE, CATEGORY_2_2_CODE))
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_2_1_CODE)
          .catalogCode(catalogCode)
          .parentCode(CATEGORY_2_CODE)
          // .subCategoryCodes(List.of(CATEGORY_2_1_1_CODE))
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_2_1_1_CODE)
          .catalogCode(catalogCode)
          .parentCode(CATEGORY_2_1_CODE)
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_2_2_CODE)
          .catalogCode(catalogCode)
          .parentCode(CATEGORY_2_CODE)
          .build());

      productRepository.save(ProductEntity.builder()
          .code(PRODUCT_1_CODE)
          .catalogCode(catalogCode)
          .type(ProductType.SIMPLE_PRODUCT)
          .build());

      productRepository.save(ProductEntity.builder()
          .code(PRODUCT_1_1_CODE)
          .catalogCode(catalogCode)
          .type(ProductType.SIMPLE_PRODUCT)
          .build());
    }

    @BeforeEach
    void check() {
      log.info("check");
      assertEquals(1, catalogRepository.count());
    }

    @Test
    @Order(1)
    void getCatalogs() {
      // Given

      // When
      ResponseEntity<Page<CatalogDto>> result = CatalogApiTestUtils.getCatalogs(
          anonymousCatalogApiClient);

      // Then
      assertEquals(OK, result.getStatusCode());
      assertNotNull(result.getBody());
      List<CatalogDto> searchResult = result.getBody().getContent();

      assertEquals(1, searchResult.size());
      CatalogDto catalogDto = searchResult.get(0);
      assertEquals(catalogCode, catalogDto.getCode());
    }

    @Test
    @Order(2)
    void getCatalogByCode() {
      // Given

      // When
      CatalogDto catalogDto = getCatalog(null, null);

      // Then
      assertEquals(catalogCode, catalogDto.getCode());
      assertFalse(isEmpty(catalogDto.getRootCategoryCodes()));
      assertTrue(isEmpty(catalogDto.getRootCategories()));
    }

    @Test
    @Order(2)
    @SneakyThrows
    void getCatalogByCodeWith1LevelOfSubcategoriesAndProducts() {
      // Given

      // When
      CatalogDto catalogDto = getCatalog(1, true);

      // Then
      List<CategoryDto> rootCategories = catalogDto.getRootCategories();
      assertFalse(isEmpty(rootCategories));
      assertThat(rootCategories, Matchers.hasSize(2));

      var category1 = rootCategories.stream()
          .filter(categoryDto -> categoryDto.getCode().equals(CATEGORY_1_CODE))
          .findFirst()
          .orElseThrow(() -> fail("Category not found"));

      assertFalse(isEmpty(category1.getProducts()));
      assertThat(category1.getProducts(), Matchers.hasSize(1));
      assertThat(category1.getProducts().get(0).getCode(),
          equalTo(PRODUCT_1_CODE));

      assertThat(category1.getSubCategories(), is(empty()));

    }

    @Test
    @Order(2)
    @SneakyThrows
    void getCatalogByCodeWith2LevelOfSubcategoriesAndProducts() {
      // Given

      // When
      CatalogDto catalogDto = getCatalog(2, true);

      // Then
      List<CategoryDto> rootCategories = catalogDto.getRootCategories();
      assertFalse(isEmpty(rootCategories));
      assertThat(rootCategories, Matchers.hasSize(2));

      var category1 = rootCategories.stream()
          .filter(categoryDto -> categoryDto.getCode().equals(CATEGORY_1_CODE))
          .findFirst()
          .orElseThrow(() -> fail("Category not found"));

      assertFalse(isEmpty(category1.getProducts()));
      assertThat(category1.getProducts(), Matchers.hasSize(1));
      assertThat(category1.getProducts().get(0).getCode(),
          equalTo(PRODUCT_1_CODE));

      var category1_1 = category1.getSubCategories().stream()
          .filter(categoryDto -> categoryDto.getCode().equals(CATEGORY_1_1_CODE))
          .findFirst()
          .orElseThrow(() -> fail("SubCategory not found"));

      assertFalse(isEmpty(category1_1.getProducts()));
      assertThat(category1_1.getProducts(), Matchers.hasSize(1));
      assertThat(category1_1.getProducts().get(0).getCode(),
          equalTo(PRODUCT_1_1_CODE));

    }

  }

  @Test
  @Order(2)
  void createCatalogAsAnonymousTest() {
    var payload = CreateCatalogDto.builder()
        .code(RandomStringUtils.randomAlphabetic(10))
        .build();

    assertThrows(Forbidden.class, () -> CatalogApiTestUtils.createCatalog(payload,
        anonymousCatalogApiClient));
  }

  @Test
  @Order(2)
  void createInvalidCatalogTest() {
    var payload = CreateCatalogDto.builder()
        .code(null)
        .build();

    assertThrows(BadRequest.class, () -> CatalogApiTestUtils.createCatalog(payload,
        adminCatalogApiClient));
  }

  @Test
  @Order(3)
  void createCatalogTest() {
    var payload = CreateCatalogDto.builder()
        .code(RandomStringUtils.randomAlphabetic(10))
        .build();

    ResponseEntity<CatalogDto> result = CatalogApiTestUtils.createCatalog(payload,
        adminCatalogApiClient);

    assertEquals(CREATED, result.getStatusCode());
    assertNotNull(result.getBody());
  }

  @Test
  @Order(3)
  void createCatalogAgainTest() {
    var payload = CreateCatalogDto.builder()
        .code(RandomStringUtils.randomAlphabetic(10))
        .build();

    catalogRepository.save(CatalogEntity.builder()
        .code(payload.getCode())
        .build());

    assertThrows(Conflict.class, () -> CatalogApiTestUtils.createCatalog(payload,
        adminCatalogApiClient));
  }

  @Test
  @Order(4)
  void updateCatalogOkTest() {
    // GIVEN
    String code = RandomStringUtils.randomAlphabetic(10);

    var creationPayload = CreateCatalogDto.builder()
        .code(code)
        .build();
    var creationResponseEntity = CatalogApiTestUtils.createCatalog(creationPayload,
        adminCatalogApiClient);
    assertEquals(CREATED, creationResponseEntity.getStatusCode());

    // WHEN
    var updatePayload = UpdateCatalogDto.builder()
        .rootCategoryCodes(List.of("rootCategoryCode1"))
        .build();
    var updateResponseEntity = CatalogApiTestUtils.saveCatalog(code, updatePayload,
        adminCatalogApiClient);

    // THEN
    assertEquals(OK, updateResponseEntity.getStatusCode());
    assertThat(updateResponseEntity.getBody(), is(notNullValue()));
    assertThat(updateResponseEntity.getBody().getRootCategoryCodes(),
        equalTo(updatePayload.getRootCategoryCodes()));
  }

  @Test
  @Order(4)
  void updateCatalogNotFoundTest() {
    // GIVEN
    String code = RandomStringUtils.randomAlphabetic(10);

    // WHEN
    var updatePayload = UpdateCatalogDto.builder()
        .rootCategoryCodes(List.of("rootCategoryCode1"))
        .build();

    assertThrows(NotFound.class, () -> CatalogApiTestUtils.saveCatalog(code, updatePayload,
        adminCatalogApiClient));
  }

  private CatalogDto getCatalog(Integer expandCategoryLevels,
      Boolean expandProducts) {
    return CatalogApiTestUtils.getCatalog(CategoriesAndProductsTreeTest.catalogCode,
        expandCategoryLevels, expandProducts,
        anonymousCatalogApiClient);
  }


}
