package shop.velox.catalog.controller;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static shop.velox.catalog.controller.utils.CatalogApiTestUtils.HOST_URL;

import jakarta.annotation.Resource;
import java.util.List;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.lang.Nullable;
import org.springframework.web.client.HttpClientErrorException.BadRequest;
import org.springframework.web.client.HttpClientErrorException.Conflict;
import org.springframework.web.client.HttpClientErrorException.NotFound;
import org.springframework.web.client.RestClientException;
import shop.velox.catalog.AbstractWebIntegrationTest;
import shop.velox.catalog.api.controller.client.CategoryApiClient;
import shop.velox.catalog.controller.utils.CategoryApiTestUtils;
import shop.velox.catalog.dao.CatalogRepository;
import shop.velox.catalog.dao.CategoryRepository;
import shop.velox.catalog.dao.ProductRepository;
import shop.velox.catalog.dto.category.CategoryDto;
import shop.velox.catalog.dto.category.CreateCategoryDto;
import shop.velox.catalog.dto.category.UpdateCategoryDto;
import shop.velox.catalog.dto.product.ProductDto;
import shop.velox.catalog.model.Attribute;
import shop.velox.catalog.model.AttributeAssignment;
import shop.velox.catalog.model.AttributeTextValue;
import shop.velox.catalog.model.CatalogEntity;
import shop.velox.catalog.model.CategoryEntity;
import shop.velox.catalog.model.CategoryReference;
import shop.velox.catalog.model.CategoryStatus;
import shop.velox.catalog.model.ProductEntity;
import shop.velox.catalog.model.ProductStatus;
import shop.velox.catalog.model.ProductType;

class CategoryControllerIntegrationTest extends AbstractWebIntegrationTest {

  private static final String CATALOG_CODE = "merchandise";

  @Value(value = "${local.server.port}")
  private int port;

  @Resource
  private CategoryRepository categoryRepository;

  CategoryApiClient anonymousCategoryApiClient;

  CategoryApiClient adminCategoryApiClient;

  @BeforeEach
  void setUpApiClients() {
    anonymousCategoryApiClient = new CategoryApiClient(
        getAnonymousRestTemplate(), String.format(HOST_URL, port));

    adminCategoryApiClient = new CategoryApiClient(
        getAdminRestTemplate(), String.format(HOST_URL, port));
  }

  @Nested
  @TestInstance(TestInstance.Lifecycle.PER_CLASS)
  @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
  class CategoryPathTest {

    private static final String CATALOG_CODE = "catalogCode";

    private static final String CATEGORY_1_CODE = "1";
    private static final String CATEGORY_1_1_CODE = "1_1";

    @Resource
    private CatalogRepository catalogRepository;

    @Resource
    private CategoryRepository categoryRepository;

    @BeforeAll
    void setUp() {
      catalogRepository.save(CatalogEntity.builder()
          .code(CATALOG_CODE)
          .rootCategoryCodes(List.of(CATEGORY_1_CODE))
          .build());

      var root = categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_1_CODE)
          .catalogCode(CATALOG_CODE)
          .subCategoryCodes(List.of(CATEGORY_1_1_CODE))
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_1_1_CODE)
          .catalogCode(CATALOG_CODE)
          .subCategoryCodes(List.of(CATEGORY_1_1_CODE))
          .categoryPath(List.of(
              CategoryReference.builder()
                  .code(root.getCode())
                  .name(root.getName())
                  .build()))
          .build());
    }

    @Test
    void rootTest() {
      CategoryDto categoryDto = getCategoryDto(CATALOG_CODE, CATEGORY_1_CODE, 0, false, false);
      assertThat(categoryDto.getCategoryPath(), is(empty()));
    }

    @Test
    void leafTest() {
      CategoryDto categoryDto = getCategoryDto(CATALOG_CODE, CATEGORY_1_1_CODE, 0, false, false);
      assertThat(categoryDto.getCategoryPath(), hasSize(1));
    }

  }

  @Nested
  @TestInstance(TestInstance.Lifecycle.PER_CLASS)
  @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
  class CatalogStructureTest {

    private static final String catalogCode = "catalogCode";

    private static final String CATEGORY_1_CODE = "1";
    private static final String CATEGORY_1_1_CODE = "1_1";
    private static final String CATEGORY_2_CODE = "2";
    private static final String CATEGORY_2_1_CODE = "2_1";
    private static final String CATEGORY_2_2_CODE = "2_2";
    private static final String CATEGORY_2_1_1_CODE = "2_1_1";

    private static final String PRODUCT_1_CODE = "product1";
    private static final String PRODUCT_1_1_CODE = "product1_1";
    private static final String PRODUCT_2_CODE = "product2";
    private static final String PRODUCT_2_1_CODE = "product2_1";
    private static final String VARIANT_IN_PRODUCT_2_1_CODE = "variant2_1";
    private static final String PRODUCT_2_2_CODE = "product2_2";
    private static final String PRODUCT_2_1_1_CODE = "product2_1_1";

    @Resource
    private CatalogRepository catalogRepository;

    @Resource
    private CategoryRepository categoryRepository;

    @Resource
    private ProductRepository productRepository;

    @BeforeAll
    void setUp() {
      catalogRepository.save(CatalogEntity.builder()
          .code(catalogCode)
          .rootCategoryCodes(List.of(CATEGORY_1_CODE, CATEGORY_2_CODE))
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_1_CODE)
          .catalogCode(catalogCode)
          .subCategoryCodes(List.of(CATEGORY_1_1_CODE))
          .productCodes(List.of(PRODUCT_1_CODE))
          .build());

      productRepository.save(ProductEntity.builder()
          .code(PRODUCT_1_CODE)
          .catalogCode(catalogCode)
          .type(ProductType.SIMPLE_PRODUCT)
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_1_1_CODE)
          .catalogCode(catalogCode)
          .parentCode(CATEGORY_1_CODE)
          .productCodes(List.of(PRODUCT_1_1_CODE))
          .build());

      productRepository.save(ProductEntity.builder()
          .code(PRODUCT_1_1_CODE)
          .catalogCode(catalogCode)
          .type(ProductType.SIMPLE_PRODUCT)
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_2_CODE)
          .catalogCode(catalogCode)
          .subCategoryCodes(List.of(CATEGORY_2_1_CODE, CATEGORY_2_2_CODE))
          .productCodes(List.of(PRODUCT_2_CODE))
          .build());

      productRepository.save(ProductEntity.builder()
          .code(PRODUCT_2_CODE)
          .catalogCode(catalogCode)
          .type(ProductType.SIMPLE_PRODUCT)
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_2_1_CODE)
          .catalogCode(catalogCode)
          .parentCode(CATEGORY_2_CODE)
          .subCategoryCodes(List.of(CATEGORY_2_1_1_CODE))
          .productCodes(List.of(PRODUCT_2_1_CODE))
          .build());

      productRepository.save(ProductEntity.builder()
          .code(PRODUCT_2_1_CODE)
          .catalogCode(catalogCode)
          .type(ProductType.MULTI_VARIANT_PRODUCT)
          .build());

      productRepository.save(ProductEntity.builder()
          .code(VARIANT_IN_PRODUCT_2_1_CODE)
          .parentCode(PRODUCT_2_1_CODE)
          .catalogCode(catalogCode)
          .type(ProductType.PRODUCT_VARIANT)
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_2_1_1_CODE)
          .catalogCode(catalogCode)
          .parentCode(CATEGORY_2_1_CODE)
          .productCodes(List.of(PRODUCT_2_1_1_CODE))
          .build());

      productRepository.save(ProductEntity.builder()
          .code(PRODUCT_2_1_1_CODE)
          .catalogCode(catalogCode)
          .type(ProductType.SIMPLE_PRODUCT)
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_2_2_CODE)
          .catalogCode(catalogCode)
          .parentCode(CATEGORY_2_CODE)
          .status(CategoryStatus.INACTIVE)
          .build());

      productRepository.save(ProductEntity.builder()
          .code(PRODUCT_2_2_CODE)
          .catalogCode(catalogCode)
          .type(ProductType.SIMPLE_PRODUCT)
          .build());

    }

    @Test
    @Order(1)
    void getCategoryWithoutSubCategoriesAndWithoutProducts() {
      CategoryDto categoryDto = getCategoryDto(catalogCode, CATEGORY_2_1_CODE, 0, false, false);
      assertThat(categoryDto.getCode(), is(CATEGORY_2_1_CODE));
      assertThat(categoryDto.getParentCode(), is(CATEGORY_2_CODE));

      assertThat(categoryDto.getSubCategoryCodes(), contains(CATEGORY_2_1_1_CODE));
      assertThat(categoryDto.getSubCategories(), is(empty()));

      assertThat(categoryDto.getProductCodes(), contains(PRODUCT_2_1_CODE));
      assertThat(categoryDto.getProducts(), is(empty()));
    }

    @Test
    @Order(1)
    void getCategoryWithoutSubCategoriesAndWithProducts() {
      CategoryDto categoryDto = getCategoryDto(catalogCode, CATEGORY_2_1_CODE, 0, true, true);
      assertThat(categoryDto.getCode(), is(CATEGORY_2_1_CODE));
      assertThat(categoryDto.getParentCode(), is(CATEGORY_2_CODE));

      assertThat(categoryDto.getSubCategoryCodes(), contains(CATEGORY_2_1_1_CODE));
      assertThat(categoryDto.getSubCategories(), is(empty()));

      assertThat(categoryDto.getProductCodes(), contains(PRODUCT_2_1_CODE));
      assertThat(categoryDto.getProducts().stream().map(ProductDto::getCode).toList(),
          is(categoryDto.getProductCodes()));

      assertThat(categoryDto.getProducts().getFirst().getVariants(), hasSize(1));
    }

    @Test
    @Order(1)
    void getCategoryWithSubCategoriesAndWithProducts() {
      CategoryDto categoryDto = getCategoryDto(catalogCode, CATEGORY_2_1_CODE, 1, true, false);
      assertThat(categoryDto.getCode(), is(CATEGORY_2_1_CODE));
      assertThat(categoryDto.getParentCode(), is(CATEGORY_2_CODE));

      assertThat(categoryDto.getSubCategoryCodes(), contains(CATEGORY_2_1_1_CODE));
      assertThat(categoryDto.getSubCategories().stream().map(CategoryDto::getCode).toList(),
          is(categoryDto.getSubCategoryCodes()));

      assertThat(categoryDto.getProductCodes(), contains(PRODUCT_2_1_CODE));
      List<ProductDto> products = categoryDto.getProducts();
      assertThat(products.stream().map(ProductDto::getCode).toList(),
          is(categoryDto.getProductCodes()));

      assertThat(products.getFirst().getVariants(), is(empty()));
    }

    @Test
    @Order(1)
    void getAllCategories() {
      Page<CategoryDto> page = getCategories();
      assertNotNull(page);
      var content = page.getContent();
      assertThat(content, hasSize(5));
      List<String> categoryCodes = content.stream()
          .map(CategoryDto::getCode)
          .toList();
      assertThat(categoryCodes,
          contains(CATEGORY_1_CODE, CATEGORY_1_1_CODE, CATEGORY_2_CODE, CATEGORY_2_1_CODE,
              CATEGORY_2_1_1_CODE));
      assertThat(categoryCodes, not(contains(CATEGORY_2_2_CODE)));

    }

    @Test
    @Order(3)
    void getCategoriesOfProductTest() {
      Page<CategoryDto> page = getCategories(PRODUCT_1_1_CODE);
      assertEquals(1, page.getTotalElements());
      assertEquals(CATEGORY_1_1_CODE, page.getContent().get(0).getCode());
    }


    private Page<CategoryDto> getCategories() {
      return getCategories(null);
    }

    private Page<CategoryDto> getCategories(@Nullable String productCode) {
      return getCategories(0, false, false, productCode);
    }

    private Page<CategoryDto> getCategories(Integer expandCategoryLevels, Boolean expandProducts,
        Boolean expandVariants, @Nullable String productCode) {
      return CategoryApiTestUtils.getCategories(catalogCode, expandCategoryLevels, expandProducts,
          expandVariants, productCode, anonymousCategoryApiClient);
    }
  }

  @Nested
  @TestInstance(TestInstance.Lifecycle.PER_CLASS)
  @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
  class CategoryWithInactiveProductsTest {

    private static final String CATALOG_CODE = "catalogCode";

    private static final String CATEGORY_1_CODE = "category1Code";

    private static final String INVALID_BASE_PRODUCT_CODE = "invalidBaseProductCode";

    private static final String VALID_BASE_PRODUCT_CODE = "validBaseProductCode";

    private static final String INVALID_VARIANT_PRODUCT_CODE = "invalidVariantProductCode";

    private static final String VALID_VARIANT_PRODUCT_CODE = "validVariantProductCode";

    @Resource
    private CatalogRepository catalogRepository;

    @Resource
    private CategoryRepository categoryRepository;

    @Resource
    private ProductRepository productRepository;

    @BeforeAll
    void setUp() {
      catalogRepository.save(CatalogEntity.builder()
          .code(CATALOG_CODE)
          .rootCategoryCodes(List.of(CATEGORY_1_CODE))
          .build());

      categoryRepository.save(CategoryEntity.builder()
          .code(CATEGORY_1_CODE)
          .catalogCode(CATALOG_CODE)
          .productCodes(List.of(VALID_BASE_PRODUCT_CODE, INVALID_BASE_PRODUCT_CODE))
          .build());

      productRepository.save(ProductEntity.builder()
          .code(INVALID_BASE_PRODUCT_CODE)
          .catalogCode(CATALOG_CODE)
          .type(ProductType.MULTI_VARIANT_PRODUCT)
          .status(ProductStatus.INACTIVE)
          .build());

      productRepository.save(ProductEntity.builder()
          .code(VALID_BASE_PRODUCT_CODE)
          .catalogCode(CATALOG_CODE)
          .type(ProductType.MULTI_VARIANT_PRODUCT)
          .build());

      productRepository.save(ProductEntity.builder()
          .code(INVALID_VARIANT_PRODUCT_CODE)
          .parentCode(VALID_BASE_PRODUCT_CODE)
          .catalogCode(CATALOG_CODE)
          .type(ProductType.PRODUCT_VARIANT)
          .status(ProductStatus.INACTIVE)
          .build());

      productRepository.save(ProductEntity.builder()
          .code(VALID_VARIANT_PRODUCT_CODE)
          .parentCode(VALID_BASE_PRODUCT_CODE)
          .catalogCode(CATALOG_CODE)
          .type(ProductType.PRODUCT_VARIANT)
          .build());
    }

    @Test
    void getCategory() {
      CategoryDto categoryDto = getCategoryDto(CATALOG_CODE, CATEGORY_1_CODE, 0, true, true);
      assertThat(categoryDto.getProductCodes(),
          contains(VALID_BASE_PRODUCT_CODE, INVALID_BASE_PRODUCT_CODE));
      assertThat(categoryDto.getProducts(), hasSize(1));
      ProductDto validBaseProduct = categoryDto.getProducts().getFirst();
      assertThat(validBaseProduct.getCode(), is(VALID_BASE_PRODUCT_CODE));
      assertThat(validBaseProduct.getVariants(), hasSize(1));
      ProductDto validVariantProduct = validBaseProduct.getVariants().getFirst();
      assertThat(validVariantProduct.getCode(), is(VALID_VARIANT_PRODUCT_CODE));
    }
  }

  private CategoryDto getCategoryDto(String catalogCode, String categoryCode,
      Integer expandCategoryLevels, Boolean expandProducts, Boolean expandVariants) {
    return CategoryApiTestUtils.getCategory(catalogCode, categoryCode, expandCategoryLevels,
        expandProducts, expandVariants, anonymousCategoryApiClient);
  }

  @Test
  @Order(4)
  void createCategoryAsAnonymousTest() {
    var payload = CreateCategoryDto.builder()
        .code(RandomStringUtils.randomAlphabetic(10))
        .build();

    var exception = assertThrows(RestClientException.class,
        () -> anonymousCategoryApiClient.createCategory(CATALOG_CODE,
            payload));

    assertEquals(403,
        ((org.springframework.web.client.HttpClientErrorException) exception).getRawStatusCode());
  }

  @Test
  @Order(4)
  void createInvalidCategoryTest() {
    var payload = CreateCategoryDto.builder()
        .code(null)
        .build();

    assertThrows(BadRequest.class,
        () -> adminCategoryApiClient.createCategory(CATALOG_CODE, payload));
  }

  @Test
  @Order(5)
  void createCategoryTest() {
    var payload = CreateCategoryDto.builder()
        .code(RandomStringUtils.randomAlphabetic(10))
        .attributeValues(List.of(AttributeAssignment.builder()
            .attribute(Attribute.builder()
                .code("attributeCode")
                .name("attributeName")
                .build())
            .values(List.of(AttributeTextValue.builder()
                .code("valueCode")
                .name("valueName")
                .build()))
            .build()))
        .build();

    var result = assertDoesNotThrow(
        () -> adminCategoryApiClient.createCategory(CATALOG_CODE, payload));

    assertEquals(CREATED, result.getStatusCode());
    assertThat(result.getBody(), is(notNullValue()));
    assertThat(result.getBody().getCode(), is(payload.getCode()));
    assertThat(result.getBody().getAttributeValues(), hasSize(1));
    assertThat(result.getBody().getAttributeValues().get(0).getValues(), hasSize(1));
  }

  @Test
  @Order(6)
  void createCategoryAgainTest() {
    CategoryEntity alreadyExisting = categoryRepository.save(CategoryEntity.builder()
        .code(RandomStringUtils.randomAlphabetic(10))
        .catalogCode(CATALOG_CODE)
        .build());

    var payload = CreateCategoryDto.builder()
        .code(alreadyExisting.getCode())
        .build();

    assertThrows(Conflict.class,
        () -> adminCategoryApiClient.createCategory(CATALOG_CODE, payload));
  }

  @Test
  @Order(7)
  void updateCategoryNotFoundTest() {
    // GIVEN
    String catalogCode = RandomStringUtils.randomAlphabetic(10);
    String categoryCode = "categoryCode";

    // WHEN
    var updatePayload = UpdateCategoryDto.builder()
        .subCategoryCodes(List.of("rootCategoryCode1"))
        .build();
    assertThrows(NotFound.class,
        () -> adminCategoryApiClient.saveCategory(catalogCode, categoryCode, updatePayload));
  }

  @Test
  @Order(4)
  void updateCategoryOkTest() {
    // GIVEN
    String catalogCode = RandomStringUtils.randomAlphabetic(10);
    String categoryCode = "categoryCode";

    var creationPayload = CreateCategoryDto.builder()
        .code(categoryCode)
        .build();
    var creationResponseEntity = adminCategoryApiClient.createCategory(catalogCode,
        creationPayload);
    assertEquals(CREATED, creationResponseEntity.getStatusCode());

    // WHEN
    var updatePayload = UpdateCategoryDto.builder()
        .subCategoryCodes(List.of("subCategoryCode1"))
        .attributeValues(List.of(AttributeAssignment.builder()
            .attribute(Attribute.builder()
                .code("attributeCode")
                .name("attributeName")
                .build())
            .values(List.of(AttributeTextValue.builder()
                .code("valueCode")
                .name("valueName")
                .build()))
            .build()))
        .build();
    var updateResponseEntity = adminCategoryApiClient.saveCategory(catalogCode, categoryCode,
        updatePayload);

    // THEN
    assertEquals(OK, updateResponseEntity.getStatusCode());
    assertThat(updateResponseEntity.getBody(), is(notNullValue()));
    assertThat(updateResponseEntity.getBody().getSubCategoryCodes(),
        equalTo(updatePayload.getSubCategoryCodes()));
    assertThat(updateResponseEntity.getBody().getAttributeValues(), hasSize(1));
    assertThat(updateResponseEntity.getBody().getAttributeValues().get(0).getValues(), hasSize(1));
  }

}
