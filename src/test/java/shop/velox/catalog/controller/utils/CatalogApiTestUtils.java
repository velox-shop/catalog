package shop.velox.catalog.controller.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.http.HttpStatus.OK;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import shop.velox.catalog.api.controller.client.CatalogApiClient;
import shop.velox.catalog.dto.catalog.CatalogDto;
import shop.velox.catalog.dto.catalog.CreateCatalogDto;
import shop.velox.catalog.dto.catalog.UpdateCatalogDto;

@UtilityClass
@Slf4j
public class CatalogApiTestUtils {

  public static final String HOST_URL = "http://localhost:%s";

  public static ResponseEntity<CatalogDto> createCatalog(CreateCatalogDto payload,
      CatalogApiClient catalogApiClient) {

    return catalogApiClient.createCatalog(payload);
  }

  public static ResponseEntity<CatalogDto> saveCatalog(String catalogCode, UpdateCatalogDto payload,
      CatalogApiClient catalogApiClient) {

    return catalogApiClient.saveCatalog(catalogCode, payload);
  }

  public static ResponseEntity<Page<CatalogDto>> getCatalogs(CatalogApiClient catalogApiClient) {
    return catalogApiClient.getCatalogs();
  }

  public static CatalogDto getCatalog(String catalogCode, Integer expandCategoryLevels,
      Boolean expandProducts, CatalogApiClient catalogApiClient) {
    return getCatalog(catalogCode, expandCategoryLevels, expandProducts, null, catalogApiClient);
  }

  public static CatalogDto getCatalog(String catalogCode, Integer expandCategoryLevels,
      Boolean expandProducts, Boolean expandVariants, CatalogApiClient catalogApiClient) {

    ResponseEntity<CatalogDto> result = catalogApiClient.getCatalog(catalogCode,
        expandCategoryLevels, expandProducts, expandVariants);

    assertEquals(OK, result.getStatusCode());
    CatalogDto catalogDto = result.getBody();
    assertNotNull(catalogDto);

    return catalogDto;
  }

}
