package shop.velox.catalog.controller.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.http.HttpStatus.OK;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import shop.velox.catalog.api.controller.client.CategoryApiClient;
import shop.velox.catalog.dto.category.CategoryDto;

@UtilityClass
@Slf4j
public class CategoryApiTestUtils {

  public static CategoryDto getCategory(String catalogCode, String categoryCode,
      Integer expandCategoryLevels, Boolean expandProducts, Boolean expandVariants,
      CategoryApiClient categoryApiClient) {

    ResponseEntity<CategoryDto> result = categoryApiClient.getCategory(catalogCode, categoryCode,
        expandCategoryLevels, expandProducts, expandVariants);

    assertEquals(OK, result.getStatusCode());
    assertNotNull(result.getBody());

    return result.getBody();
  }

  public static Page<CategoryDto> getCategories(String catalogCode, Integer expandCategoryLevels,
      Boolean expandProducts, Boolean expandVariants, @Nullable String productCode,
      CategoryApiClient categoryApiClient) {

    ResponseEntity<? extends Page<CategoryDto>> result = categoryApiClient.getCategories(
        catalogCode, expandCategoryLevels, expandProducts, expandVariants, productCode);

    assertEquals(OK, result.getStatusCode());
    assertNotNull(result.getBody());

    return result.getBody();
  }

}
