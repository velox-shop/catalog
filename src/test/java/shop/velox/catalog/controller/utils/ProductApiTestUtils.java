package shop.velox.catalog.controller.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.MatcherAssert;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import shop.velox.catalog.api.controller.client.ProductApiClient;
import shop.velox.catalog.dto.product.CreateProductDto;
import shop.velox.catalog.dto.product.ProductDto;
import shop.velox.catalog.dto.product.UpdateProductDto;
import shop.velox.catalog.model.ProductType;

@UtilityClass
@Slf4j
public class ProductApiTestUtils {

  public static ProductDto createProduct(String catalogCode, String productCode,
      ProductApiClient productApiClient) {

    var payload = CreateProductDto.builder()
        .code(productCode)
        .name("aName")
        .type(ProductType.SIMPLE_PRODUCT)
        .build();

    var creationResponseEntity = productApiClient.createProduct(payload, catalogCode);
    assertEquals(CREATED, creationResponseEntity.getStatusCode());
    ProductDto creationBody = creationResponseEntity.getBody();
    assertThat(creationBody).isNotNull();
    MatcherAssert.assertThat(creationBody.getCode(), equalTo(productCode));
    return creationBody;
  }

  public static ResponseEntity<ProductDto> createProduct(CreateProductDto payload,
      String catalogCode, ProductApiClient productApiClient) {

    return productApiClient.createProduct(payload, catalogCode);
  }

  public static ResponseEntity<ProductDto> saveProduct(UpdateProductDto payload,
      String catalogCode, String productCode, ProductApiClient productApiClient) {

    return productApiClient.saveProduct(payload, catalogCode, productCode);
  }

  public static ProductDto getProduct(String catalogCode, String productCode,
      ProductApiClient productApiClient) {

    var result = productApiClient.getProduct(catalogCode, productCode);

    assertEquals(OK, result.getStatusCode());
    assertNotNull(result.getBody());

    return result.getBody();
  }

  public static Page<ProductDto> getProducts(String catalogCode, @Nullable ProductType productType,
      Pageable pageable, ProductApiClient productApiClient) {
    var responseEntity = productApiClient.getProducts(catalogCode, productType, pageable);
    assertThat(responseEntity.getStatusCode()).isEqualTo(OK);
    Page<ProductDto> body = responseEntity.getBody();
    assertThat(body).isNotNull();
    return body;
  }

  public static Page<ProductDto> getProducts(String catalogCode, @Nullable String searchTerm,
      ProductApiClient productApiClient) {
    return searchProducts(catalogCode, searchTerm, productApiClient);
  }

  public static Page<ProductDto> searchProducts(String catalogCode, @Nullable String searchTerm,
      ProductApiClient productApiClient) {

    ResponseEntity<? extends Page<ProductDto>> result = productApiClient.searchProducts(
        catalogCode, searchTerm, true);

    assertEquals(OK, result.getStatusCode());
    var body = result.getBody();
    assertNotNull(body);

    return body;
  }

}
