package shop.velox.catalog.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static shop.velox.catalog.controller.utils.CatalogApiTestUtils.HOST_URL;
import static shop.velox.catalog.controller.utils.ProductApiTestUtils.getProduct;
import static shop.velox.catalog.controller.utils.ProductApiTestUtils.getProducts;
import static shop.velox.catalog.controller.utils.ProductApiTestUtils.searchProducts;

import jakarta.annotation.Resource;
import java.util.List;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.client.HttpClientErrorException.BadRequest;
import org.springframework.web.client.HttpClientErrorException.Conflict;
import org.springframework.web.client.HttpClientErrorException.Forbidden;
import org.springframework.web.client.HttpClientErrorException.NotFound;
import shop.velox.catalog.AbstractWebIntegrationTest;
import shop.velox.catalog.api.controller.client.ProductApiClient;
import shop.velox.catalog.api.controller.client.ProductApiClient.GetProductsByCodesRequestParams;
import shop.velox.catalog.controller.utils.ProductApiTestUtils;
import shop.velox.catalog.dao.ProductRepository;
import shop.velox.catalog.dto.product.CreateProductDto;
import shop.velox.catalog.dto.product.ProductDto;
import shop.velox.catalog.dto.product.UpdateProductDto;
import shop.velox.catalog.model.CategoryReference;
import shop.velox.catalog.model.ProductEntity;
import shop.velox.catalog.model.ProductType;

class CatalogProductControllerIntegrationTest extends AbstractWebIntegrationTest {

  @DynamicPropertySource
  static void dynamicProperties(DynamicPropertyRegistry registry) {
    // registry.add("velox.websecurity.debug", () -> true);
    registry.add("logging.level.spring.security", () -> "DEBUG");
  }

  @Value(value = "${local.server.port}")
  private int port;

  @Resource
  private ProductRepository productRepository;

  ProductApiClient anonymousProductApiClient;

  ProductApiClient adminProductApiClient;

  @BeforeEach
  void setUpApiClients() {
    anonymousProductApiClient = new ProductApiClient(
        getAnonymousRestTemplate(), String.format(HOST_URL, port));

    adminProductApiClient = new ProductApiClient(
        getAdminRestTemplate(), String.format(HOST_URL, port));
  }

  @Nested
  @TestInstance(TestInstance.Lifecycle.PER_CLASS)
  @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
  class ProductsTest {

    private static final String catalogCode = "aCatalogCode";

    @Resource
    private ProductRepository productRepository;

    @BeforeAll
    void setUp() {
      productRepository.save(ProductEntity.builder()
          .code("shirt")
          .name("Shirt")
          .catalogCode(catalogCode)
          .type(ProductType.MULTI_VARIANT_PRODUCT)
          .build());

      productRepository.save(ProductEntity.builder()
          .code("shirt-black-s")
          .name("Black Shirt S")
          .catalogCode(catalogCode)
          .type(ProductType.PRODUCT_VARIANT)
          .build());

      productRepository.save(ProductEntity.builder()
          .code("shirt-black-m")
          .name("Black Shirt M")
          .catalogCode(catalogCode)
          .type(ProductType.PRODUCT_VARIANT)
          .build());

      productRepository.save(ProductEntity.builder()
          .code("other-product")
          .name("Other Product")
          .catalogCode(catalogCode)
          .type(ProductType.SIMPLE_PRODUCT)
          .build());

    }

    @Test
    @Order(1)
    void getProductsByTypeTest() {

      Page<ProductDto> page0 = getProducts(catalogCode, ProductType.PRODUCT_VARIANT,
          PageRequest.of(0, 1), anonymousProductApiClient);

      Page<ProductDto> page1 = getProducts(catalogCode, ProductType.PRODUCT_VARIANT,
          PageRequest.of(1, 1), anonymousProductApiClient);

      assertThat(page0.getTotalElements()).isEqualTo(2);
      assertThat(page0.getNumber()).isEqualTo(0);
      assertThat(page0.getTotalPages()).isEqualTo(2);
      assertThat(page0.isLast()).isEqualTo(false);

      assertThat(page1.getTotalElements()).isEqualTo(2);
      assertThat(page1.getNumber()).isEqualTo(1);
      assertThat(page1.getTotalPages()).isEqualTo(2);
      assertThat(page1.isLast()).isEqualTo(true);

      assertNotEquals(page0.getContent().getFirst().getCode(),
          page1.getContent().getFirst().getCode());

    }

    @Test
    @Order(1)
    void getProductsByCodesTest() {

      List<ProductDto> productList = anonymousProductApiClient.getProductsByCodes(catalogCode,
          GetProductsByCodesRequestParams.builder()
              .productCodes(List.of("shirt", "shirt-black-s"))
              .build()).getBody();

      assertThat(productList).extracting(ProductDto::getCode)
          .containsExactly("shirt", "shirt-black-s");
    }

    @Test
    @Order(1)
    void getProductsTest() {

      Page<ProductDto> page = getProducts(catalogCode, null, anonymousProductApiClient);

      assertFalse(page.isEmpty());
      assertEquals(4, page.getTotalElements());
    }

    @Test
    @Order(1)
    void getProductTest() {

      ProductDto productDto = getProduct(catalogCode, "shirt", anonymousProductApiClient);

      assertEquals("shirt", productDto.getCode());
    }

    @Test
    @Order(1)
    void searchProductsTest() {

      Page<ProductDto> page = searchProducts(catalogCode, "shirt", anonymousProductApiClient);

      assertFalse(page.isEmpty());
      List<String> actualProductCodes = page.getContent()
          .stream()
          .map(ProductDto::getCode)
          .toList();

      List<String> expectedProductCodes = List.of("shirt", "shirt-black-s", "shirt-black-m");
      assertThat(actualProductCodes, containsInAnyOrder(expectedProductCodes.toArray()));
      assertThat(actualProductCodes, not(hasItem("other-product")));
    }
  }

  @Test
  @Order(2)
  void createProductAsAnonymousTest() {

    String catalogCode = RandomStringUtils.randomAlphabetic(10);

    var payload = CreateProductDto.builder()
        .code("aProductCode")
        .name("name")
        .type(ProductType.SIMPLE_PRODUCT)
        .build();

    assertThrows(Forbidden.class, () -> ProductApiTestUtils.createProduct(payload, catalogCode,
        anonymousProductApiClient));
  }

  @Test
  @Order(3)
  void createInvalidProductAsAdminTest() {
    String catalogCode = RandomStringUtils.randomAlphabetic(10);

    var payload = CreateProductDto.builder().build();

    assertThrows(BadRequest.class, () -> ProductApiTestUtils.createProduct(payload, catalogCode,
        adminProductApiClient));
  }

  @Test
  @Order(3)
  void createProductTest() {
    String catalogCode = RandomStringUtils.randomAlphabetic(10);

    var payload = CreateProductDto.builder()
        .code("aProductCode")
        .name("name")
        .externalId("anExternalId")
        .type(ProductType.SIMPLE_PRODUCT)
        .build();

    ResponseEntity<ProductDto> result = ProductApiTestUtils.createProduct(payload, catalogCode,
        adminProductApiClient);

    assertEquals(CREATED, result.getStatusCode());

    ProductDto resultBody = result.getBody();
    assertThat(resultBody, is(notNullValue()));
    assertThat(resultBody.getCode(), equalTo(payload.getCode()));
    assertThat(resultBody.getExternalId(), equalTo(payload.getExternalId()));

  }

  @Test
  @Order(4)
  void createProductAgainTest() {
    String catalogCode = RandomStringUtils.randomAlphabetic(10);
    String productCode = "aProductCode";

    var alreadyExisting = productRepository.save(ProductEntity.builder()
        .code(productCode)
        .catalogCode(catalogCode)
        .type(ProductType.SIMPLE_PRODUCT)
        .build());

    // Create your payload
    var payload = CreateProductDto.builder()
        .code(productCode)
        .name("name")
        .type(ProductType.SIMPLE_PRODUCT)
        .build();

    assertThrows(Conflict.class, () -> ProductApiTestUtils.createProduct(payload, catalogCode,
        adminProductApiClient));
  }

  @Test
  @Order(5)
  void updateProductNotFoundTest() {
    // GIVEN
    String catalogCode = RandomStringUtils.randomAlphabetic(10);
    String productCode = "productCode";

    // WHEN
    var updatePayload = UpdateProductDto.builder()
        .name("aName")
        .type(ProductType.SIMPLE_PRODUCT)
        .build();

    assertThrows(NotFound.class, () -> ProductApiTestUtils.saveProduct(updatePayload, catalogCode,
        productCode, adminProductApiClient));
  }

  @Test
  @Order(6)
  void updateProductOkTest() {
    // GIVEN
    String catalogCode = RandomStringUtils.randomAlphabetic(10);
    String productCode = "productCode";

    var creationPayload = CreateProductDto.builder()
        .code(productCode)
        .name("name1")
        .type(ProductType.SIMPLE_PRODUCT)
        .build();
    var creationResponseEntity = ProductApiTestUtils.createProduct(creationPayload, catalogCode,
        adminProductApiClient);
    assertEquals(CREATED, creationResponseEntity.getStatusCode());

    // WHEN
    List<CategoryReference> categoryPath = List.of(
        CategoryReference.builder()
            .code("crCode1")
            .name("crName1")
            .build());
    var updatePayload = UpdateProductDto.builder()
        .name("name2")
        .type(ProductType.SIMPLE_PRODUCT)
        .categoryPath(categoryPath)
        .build();
    var updateResponseEntity = ProductApiTestUtils.saveProduct(updatePayload, catalogCode,
        productCode, adminProductApiClient);

    // THEN
    assertEquals(OK, updateResponseEntity.getStatusCode());
    ProductDto updatedProductDto = updateResponseEntity.getBody();
    assertThat(updatedProductDto, is(notNullValue()));
    assertThat(updatedProductDto.getName(), equalTo(updatePayload.getName()));
    assertEquals(categoryPath, updatedProductDto.getCategoryPath());
  }

  @Test
  @Order(6)
  void updateMultiVariantProductOkTest() {
    // GIVEN
    String catalogCode = RandomStringUtils.randomAlphabetic(10);
    String parentCode = "parentCode";
    String childCode = "childCode";

    var childCreationResponseEntity = ProductApiTestUtils.createProduct(CreateProductDto.builder()
            .code(childCode)
            .name("childName")
            .parentCode(parentCode)
            .type(ProductType.PRODUCT_VARIANT)
            .build(), catalogCode,
        adminProductApiClient);
    assertEquals(CREATED, childCreationResponseEntity.getStatusCode());
    ProductDto childProductDto = childCreationResponseEntity.getBody();
    assertThat(childProductDto, is(notNullValue()));
    assertThat(childProductDto.getCode(), equalTo(childCode));
    assertThat(childProductDto.getParentCode(), equalTo(parentCode));
    assertThat(childProductDto.getVariants(), anyOf(nullValue(), empty()));

    var parentCreationResponseEntity = ProductApiTestUtils.createProduct(CreateProductDto.builder()
            .code(parentCode)
            .name("parentName")
            .type(ProductType.MULTI_VARIANT_PRODUCT)
            .build(),
        catalogCode, adminProductApiClient);
    assertEquals(CREATED, parentCreationResponseEntity.getStatusCode());
    ProductDto parentProductDto = parentCreationResponseEntity.getBody();
    assertThat(parentProductDto, is(notNullValue()));
    assertThat(parentProductDto.getCode(), equalTo(parentCode));
    assertThat(parentProductDto.getParentCode(), is(nullValue()));
    assertThat(parentProductDto.getVariants(), not(empty()));

    var parentFormGet = ProductApiTestUtils.getProduct(catalogCode, parentCode,
        adminProductApiClient);
    assertThat(parentFormGet.getVariants(), not(empty()));

    var updateResponseEntity = ProductApiTestUtils.saveProduct(UpdateProductDto.builder()
            .name("parentName2")
            .type(ProductType.MULTI_VARIANT_PRODUCT)
            .build(), catalogCode,
        parentCode, adminProductApiClient);
    assertEquals(OK, updateResponseEntity.getStatusCode());
    ProductDto updatedProductDto = updateResponseEntity.getBody();
    assertThat(updatedProductDto, is(notNullValue()));
    assertThat(updatedProductDto.getVariants(), not(empty()));
  }

  @Test
  @Order(6)
  void updateProductInvalidTest() {
    // GIVEN
    String catalogCode = RandomStringUtils.randomAlphabetic(10);
    String productCode = "productCode";

    var creationPayload = CreateProductDto.builder()
        .code(productCode)
        .name("aName")
        .type(ProductType.SIMPLE_PRODUCT)
        .build();
    var creationResponseEntity = ProductApiTestUtils.createProduct(creationPayload, catalogCode,
        adminProductApiClient);
    assertEquals(CREATED, creationResponseEntity.getStatusCode());

    // WHEN
    var updatePayload = UpdateProductDto.builder()
        .build();
    assertThrows(BadRequest.class, () -> ProductApiTestUtils.saveProduct(updatePayload, catalogCode,
        productCode, adminProductApiClient));
  }

  @Nested
  @TestPropertySource(properties = {
      "logging.level.shop.velox.catalog.api.controller.client.ProductApiClient=DEBUG",
      "logging.level.shop.velox.catalog.api.controller.impl=DEBUG",
      "logging.level.org.springframework.security.web=DEBUG"
  })
  class EncodingTest {

    @Test
    void GetProductTest() {
      // GIVEN
      String catalogCode = RandomStringUtils.randomAlphabetic(10);

      String productCodeWithoutSlash = "productCode";
      var productWithoutSlash = ProductApiTestUtils.createProduct(catalogCode,
          productCodeWithoutSlash,
          adminProductApiClient);

      String productCodeWithSlash = productCodeWithoutSlash + "/2";
      var productWithSlash = ProductApiTestUtils.createProduct(catalogCode, productCodeWithSlash,
          adminProductApiClient);

      // WHEN
      var product = ProductApiTestUtils.getProduct(catalogCode, productCodeWithSlash,
          adminProductApiClient);

      // THEN
      assertThat(product.getCode()).isEqualTo(productCodeWithSlash);
    }

    @Test
    void SaveProductTest() {
      // GIVEN
      String catalogCode = RandomStringUtils.randomAlphabetic(10);
      String productCode = "productCode/2";

      var creationPayload = CreateProductDto.builder()
          .code(productCode)
          .name("aName")
          .type(ProductType.SIMPLE_PRODUCT)
          .build();
      var creationResponseEntity = ProductApiTestUtils.createProduct(creationPayload, catalogCode,
          adminProductApiClient);
      assertEquals(CREATED, creationResponseEntity.getStatusCode());
      ProductDto creationBody = creationResponseEntity.getBody();
      assertThat(creationBody).isNotNull();
      assertThat(creationBody.getCode(), equalTo(productCode));

      // WHEN
      var updatePayload = UpdateProductDto.builder()
          .name("name2")
          .type(ProductType.SIMPLE_PRODUCT)
          .categoryPath(List.of(
              CategoryReference.builder()
                  .code("crCode1")
                  .name("crName1")
                  .build()))
          .build();
      var updateResponseEntity = ProductApiTestUtils.saveProduct(updatePayload, catalogCode,
          productCode, adminProductApiClient);

      // THEN
      assertEquals(OK, updateResponseEntity.getStatusCode());
      ProductDto updatedProductDto = updateResponseEntity.getBody();
      assertThat(updatedProductDto, is(notNullValue()));
      assertThat(updatedProductDto.getCode()).isEqualTo(productCode);
    }
  }
}
