package shop.velox.catalog.converter.catalog;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.opentest4j.AssertionFailedError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import shop.velox.catalog.AbstractIntegrationTest;
import shop.velox.catalog.converter.category.CategoryConverter;
import shop.velox.catalog.converter.category.CategoryConverterContext;
import shop.velox.catalog.dao.CategoryRepository;
import shop.velox.catalog.dao.ProductRepository;
import shop.velox.catalog.model.CategoryEntity;
import shop.velox.catalog.model.ProductEntity;
import shop.velox.catalog.model.ProductType;

@TestInstance(Lifecycle.PER_CLASS)
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
public class CategoryProductsDecoratorTest extends AbstractIntegrationTest {

  private static final String CATALOG_CODE = "catalogCode";

  private static final String PARENT_CODE = "parentCode";

  private static final String CHILD_CODE = "childCode";

  private static final String CATEGORY_CODE = "categoryCode";

  @Autowired
  CategoryRepository categoryRepository;

  @Autowired
  ProductRepository productRepository;

  @Autowired
  CategoryConverter categoryConverter;

  @BeforeAll
  void setup() {
    productRepository.save(ProductEntity.builder()
        .code(PARENT_CODE)
        .catalogCode(CATALOG_CODE)
        .type(ProductType.MULTI_VARIANT_PRODUCT)
        .build());

    productRepository.save(ProductEntity.builder()
        .code(CHILD_CODE)
        .catalogCode(CATALOG_CODE)
        .parentCode(PARENT_CODE)
        .type(ProductType.PRODUCT_VARIANT)
        .build());

    categoryRepository.save(CategoryEntity.builder()
        .code(CATEGORY_CODE)
        .catalogCode(CATALOG_CODE)
        .productCodes(List.of(PARENT_CODE, "notExisting"))
        .build());
  }

  @Test
  void doNotExpandProductsTest() {

    var categoryEntity = categoryRepository.findByCodeAndCatalogCode(CATEGORY_CODE, CATALOG_CODE)
        .orElseThrow(AssertionFailedError::new);

    var categoryDto = categoryConverter.convertEntityToDto(categoryEntity,
        CategoryConverterContext.builder()
            .expandProducts(false)
            .build());

    assertThat(categoryDto.getProducts()).isEmpty();
  }

  @Test
  void expandOnlyProductsTest() {

    var categoryEntity = categoryRepository.findByCodeAndCatalogCode(CATEGORY_CODE, CATALOG_CODE)
        .orElseThrow(AssertionFailedError::new);

    var categoryDto = categoryConverter.convertEntityToDto(categoryEntity,
        CategoryConverterContext.builder()
            .expandProducts(true)
            .build());

    assertThat(categoryDto.getProducts()).hasSize(1);
    assertThat(categoryDto.getProducts().getFirst().getVariants()).isEmpty();
  }

  @Test
  void expandProductsAndVariantsTest() {

    var categoryEntity = categoryRepository.findByCodeAndCatalogCode(CATEGORY_CODE, CATALOG_CODE)
        .orElseThrow(AssertionFailedError::new);

    var categoryDto = categoryConverter.convertEntityToDto(categoryEntity,
        CategoryConverterContext.builder()
            .expandProducts(true)
            .expandVariants(true)
            .build());

    assertThat(categoryDto.getProducts()).hasSize(1);
    assertThat(categoryDto.getProducts().getFirst().getVariants()).hasSize(1);
  }

}
