package shop.velox.catalog.converter.catalog;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import shop.velox.catalog.AbstractIntegrationTest;
import shop.velox.catalog.dao.CategoryRepository;
import shop.velox.catalog.dto.category.CategoryDto;
import shop.velox.catalog.model.CategoryEntity;

@TestInstance(Lifecycle.PER_CLASS)
class CatalogCategoriesDecoratorTest extends AbstractIntegrationTest {

  @Autowired
  private CatalogCategoriesDecorator catalogCategoriesDecorator;

  @Autowired
  private CategoryRepository categoryRepository;

  String catalogCode = "merchandise";

  String categoryCode_1 = "111";
  String categoryCode_1_1 = "111-111";
  String categoryCode_1_1_1 = "111-111-111";
  String categoryCode_1_2 = "111-222";
  String categoryCode_2 = "222";

  @BeforeAll
  void beforeAll() {
    createCategoriesTree();
  }

  private void createCategoriesTree() {

    categoryRepository.save(CategoryEntity.builder()
        .code(categoryCode_1_1)
        .catalogCode("anotherCatalog")
        .parentCode(categoryCode_1)
        .subCategoryCodes(List.of(categoryCode_1_1_1))
        .build());

    categoryRepository.save(CategoryEntity.builder()
        .code(categoryCode_1)
        .catalogCode(catalogCode)
        .subCategoryCodes(List.of(categoryCode_1_1, categoryCode_1_2))
        .build());

    categoryRepository.save(CategoryEntity.builder()
        .code(categoryCode_1_1)
        .catalogCode(catalogCode)
        .parentCode(categoryCode_1)
        .subCategoryCodes(List.of(categoryCode_1_1_1))
        .build());

    categoryRepository.save(CategoryEntity.builder()
        .code(categoryCode_1_1_1)
        .catalogCode(catalogCode)
        .parentCode(categoryCode_1_1)
        .build());

    categoryRepository.save(CategoryEntity.builder()
        .code(categoryCode_1_2)
        .catalogCode(catalogCode)
        .parentCode(categoryCode_1)
        .build());

    categoryRepository.save(CategoryEntity.builder()
        .code(categoryCode_2)
        .catalogCode(catalogCode)
        .build());
  }

  @Test
  void ZeroSubCategoriesFromRoot() {
    // Given
    List<String> rootCategoriesIds = List.of(categoryCode_1, categoryCode_2);

    // When
    List<CategoryDto> rootCategories = catalogCategoriesDecorator.getSubcategories(catalogCode,
        rootCategoriesIds, 0, false, false);

    // Then
    assertThat(rootCategories, is(empty()));

  }

  @Test
  void OneSubcategoryFromRoot() {
    // Given
    List<String> rootCategoriesIds = List.of(categoryCode_1, categoryCode_2);

    // When
    List<CategoryDto> rootCategories = catalogCategoriesDecorator.getSubcategories(catalogCode,
        rootCategoriesIds, 1, false, false);

    // Then
    assertThat(rootCategories, hasSize(2));

    assertEquals(categoryCode_1, rootCategories.get(0).getCode());
    assertEquals(categoryCode_2, rootCategories.get(1).getCode());
    assertThat(rootCategories.get(0).getSubCategories(), is(empty()));
    assertThat(rootCategories.get(1).getSubCategories(), is(empty()));

  }

  @Test
  void TwoSubcategoriesFromRoot() {
    // Given
    List<String> rootCategoriesIds = List.of(categoryCode_1, categoryCode_2);

    // When
    List<CategoryDto> rootCategories = catalogCategoriesDecorator.getSubcategories(catalogCode,
        rootCategoriesIds, 2, false, false);

    // Then
    assertThat(rootCategories, hasSize(2));

    assertEquals(categoryCode_1, rootCategories.get(0).getCode());
    assertEquals(categoryCode_2, rootCategories.get(1).getCode());
    assertThat(rootCategories.get(0).getSubCategories(), hasSize(2));
    assertEquals(categoryCode_1_1, rootCategories.get(0).getSubCategories().get(0).getCode());
    assertEquals(categoryCode_1_2, rootCategories.get(0).getSubCategories().get(1).getCode());
    assertThat(rootCategories.get(0).getSubCategories().get(0).getSubCategories(),
        is(empty()));

  }

  @Test
  void ThreeSubcategoriesFromRoot() {
    // Given
    List<String> rootCategoriesIds = List.of(categoryCode_1, categoryCode_2);

    // When
    List<CategoryDto> rootCategories = catalogCategoriesDecorator.getSubcategories(catalogCode,
        rootCategoriesIds, 3, false, false);

    // Then
    assertEquals(categoryCode_1, rootCategories.get(0).getCode());
    assertEquals(categoryCode_2, rootCategories.get(1).getCode());
    assertThat(rootCategories.get(0).getSubCategories(), hasSize(2));
    assertEquals(categoryCode_1_1, rootCategories.get(0).getSubCategories().get(0).getCode());
    assertEquals(categoryCode_1_2, rootCategories.get(0).getSubCategories().get(1).getCode());
    assertThat(rootCategories.get(0).getSubCategories().get(0).getSubCategories(),
        is(not(empty())));
    assertEquals(categoryCode_1_1_1,
        rootCategories.get(0).getSubCategories().get(0).getSubCategories().get(0).getCode());

  }

}
