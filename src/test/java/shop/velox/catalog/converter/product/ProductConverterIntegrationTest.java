package shop.velox.catalog.converter.product;

import static org.assertj.core.api.Assertions.assertThat;
import static shop.velox.catalog.model.ProductStatus.ACTIVE;
import static shop.velox.catalog.model.ProductStatus.INACTIVE;

import jakarta.annotation.Resource;
import java.util.List;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.test.context.TestPropertySource;
import shop.velox.catalog.AbstractIntegrationTest;
import shop.velox.catalog.dao.ProductRepository;
import shop.velox.catalog.dto.product.ProductDto;
import shop.velox.catalog.model.ProductEntity;
import shop.velox.catalog.model.ProductStatus;
import shop.velox.catalog.model.ProductType;

@Slf4j
public class ProductConverterIntegrationTest {

  @Nested
  @TestInstance(TestInstance.Lifecycle.PER_CLASS)
  @TestPropertySource(properties = {
      "logging.level.org.mongodb.driver.protocol.command=DEBUG",
  })
  class ExpandVariants extends AbstractIntegrationTest {

    public static final String CATALOG_CODE = "catalogCode";
    public static final String PARENT_CODE = "parentCode";
    public static final String ACTIVE_CHILD_CODE = "activeChildCode";
    public static final String INACTIVE_CHILD_CODE = "inactiveChildCode";

    @Resource
    ProductRepository productRepository;

    @Resource
    ProductConverter productConverter;

    ProductEntity parent;
    ProductEntity activeChild;
    ProductEntity inactiveChild;

    @BeforeAll
    void setup() {
      parent = productRepository.save(ProductEntity.builder()
          .code(PARENT_CODE)
          .catalogCode(CATALOG_CODE)
          .type(ProductType.MULTI_VARIANT_PRODUCT)
          .build());

      activeChild = productRepository.save(ProductEntity.builder()
          .code(ACTIVE_CHILD_CODE)
          .catalogCode(CATALOG_CODE)
          .parentCode(parent.getCode())
          .type(ProductType.PRODUCT_VARIANT)
          .status(ACTIVE)
          .build());

      inactiveChild = productRepository.save(ProductEntity.builder()
          .code(INACTIVE_CHILD_CODE)
          .catalogCode(CATALOG_CODE)
          .parentCode(parent.getCode())
          .type(ProductType.PRODUCT_VARIANT)
          .status(ProductStatus.INACTIVE)
          .build());
    }

    @ParameterizedTest
    @MethodSource("convertEntityToDtoTestArguments")
    void convertEntityToDtoTest(boolean expandVariants, List<ProductStatus> allowedStatuses,
        List<ProductEntity> expectedVariants) {
      List<String> expectedVariantCodes = expectedVariants.stream()
          .map(ProductEntity::getCode)
          .toList();

      ProductEntity parentEntity = productRepository.findOneByCodeAndCatalogCode(PARENT_CODE,
          CATALOG_CODE).orElseThrow();

      ProductDto parentDto = productConverter.convertEntityToDto(parentEntity, expandVariants,
          allowedStatuses);

      List<String> actualVariantCodes = parentDto.getVariants()
          .stream()
          .map(ProductDto::getCode)
          .toList();
      assertThat(actualVariantCodes).isEqualTo(expectedVariantCodes);
    }

    public Stream<Arguments> convertEntityToDtoTestArguments() {
      return Stream.of(
          Arguments.of(true, List.of(ACTIVE), List.of(activeChild)),
          Arguments.of(true, List.of(ACTIVE, INACTIVE), List.of(activeChild, inactiveChild)),
          Arguments.of(true, null, List.of(activeChild, inactiveChild)),
          Arguments.of(false, List.of(ACTIVE), List.of())
      );
    }
  }

}
