package shop.velox.catalog;

import jakarta.annotation.Resource;
import lombok.AccessLevel;
import lombok.Getter;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractWebIntegrationTest extends AbstractIntegrationTest {

  private final Logger log = LoggerFactory.getLogger(AbstractWebIntegrationTest.class);

  @Value(value = "${local.server.port}")
  protected int port;

  @Resource
  protected RestTemplateBuilder restTemplateBuilder;

  @Getter(AccessLevel.PROTECTED)
  private RestTemplate anonymousRestTemplate;

  @Getter(AccessLevel.PROTECTED)
  private RestTemplate adminRestTemplate;

  @BeforeEach
  public void setup() throws InterruptedException {
    log.info("Web test method setup");

    anonymousRestTemplate = restTemplateBuilder.build();

    adminRestTemplate = restTemplateBuilder.basicAuthentication("admin", "velox").build();
  }

}
