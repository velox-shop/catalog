package shop.velox.catalog.api.controller.client;

import static org.assertj.core.api.Assertions.assertThat;
import static shop.velox.catalog.model.ProductStatus.ACTIVE;

import java.net.URI;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import shop.velox.catalog.api.controller.client.ProductApiClient.GetProductsRequestParams;

class ApiClientUtilsTest {

  @Test
  void getProductsUri() {

    Pageable pageable = PageRequest.of(0,20, Sort.by(Order.asc("id")));
    GetProductsRequestParams getProductsRequestParams = GetProductsRequestParams.builder()
        .expandVariants(true)
        .allowedStatuses(List.of(ACTIVE))
        .pageable(pageable)
        .searchTerm("éöàä")
        .build();

    URI uri = ApiClientUtils.getProductsUri("http://localhost", "velox-de", getProductsRequestParams);

    assertThat(uri.toString()).isEqualTo("http://localhost?searchTerm=éöàä&expandVariants=true&allowedStatuses=ACTIVE&page=0&size=20&sort=id,ASC");
  }

  @Test
  void toQueryParam() {
    Sort sort = Sort.by(Order.asc("id"));
    String queryParam = ApiClientUtils.toQueryParam(sort);
    assertThat(queryParam).isEqualTo("id,ASC");
  }
}
