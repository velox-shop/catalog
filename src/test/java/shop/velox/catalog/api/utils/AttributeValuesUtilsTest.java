package shop.velox.catalog.api.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import java.util.List;
import java.util.stream.StreamSupport;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import shop.velox.catalog.model.Attribute;
import shop.velox.catalog.model.AttributeAssignment;
import shop.velox.catalog.model.AttributeTextValue;
import shop.velox.catalog.model.TextAttributeAssignment;

class AttributeValuesUtilsTest {

  @Test
  void testGetSingleValueProperty_WhenAttributeCodeExists() {
    // Arrange
    String attributeCode = "color";
    String expectedValue = "red";

    AttributeTextValue existingValue = AttributeTextValue.builder().name(expectedValue).build();
    TextAttributeAssignment existingAssignment = TextAttributeAssignment.builder()
        .attribute(Attribute.builder().code(attributeCode).build())
        .values(List.of(existingValue))
        .build();

    List<AttributeAssignment<?>> attributeValues = List.of(existingAssignment);

    // Act
    String actualValue = AttributeValuesUtils.getSingleValueProperty(attributeValues, attributeCode);

    // Assert
    assertEquals(expectedValue, actualValue);
  }

  @Test
  void testGetSingleValueProperty_WhenAttributeCodeDoesNotExist() {
    // Arrange
    String attributeCode = "color";
    List<AttributeAssignment<?>> attributeValues = List.of();

    // Act
    String actualValue = AttributeValuesUtils.getSingleValueProperty(attributeValues, attributeCode);

    // Assert
    assertNull(actualValue);
  }

  @Test
  void getMultiVaueProperty_WhenAttributeCodeExists() {
    // Arrange
    String attributeCode = "color";
    List<String> expectedValues = List.of("red", "blue");

    AttributeTextValue value1 = AttributeTextValue.builder().name("red").build();
    AttributeTextValue value2 = AttributeTextValue.builder().name("blue").build();
    TextAttributeAssignment existingAssignment = TextAttributeAssignment.builder()
        .attribute(Attribute.builder().code(attributeCode).build())
        .values(List.of(value1, value2))
        .build();

    List<AttributeAssignment<?>> attributeValues = List.of(existingAssignment);

    // Act
    List<String> actualValues = AttributeValuesUtils.getMultiVaueProperty(attributeCode, attributeValues);

    // Assert
    assertEquals(expectedValues, actualValues);
  }

  @Test
  void getMultiVaueProperty_WhenAttributeCodeDoesNotExist() {
    // Arrange
    String attributeCode = "color";
    List<AttributeAssignment<?>> attributeValues = List.of();

    // Act
    List<String> actualValues = AttributeValuesUtils.getMultiVaueProperty(attributeCode, attributeValues);

    // Assert
    assertThat(actualValues).isEmpty();
  }

  @Test
  @DisplayName("setSingleValueProperty: When attributeAssignment and AttributeValue exist, updates value")
  void testSetSingleValueProperty_WhenAttributeCodeExists_UpdatesValue() {
    // Arrange
    String attributeCode = "color";
    String newValue = "blue";

    TextAttributeAssignment existingAssignment = TextAttributeAssignment.builder()
        .attribute(Attribute.builder().code(attributeCode).build())
        .values(List.of(AttributeTextValue.builder()
            .code("red")
            .name("Red")
            .build()))
        .build();

    List<AttributeAssignment<?>> attributeValues = List.of(existingAssignment);
    testThatThereIsNoValidationErrors(attributeValues);

    // Act
    var updatedAttributeAssignments = AttributeValuesUtils.setSingleValueProperty(attributeValues,
        attributeCode, newValue);

    // Assert
    assertEquals(1, updatedAttributeAssignments.size());
    AttributeTextValue newTextValue = (AttributeTextValue) updatedAttributeAssignments.get(0)
        .getValues().get(0);
    assertThat(newTextValue.getName()).isEqualTo(newValue);

    testThatThereIsNoValidationErrors(updatedAttributeAssignments);
  }

  @Test
  @DisplayName("setSingleValueProperty: When attributeAssignment exists but AttributeValue does not, create it")
  void testSetSingleValueProperty_WhenAttributeCodeExistsButValueDoesNotExist_AddsNewValue() {
    // Arrange
    String attributeCode = "color";
    String value = "blue";

    TextAttributeAssignment existingAssignment = TextAttributeAssignment.builder()
        .attribute(Attribute.builder().code(attributeCode).build())
        .values(List.of())
        .build();

    List<AttributeAssignment<?>> attributeValues = List.of(existingAssignment);

    // Act
    var updatedAttributeAssignments = AttributeValuesUtils.setSingleValueProperty(attributeValues,
        attributeCode, value);

    // Assert
    assertEquals(1, updatedAttributeAssignments.size());
    AttributeTextValue newValue = (AttributeTextValue) updatedAttributeAssignments.get(0)
        .getValues().get(0);
    assertEquals(value, newValue.getName());

    testThatThereIsNoValidationErrors(updatedAttributeAssignments);
  }

  @Test
  @DisplayName("setSingleValueProperty: When attributeAssignment does not exist, create it")
  void testSetSingleValueProperty_WhenAttributeCodeDoesNotExist_AddsNewAttribute() {
    // Arrange
    String attributeCode = "size";
    String value = "large";

    List<AttributeAssignment<?>> attributeValues = List.of();

    // Act
    var updatedAttributeAssignments = AttributeValuesUtils.setSingleValueProperty(attributeValues, attributeCode, value);

    // Assert
    assertEquals(1, updatedAttributeAssignments.size());
    AttributeAssignment<?> newAssignment = updatedAttributeAssignments.get(0);
    assertEquals(attributeCode, newAssignment.getAttribute().getCode());
    assertEquals(value, ((AttributeTextValue) newAssignment.getValues().get(0)).getName());

    testThatThereIsNoValidationErrors(updatedAttributeAssignments);

  }

  private static void testThatThereIsNoValidationErrors(Object dto) {
    try (ValidatorFactory factory = Validation.buildDefaultValidatorFactory()) {
      Validator validator = factory.getValidator();

      if (dto instanceof Iterable<?> iterable) {
        List<String> violations = StreamSupport.stream(iterable.spliterator(), false)
            .flatMap(element -> validator.validate(element).stream()
                .map(
                    violation -> "Element: " + element + " -> " + violation.getPropertyPath() + ": "
                        + violation.getMessage()))
            .toList();

        // Assert all violations are collected
        assertThat(violations)
            .as("Validation failed for one or more elements in the collection")
            .isEmpty();
      } else {
        var violations = validator.validate(dto);

        // Assert that the object has no violations
        assertThat(violations)
            .as("Validation failed for object: " + dto)
            .isEmpty();
      }
    }
  }


  @Test
  void testSetSingleValueProperty_WhenAttributeCodeExistsButValuesAreEmpty_AddsNewValue() {
    // Arrange
    String attributeCode = "material";
    String value = "cotton";

    TextAttributeAssignment existingAssignment = TextAttributeAssignment.builder()
        .attribute(Attribute.builder().code(attributeCode).build())
        .values(List.of())
        .build();

    List<AttributeAssignment<?>> attributeValues = List.of(existingAssignment);

    // Act
    var updatedAttributeAssignments = AttributeValuesUtils.setSingleValueProperty(attributeValues, attributeCode, value);

    // Assert
    assertThat(updatedAttributeAssignments).hasSize(1);
    AttributeTextValue newValue = (AttributeTextValue) updatedAttributeAssignments.get(0).getValues().get(0);
    assertEquals(value, newValue.getName());
  }

  @Test
  void testDeleteSingleValueProperty_WhenAttributeCodeExists() {
    // Arrange
    String attributeCode = "color";
    AttributeTextValue existingValue = AttributeTextValue.builder().name("red").build();
    TextAttributeAssignment existingAssignment = TextAttributeAssignment.builder()
        .attribute(Attribute.builder().code(attributeCode).build())
        .values(List.of(existingValue))
        .build();

    List<AttributeAssignment<?>> attributeValues = List.of(existingAssignment);

    // Act
    AttributeValuesUtils.deleteSingleValueProperty(attributeValues, attributeCode);

    // Assert
    assertThat(attributeValues.get(0).getValues()).isEmpty();
  }

  @Test
  void testDeleteSingleValueProperty_WhenAttributeCodeDoesNotExist() {
    // Arrange
    String attributeCode = "color";
    List<AttributeAssignment<?>> attributeValues = List.of();

    // Act
    assertDoesNotThrow(
        () -> AttributeValuesUtils.deleteSingleValueProperty(attributeValues, attributeCode));
  }
}
