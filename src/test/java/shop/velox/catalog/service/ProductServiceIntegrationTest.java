package shop.velox.catalog.service;


import static org.assertj.core.api.Assertions.assertThat;
import static shop.velox.commons.security.VeloxSecurityConstants.Authorities.GLOBAL_ADMIN_AUTHORIZATION;

import jakarta.annotation.Resource;
import java.util.List;
import lombok.SneakyThrows;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.opentest4j.AssertionFailedError;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import shop.velox.catalog.AbstractIntegrationTest;
import shop.velox.catalog.dao.ProductRepository;
import shop.velox.catalog.dto.product.CreateProductDto;
import shop.velox.catalog.dto.product.ProductDto;
import shop.velox.catalog.model.ProductEntity;
import shop.velox.catalog.model.ProductStatus;
import shop.velox.catalog.model.ProductType;

public class ProductServiceIntegrationTest {

  @Nested
  @TestInstance(TestInstance.Lifecycle.PER_CLASS)
  @TestPropertySource(properties = {
      "logging.level.org.springframework.data.mongodb.core=TRACE",
      "logging.level.org.mongodb.driver=TRACE"
  })
  class GetProductsTest extends AbstractIntegrationTest {

    private static final String CATALOG_CODE = "theCatalogCode";

    private static final String PARENT_CODE = "shirt";

    private static final String VARIANT_CODE = "shirt-black-s";

    @Resource
    ProductRepository productRepository;

    @Resource
    ProductService productService;


    @BeforeAll
    void setUp() {
      productRepository.save(ProductEntity.builder()
          .code(PARENT_CODE)
          .name("Shirt")
          .catalogCode(CATALOG_CODE)
          .type(ProductType.MULTI_VARIANT_PRODUCT)
          .build());

      productRepository.save(ProductEntity.builder()
          .code(VARIANT_CODE)
          .name("Black Shirt S")
          .catalogCode(CATALOG_CODE)
          .type(ProductType.PRODUCT_VARIANT)
          .parentCode(PARENT_CODE)
          .build());

      productRepository.save(ProductEntity.builder()
          .code("other-product")
          .name("Other Product")
          .catalogCode(CATALOG_CODE)
          .type(ProductType.SIMPLE_PRODUCT)
          .build());

      productRepository.save(ProductEntity.builder()
          .code("other-variant")
          .name("Other Variant")
          .catalogCode(CATALOG_CODE)
          .type(ProductType.PRODUCT_VARIANT)
          .parentCode("Other Parent")
          .build());

      productRepository.save(ProductEntity.builder()
          .code("inactive-variant")
          .name("Inactive Variant")
          .catalogCode(CATALOG_CODE)
          .type(ProductType.PRODUCT_VARIANT)
          .parentCode("Other Parent")
          .status(ProductStatus.INACTIVE)
          .build());

      productRepository.save(ProductEntity.builder()
          .code(VARIANT_CODE)
          .name("Black Shirt S")
          .catalogCode("otherCatalog")
          .type(ProductType.PRODUCT_VARIANT)
          .parentCode(PARENT_CODE)
          .build());
    }

    @Test
    void getVariantsTest() {
      ProductDto baseProduct = productService.getProductByCatalogCodeAndCode(CATALOG_CODE,
              PARENT_CODE, List.of())
          .orElseThrow(() -> new AssertionFailedError("Product not found"));

      assertThat(baseProduct.getVariants()).hasSize(1);
    }

    @Test
    void getProductsByTypeTest() {
      Page<ProductDto> productPage = productService.getProducts(CATALOG_CODE,
          ProductType.PRODUCT_VARIANT, null, false, List.of(ProductStatus.ACTIVE),
          Pageable.ofSize(1));

      assertThat(productPage.getTotalElements()).isEqualTo(2L);
      assertThat(productPage.getNumber()).isEqualTo(0);
      assertThat(productPage.getSize()).isEqualTo(1);
      assertThat(productPage.getNumberOfElements()).isEqualTo(1);
    }

    @Test
    void getProductsByNullTypeTest() {
      Page<ProductDto> productPage = productService.getProducts(CATALOG_CODE, null, null, false,
          List.of(ProductStatus.ACTIVE), Pageable.ofSize(1));

      assertThat(productPage.getTotalElements()).isEqualTo(4L);
      assertThat(productPage.getNumber()).isEqualTo(0);
      assertThat(productPage.getSize()).isEqualTo(1);
      assertThat(productPage.getNumberOfElements()).isEqualTo(1);
    }
  }

  @Nested
  @TestInstance(TestInstance.Lifecycle.PER_CLASS)
  class CreateProductTest extends AbstractIntegrationTest {

    private static final String CATALOG_CODE = "theCatalogCode";

    @Resource
    ProductService productService;

    @Test
    @WithMockUser(authorities = {GLOBAL_ADMIN_AUTHORIZATION})
    @SneakyThrows
    void createMultiVariantProductTest() {
      String parentCode = "parentCode";
      SoftAssertions softly = new SoftAssertions();

      productService.createProduct(CreateProductDto.builder()
          .code("childCode")
          .parentCode(parentCode)
          .type(ProductType.PRODUCT_VARIANT)
          .build(), CATALOG_CODE);

      var savedParent = productService.createProduct(CreateProductDto.builder()
          .code(parentCode)
          .type(ProductType.MULTI_VARIANT_PRODUCT)
          .build(), CATALOG_CODE);
      softly.assertThat(savedParent.getVariants()).hasSize(1);

      ProductDto gotParent = productService.getProductByCatalogCodeAndCode(CATALOG_CODE,
              parentCode, List.of())
          .orElseThrow(() -> new AssertionFailedError("Product not found"));
      softly.assertThat(gotParent.getVariants()).hasSize(1);
      
      softly.assertAll();
    }
  }

}
