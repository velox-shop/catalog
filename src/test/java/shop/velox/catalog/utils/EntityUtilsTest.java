package shop.velox.catalog.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import shop.velox.catalog.model.CategoryEntity;

class EntityUtilsTest {

  @Test
  void reorderAndFilterEntitiesAsTheCodes() {

    CategoryEntity entity1 = CategoryEntity.builder()
        .code("1")
        .build();

    CategoryEntity entity2 = CategoryEntity.builder()
        .code("2")
        .build();

    CategoryEntity entity3 = CategoryEntity.builder()
        .code("3")
        .build();

    CategoryEntity entity4 = CategoryEntity.builder()
        .code("4")
        .build();

    List<String> codes = List.of("3", "2", "1", "4");
    List<CategoryEntity> entities = List.of(entity1, entity2, entity3, entity4);
    var reorderedEntities = EntityUtils.reorderAndFilterEntitiesAsTheCodes(codes, entities, CategoryEntity::getCode);

    assertThat(reorderedEntities).extracting(CategoryEntity::getCode).isEqualTo(codes);
  }

  @Test
  void reorderAndFilterEntitiesAsTheCodes_emptyList() {

    List<String> codes = List.of();
    List<CategoryEntity> entities = List.of();
    var reorderedEntities = EntityUtils.reorderAndFilterEntitiesAsTheCodes(codes, entities, CategoryEntity::getCode);

    assertThat(reorderedEntities).isEmpty();
  }

  @Test
  void reorderAndFilterEntitiesAsTheCodes_nullList() {

    List<String> codes = null;
    List<CategoryEntity> entities = null;
    assertThrows(IllegalArgumentException.class,
        ()-> EntityUtils.reorderAndFilterEntitiesAsTheCodes(codes, entities, CategoryEntity::getCode));

  }

  @Test
  void reorderAndFilterEntitiesAsTheCodes_nullCode() {

    CategoryEntity entity1 = CategoryEntity.builder()
        .code("1")
        .build();

    CategoryEntity entityWithoutCode = CategoryEntity.builder()
        .code(null)
        .build();

    List<String> codes = Stream.of("1", null).toList();

    List<CategoryEntity> entities = List.of(entityWithoutCode);
    var reorderedEntities = EntityUtils.reorderAndFilterEntitiesAsTheCodes(codes, entities, CategoryEntity::getCode);

    assertThat(reorderedEntities).contains(entityWithoutCode);
  }

  @Test
  void reorderEntitiesAsTheCodes_codeNotInAndFilterEntities() {

    CategoryEntity entity1 = CategoryEntity.builder()
        .code("1")
        .build();


    List<String> codes = List.of("2", "1");
    List<CategoryEntity> entities = List.of(entity1);
    var reorderedEntities = EntityUtils.reorderAndFilterEntitiesAsTheCodes(codes, entities, CategoryEntity::getCode);

    assertThat(reorderedEntities).extracting(CategoryEntity::getCode).isEqualTo(List.of( "1"));
  }

  @Test
  void reorderAndFilterEntitiesAsTheCodes_entityNotInCodes() {

    CategoryEntity entity1 = CategoryEntity.builder()
        .code("1")
        .build();

    CategoryEntity entity2 = CategoryEntity.builder()
        .code("2")
        .build();

    List<String> codes = List.of("1");
    List<CategoryEntity> entities = List.of(entity1, entity2);
    var reorderedEntities = EntityUtils.reorderAndFilterEntitiesAsTheCodes(codes, entities,
        CategoryEntity::getCode);

    assertThat(reorderedEntities).extracting(CategoryEntity::getCode).isEqualTo(List.of("1"));
  }

}
