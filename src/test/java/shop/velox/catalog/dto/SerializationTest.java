package shop.velox.catalog.dto;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.json.JacksonTester;
import shop.velox.catalog.AbstractIntegrationTest;
import shop.velox.catalog.dto.product.ProductDto;


class SerializationTest extends AbstractIntegrationTest {

  private JacksonTester<ProductDto> jacksonTester;

  ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
  Validator validator = factory.getValidator();

  @Autowired
  ObjectMapper objectMapper;

  @BeforeEach
  public void setup() {
    JacksonTester.initFields(this, objectMapper);
  }

  @Test
  @SneakyThrows
  void noAttributeValues(){
    String json = """
        {
          "code": "velox-flask",
          "name": "VELOX Flask",
          "description": "<p>Very nice <strong>VELOX</strong> branded thermoflask</p>",
          "type": "SIMPLE_PRODUCT"
        }
        """;

    ProductDto productDto = jacksonTester.parseObject(json);

    assertThat(productDto.getName()).isEqualTo("VELOX Flask");
    assertThat(productDto.getAttributeValues()).isNullOrEmpty();

    var violations = validator.validate(productDto);
    assertThat(violations).isEmpty();
  }

  @Test
  @SneakyThrows
  void singleTextAttributeValues(){
    String json = """
        {
          "code":"velox-polo-shirt-black-s",
          "name":"VELOX Polo Shirt Black (S)",
          "attributeValues":[
            {
              "attribute":{
                "code":"color",
                "name":"Color"
              },
              "values":[
                {
                  "code":"black",
                  "name":"Black"
                }
              ]
            },
            {
              "attribute":{
                "code":"size",
                "name":"Size"
              },
              "values":[
                {
                  "code":"s",
                  "name":"S"
                }
              ]
            }
          ],
          "type":"PRODUCT_VARIANT"
        }
        """;

    ProductDto productDto = jacksonTester.parseObject(json);

    assertThat(productDto.getAttributeValues()).hasSize(2);
    assertThat(productDto.getAttributeValues()).allMatch(
        assignment -> assignment.getValues().size() == 1);

    var violations = validator.validate(productDto);
    assertThat(violations).isEmpty();
  }

  @Test
  @SneakyThrows
  void multiTextAttributeValues(){
    String json = """
        {
          "code":"velox-polo-shirt-black-s",
          "name":"VELOX Polo Shirt Black (S)",
          "attributeValues":[
            {
              "attribute":{
                "code":"material",
                "name":"Material"
              },
              "values":[
                {
                  "code":"aluminum",
                  "name":"Aluminum"
                },
                {
                  "code":"steel",
                  "name":"Steel"
                }
              ]
            }
          ],
          "type":"PRODUCT_VARIANT"
        }
        """;

    ProductDto productDto = jacksonTester.parseObject(json);

    assertThat(productDto.getAttributeValues()).hasSize(1);
    assertThat(productDto.getAttributeValues()).allMatch(
        attribute -> attribute.getValues().size() == 2);

    var violations = validator.validate(productDto);
    assertThat(violations).isEmpty();

  }

  @Test
  @SneakyThrows
  void withChoiceAttributes() {
    String json = """
        {
        "code": "velox-polo-shirt",
            "name": "VELOX Polo Shirt",
            "description": "<p>Very nice <strong>VELOX</strong>&nbsp;branded Polo-Shirt.</p>",
            "images": [
              {
                "type": "GALLERY",
                "url": "https://uploads-ssl.webflow.com/5f4d4a1634bebb66e5a60983/5fe08c57048e93477f527c8a_velox_promotional_materials_mockups-polo1.jpg"
              }
            ],
            "choiceAttributes": [
                {
                  "attribute": {
                    "code":"color",
                    "name":"Color"
                },
                "values": [
                  {
                    "code": "red",
                    "name": "Red"
                  },
                  {
                    "code": "black",
                    "name": "Black"
                  }
                ]
              },
              {
                 "attribute": {
                    "code":"size",
                    "name":"Size"
                },
                "values": [
                  {
                    "code": "s",
                    "name": "S"
                  },
                  {
                    "code": "m",
                    "name": "M"
                  },
                  {
                    "code": "l",
                    "name": "L"
                  }
                ]
              }
            ],
            "type": "MULTI_VARIANT_PRODUCT"
        }
        """;

    ProductDto productDto = jacksonTester.parseObject(json);

    assertThat(productDto.getAttributeValues()).isNullOrEmpty();
    assertThat(productDto.getChoiceAttributes()).hasSize(2);

    var violations = validator.validate(productDto);
    assertThat(violations).isEmpty();
  }


  @Test
  @SneakyThrows
  void missingName() {
    String json = """
        {
          "code":"velox-polo-shirt-black-s",
          "name":"VELOX Polo Shirt Black (S)",
          "attributeValues":[
            {
              "attribute":{
                "code":"material",
                "name":"Material"
              },
              "values":[
                {
                  "code":"aluminum"
                }
              ]
            }
          ],
          "type":"PRODUCT_VARIANT"
        }
        """;

    var e = assertThrows(JsonMappingException.class, () -> jacksonTester.parseObject(json));
    assertThat(e.getMessage()).startsWith("Cannot deserialize node");
  }

  @Test
  @SneakyThrows
  void missingCode() {
    String json = """
        {
          "code":"velox-polo-shirt-black-s",
          "name":"VELOX Polo Shirt Black (S)",
          "attributeValues":[
            {
              "attribute":{
                "code":"material",
                "name":"Material"
              },
              "values":[
                {
                  "name":"Aluminum"
                }
              ]
            }
          ],
          "type":"PRODUCT_VARIANT"
        }
        """;

    var e = assertThrows(JsonMappingException.class, () -> jacksonTester.parseObject(json));
    assertThat(e.getMessage()).startsWith("Cannot deserialize node");
  }

  @Test
  @SneakyThrows
  void emptyValues() {
    String json = """
        {
           "code":"velox-polo-shirt-black-s",
           "name":"VELOX Polo Shirt Black (S)",
           "attributeValues": [],
           "type":"PRODUCT_VARIANT"
        }
        """;

    ProductDto productDto = assertDoesNotThrow(() -> jacksonTester.parseObject(json));

    var violations = validator.validate(productDto);
    assertThat(violations).isEmpty();
  }


}
