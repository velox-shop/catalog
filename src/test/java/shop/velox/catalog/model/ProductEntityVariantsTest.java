package shop.velox.catalog.model;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

import jakarta.annotation.Resource;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.opentest4j.AssertionFailedError;
import org.springframework.test.context.TestPropertySource;
import shop.velox.catalog.AbstractIntegrationTest;
import shop.velox.catalog.dao.ProductRepository;


@TestPropertySource(properties = {
    "logging.level.org.springframework.data.mongodb.core.convert=TRACE"})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ProductEntityVariantsTest extends AbstractIntegrationTest {

  private static final String CATALOG_CODE = "theCatalogCode";

  private static final String PARENT_CODE = "shirt";

  private static final String VARIANT_CODE = "shirt-black-s";

  @Resource
  private ProductRepository productRepository;

  ProductEntity parent;

  @BeforeAll
  void setUp() {

    productRepository.save(ProductEntity.builder()
        .code(VARIANT_CODE)
        .name("Black Shirt S")
        .catalogCode(CATALOG_CODE)
        .type(ProductType.PRODUCT_VARIANT)
        .parentCode(PARENT_CODE)
        .build());

    productRepository.save(ProductEntity.builder()
        .code("other-product")
        .name("Other Product")
        .catalogCode(CATALOG_CODE)
        .type(ProductType.SIMPLE_PRODUCT)
        .build());

    productRepository.save(ProductEntity.builder()
        .code(VARIANT_CODE)
        .name("Black Shirt S")
        .catalogCode("otherCatalog")
        .type(ProductType.PRODUCT_VARIANT)
        .parentCode(PARENT_CODE)
        .build());

    parent = productRepository.save(ProductEntity.builder()
        .code(PARENT_CODE)
        .name("Shirt")
        .catalogCode(CATALOG_CODE)
        .type(ProductType.MULTI_VARIANT_PRODUCT)
        .build());
  }

  @Test
  public void saveAlsoResolvesDocumentReferenceCharacterizationTest() {
    // Expected behaviour is that saving the parent will also resolve the document reference, but this is not the case.
    // To address this, shop.velox.catalog.service.impl.ProductServiceImpl.createProduct and
    // shop.velox.catalog.service.impl.ProductServiceImpl.updateProduct reload the product before returning it.
    // If / When this test will fail, we can remove that workaround
    assertThat(parent.getVariants(), hasSize(0));
  }

  @Test
  public void testGetVariantsOnMultiVariantProduct() {
    ProductEntity result = productRepository.findOneByCodeAndCatalogCode(PARENT_CODE,
        CATALOG_CODE).orElseThrow(() -> new AssertionFailedError("Product not found"));

    // With Following call, MongoDatabaseFactoryReferenceLoader will log at TRACE level:
    //MongoDatabaseFactoryReferenceLoader : Bulk fetching ... from catalog.products
    var variants = result.getVariants();
    assertThat(variants, hasSize(1));
  }

  @Test
  public void testGetVariantsOnProductVariant() {
    ProductEntity result = productRepository.findOneByCodeAndCatalogCode(VARIANT_CODE,
        CATALOG_CODE).orElseThrow(() -> new AssertionFailedError("Product not found"));

    // With Following call, MongoDatabaseFactoryReferenceLoader will log nothing:
    var variants = result.getVariants();
    assertThat(variants, hasSize(0));
  }


}
