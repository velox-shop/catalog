package shop.velox.catalog.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;

@UtilityClass
@Slf4j
public class EntityUtils {

  public static <T> List<T> reorderAndFilterEntitiesAsTheCodes(List<String> codes,
      List<T> entities, Function<T, String> codeExtractor) {

    Assert.notNull(codes, "codes must not be null");
    Assert.notNull(entities, "entities must not be null");
    Assert.notNull(codeExtractor, "codeExtractor must not be null");

    List<T> reorderedEntities = new ArrayList<>();

    for (String code : codes) {
      for (T entity : entities) {
        if (Objects.equals(code, codeExtractor.apply(entity))) {
          reorderedEntities.add(entity);
          break;
        }
      }
    }

    if (log.isDebugEnabled()) {
      log.debug(" entities: {} reordered to: {}",
          entities.stream().map(codeExtractor).toList(),
          reorderedEntities.stream().map(codeExtractor).toList());
    }

    return reorderedEntities;
  }

}
