package shop.velox.catalog.dto.catalog;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import shop.velox.catalog.dto.category.CategoryDto;

@Value
@Builder
@Jacksonized
@FieldNameConstants
@Schema(name = "Catalog")
public class CatalogDto {

  String id;

  @NotBlank
  String code;

  @Builder.Default
  List<CategoryDto> rootCategories = new ArrayList<>();

  @Builder.Default
  List<String> rootCategoryCodes = new ArrayList<>();

}
