package shop.velox.catalog.dto.catalog;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
@FieldNameConstants
@Schema(name = "UpdateCatalog")
public class UpdateCatalogDto {

  @Builder.Default
  List<String> rootCategoryCodes = new ArrayList<>();

}
