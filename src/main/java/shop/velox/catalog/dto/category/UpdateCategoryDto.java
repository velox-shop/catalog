package shop.velox.catalog.dto.category;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import shop.velox.catalog.model.AttributeAssignment;
import shop.velox.catalog.model.CategoryReference;
import shop.velox.catalog.model.CategoryStatus;
import shop.velox.catalog.model.Image;


@Value
@Builder
@Jacksonized
@FieldNameConstants
@Schema(name = "UpdateCategory")
public class UpdateCategoryDto {

  @Schema(description = "Status of the category.", example = "ACTIVE")
  @Builder.Default
  CategoryStatus status = CategoryStatus.ACTIVE;

  @Schema(description = "Unique identifier of the category in external system.",
      example = "1234")
  String externalId;

  @Schema(description = "Name of the category.",
      example = "Paper Note Books")
  String name;

  @Schema(description = "Parent Code of the category.")
  String parentCode;

  @Schema(description = "Description of the category.")
  String description;

  @Schema(description = "Images of the category.")
  List<Image> images;

  @Schema(description = "Attributes of the Category")
  @Valid
  List<? extends AttributeAssignment<?>> attributeValues;

  @Schema(description = "Sub category codes of the category.")
  @Builder.Default
  List<String> subCategoryCodes = new ArrayList<>();

  @Schema(description = "Product ids of the category.")
  @Builder.Default
  List<String> productCodes = new ArrayList<>();

  @Schema(description = "Path of category references,, from root to current category")
  @Builder.Default
  List<CategoryReference> categoryPath = new ArrayList<>();

}
