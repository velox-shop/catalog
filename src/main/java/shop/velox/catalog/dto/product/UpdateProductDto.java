package shop.velox.catalog.dto.product;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import shop.velox.catalog.model.AttributeAssignment;
import shop.velox.catalog.model.CategoryReference;
import shop.velox.catalog.model.Image;
import shop.velox.catalog.model.ProductStatus;
import shop.velox.catalog.model.ProductType;


@Data
@Builder
@Jacksonized
@FieldNameConstants
@Schema(name = "UpdateProduct")
public class UpdateProductDto {

  @Schema(description = "Status of the product", example = "ACTIVE")
  @Builder.Default
  ProductStatus status = ProductStatus.ACTIVE;

  @Schema(description = "Unique identifier of the product in external system.",
      example = "1234")
  String externalId;

  @Schema(description = "Name of the product.", example = "Velox Shirt")
  @NotBlank
  String name;

  @Schema(description = "Description of the product",
      example = "Very nice VELOX branded Polo-Shirt in size XL.")
  String description;

  @Schema(description = "Long description of the product",
      example = "Very nice VELOX branded Polo-Shirt in size XL made from cotton in various colours.")
  String longDescription;

  @Schema(description = "Images of the product")
  @Valid
  List<Image> images;

  @Schema(description = "Attributes of Product with no variants")
  @Valid
  List<? extends AttributeAssignment<?>> attributeValues;

  @Schema(description = "Attributes that change between variants")
  @Valid
  List<? extends AttributeAssignment<?>> choiceAttributes;

  @Schema(description = "Product Type")
  @NotNull
  ProductType type;

  @Schema(description = "code of a parent MULTI_VARIANT_PRODUCT if a product type is VARIANT")
  String parentCode;

  @Schema(description = "Path of category references, from root to category the product is in")
  @Builder.Default
  @Valid
  List<CategoryReference> categoryPath = new ArrayList<>();

}
