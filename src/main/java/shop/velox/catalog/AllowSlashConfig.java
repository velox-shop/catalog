package shop.velox.catalog;

import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.buf.EncodedSolidusHandling;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;

@Configuration
@Slf4j
public class AllowSlashConfig {

  @Bean
  public WebServerFactoryCustomizer<TomcatServletWebServerFactory> tomcatCustomizer() {
    log.info("Allowing encoded slash in URL in Tomcat.");
    return factory -> factory.addConnectorCustomizers(
        connector -> connector.setEncodedSolidusHandling(
            EncodedSolidusHandling.DECODE.getValue()));
  }

  @Bean
  public HttpFirewall allowUrlEncodedSlashHttpFirewall() {
    log.info("Allowing encoded slash in URL in Spring Security.");
    StrictHttpFirewall firewall = new StrictHttpFirewall();
    firewall.setAllowUrlEncodedSlash(true);
    return firewall;
  }
}
