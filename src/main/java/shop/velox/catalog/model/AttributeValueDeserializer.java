package shop.velox.catalog.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import shop.velox.catalog.model.AttributeValue.Fields;

@Slf4j
public class AttributeValueDeserializer extends StdDeserializer<AttributeValue> {

  public AttributeValueDeserializer() {
    this(null);
  }

  public AttributeValueDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public AttributeValue<?> deserialize(JsonParser jp, DeserializationContext ctxt)
      throws IOException {
    JsonNode node = jp.getCodec().readTree(jp);
    try {
      var nodeType = node.getNodeType();
      JsonNode codeNode = node.get(Fields.code);
      String code = codeNode.textValue();

      JsonNode nameNode = node.get(Fields.name);

      if (nameNode.isTextual()) {
        String value = nameNode.textValue();
        return AttributeTextValue.builder().code(code).name(value).build();
      } else {
        throw new IllegalArgumentException(
            "Cannot deserialize nameNode: " + nameNode.toPrettyString());
      }
    } catch (Exception e) {
      throw new IllegalArgumentException(
          "Cannot deserialize node: " + node.toPrettyString(), e);
    }
  }
}
