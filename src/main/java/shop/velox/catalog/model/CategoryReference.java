package shop.velox.catalog.model;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Builder
@Value
@Jacksonized
public class CategoryReference {

  String code;

  String name;

}
