package shop.velox.catalog.model;

import static java.util.Collections.emptyList;
import static shop.velox.catalog.model.ProductType.MULTI_VARIANT_PRODUCT;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;


@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Jacksonized
@FieldNameConstants
@Document(collection = "products")
@CompoundIndex(name = ProductEntity.CATALOG_CODE_AND_CODE_INDEX_NAME, def = "{'catalogCode' : 1, 'code': 1}", unique = true)
@CompoundIndex(name = "catalogCodeAndCodeAndStatus", def = "{'catalogCode' : 1, 'code': 1, 'status': 1}")
@CompoundIndex(name = "parentCodeAndCatalogCode", def = "{'parentCode': 1, 'catalogCode': 1}")
public class ProductEntity {

  public static final String CATALOG_CODE_AND_CODE_INDEX_NAME = "catalogCodeAndCode";

  @Id
  @Builder.Default
  private String id = UUID.randomUUID().toString();

  @NotBlank
  private String code;

  @Builder.Default
  private ProductStatus status = ProductStatus.ACTIVE;

  private String externalId;

  private String name;

  private String description;

  private String longDescription;

  private List<Image> images;

  private List<? extends AttributeAssignment<?>> attributeValues;

  private List<? extends AttributeAssignment<?>> choiceAttributes;

  @NotNull
  private ProductType type;

  private String parentCode;

  @NotBlank
  private String catalogCode;

  @CreatedDate
  @EqualsAndHashCode.Exclude
  private LocalDateTime createdDateTime;

  @LastModifiedDate
  @EqualsAndHashCode.Exclude
  private LocalDateTime lastModifiedDateTime;

  // Needed because we manually set the id
  @Version
  @EqualsAndHashCode.Exclude
  private Long version;

  @Builder.Default
  @ReadOnlyProperty
  @DocumentReference(lookup = "{'parentCode': ?#{#self.code}, 'catalogCode': ?#{#self.catalogCode}}",
      lazy = true)
  @EqualsAndHashCode.Exclude
  @ToString.Exclude
  private List<ProductEntity> variants = new ArrayList<>();

  @NotNull
  @Builder.Default
  private List<CategoryReference> categoryPath = new ArrayList<>();

  public List<ProductEntity> getVariants() {
    return MULTI_VARIANT_PRODUCT.equals(getType()) ? variants : emptyList();
  }
}
