package shop.velox.catalog.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;


@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@SuperBuilder
@FieldDefaults(makeFinal = false, level= AccessLevel.PROTECTED)
@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
@FieldNameConstants
@JsonDeserialize(using = AttributeAssignmentDeserializer.class)
public class AttributeAssignment<T> {

  @Schema(description = "For which Attribute the choice is possible",
      example = """
          {"code":"color","name":"Color"}
          """)
  @NotNull
  Attribute attribute;

  @Schema(description = "Assignment of AttributeValues to an Attribute",
      example = """
          [{"id":"red","name":"Red"},{"id":"black","name":"Black"}]
          """)
  @Valid
  List<? extends AttributeValue<? extends T>> values;
}
