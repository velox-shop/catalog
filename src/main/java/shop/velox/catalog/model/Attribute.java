package shop.velox.catalog.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

@Data
@Builder
@Jacksonized
public class Attribute {

  @Schema(description = "Human-Readable unique identifier of the Attribute.",
      example = "color")
  @NotBlank
  String code;

  @Schema(description = "name of the Attribute.",
      example = "Color")
  String name;

}
