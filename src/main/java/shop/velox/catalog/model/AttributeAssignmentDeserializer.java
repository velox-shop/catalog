package shop.velox.catalog.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import shop.velox.catalog.model.AttributeAssignment.Fields;


@Slf4j
public class AttributeAssignmentDeserializer extends StdDeserializer<AttributeAssignment> {

  public AttributeAssignmentDeserializer() {
    this(null);
  }

  public AttributeAssignmentDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public AttributeAssignment<?> deserialize(JsonParser jp, DeserializationContext ctxt)
      throws IOException {
    JsonNode node = jp.getCodec().readTree(jp);
    try {
      ObjectMapper mapper = new ObjectMapper();

      var attributeNode = node.get(Fields.attribute);
      Attribute attribute = mapper.readValue(attributeNode.toString(), Attribute.class);

      // Add deserialization for Attribute Values
      ArrayNode values = (ArrayNode) node.get(Fields.values);
      List<AttributeValue<String>> attributeValues = Arrays.asList(
          mapper.readValue(values.toString(), new TypeReference<>() {
          }));

      return TextAttributeAssignment.builder().attribute(attribute).values(attributeValues).build();

    } catch (Exception e) {
      throw new IllegalArgumentException(
          "Cannot deserialize node: " + node.toPrettyString());
    }
  }
}
