package shop.velox.catalog.model;

public enum ProductType {
    SIMPLE_PRODUCT,
    PRODUCT_VARIANT,
    MULTI_VARIANT_PRODUCT
}
