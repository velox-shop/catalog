package shop.velox.catalog.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import shop.velox.catalog.dto.category.CategoryDto;

@Value
@Builder
@Jacksonized
@FieldNameConstants
@Document(collection = "catalog")
public class CatalogEntity {

  public static final String UNIQUE_CODE_INDEX_NAME = "uniqueCode";

  @Id
  @Builder.Default
  String id = UUID.randomUUID().toString();

  @Indexed(name = UNIQUE_CODE_INDEX_NAME, unique = true)
  String code;

  @Builder.Default
  List<String> rootCategoryCodes = new ArrayList<>();

  @Builder.Default
  @EqualsAndHashCode.Exclude
  @ToString.Exclude
  List<CategoryDto> rootCategories = new ArrayList<>();

  @CreatedDate
  @EqualsAndHashCode.Exclude
  LocalDateTime createdDateTime;

  @LastModifiedDate
  @EqualsAndHashCode.Exclude
  LocalDateTime lastModifiedDateTime;

  @Version
  @EqualsAndHashCode.Exclude
  Long version;
}
