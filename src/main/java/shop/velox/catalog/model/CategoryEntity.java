package shop.velox.catalog.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Jacksonized
@FieldNameConstants
@Document(collection = "category")
@CompoundIndex(name = CategoryEntity.CATALOG_CODE_AND_CODE_INDEX_NAME,
    def = "{'catalogCode' : 1, 'code': 1}", unique = true)
@CompoundIndex(name = CategoryEntity.CATALOG_CODE_AND_CODE_AND_STATUS_INDEX_NAME,
    def = "{'catalogCode' : 1, 'code': 1, 'status': 1}")
public class CategoryEntity {

  public static final String CATALOG_CODE_AND_CODE_INDEX_NAME = "catalogCodeAndCode";

  public static final String CATALOG_CODE_AND_CODE_AND_STATUS_INDEX_NAME = "catalogCodeAndCodeAndStatus";

  @Id
  @Builder.Default
  private String id = UUID.randomUUID().toString();

  @NotBlank
  private String code;

  @Builder.Default
  private CategoryStatus status = CategoryStatus.ACTIVE;

  private String externalId;

  private String name;

  private String description;

  private List<Image> images;

  private List<? extends AttributeAssignment<?>> attributeValues;

  private String parentCode;

  @Builder.Default
  private List<String> subCategoryCodes = new ArrayList<>();

  /**
   * The {@code @DocumentReference} annotation in Spring Data MongoDB enables the referencing of
   * entities in MongoDB using a flexible schema. It is important to note that the child document
   * must be saved prior to the parent document. This is due to the fact that the parent document
   * references the child document, hence the child document must exist first before it can be
   * referenced.
   * <p>
   * The {@code subCategories} field serves as a reference to other documents in the same collection
   * (category) that have a parentCode value matching the code value of the current document.
   */
  @Builder.Default
  @ReadOnlyProperty
  @DocumentReference(lookup = "{'parentCode': ?#{#self.code}, 'catalogCode': ?#{#self.catalogCode}}", lazy = true)
  @EqualsAndHashCode.Exclude
  @ToString.Exclude
  private List<CategoryEntity> subCategories = new ArrayList<>();

  @Builder.Default
  private List<String> productCodes = new ArrayList<>();

  @NotBlank
  private String catalogCode;

  @NotNull
  @Builder.Default
  private List<CategoryReference> categoryPath = new ArrayList<>();

  @CreatedDate
  @EqualsAndHashCode.Exclude
  private LocalDateTime createdDateTime;

  @LastModifiedDate
  @EqualsAndHashCode.Exclude
  private LocalDateTime lastModifiedDateTime;

  // Needed because we manually set the id
  @Version
  @EqualsAndHashCode.Exclude
  private Long version;

}
