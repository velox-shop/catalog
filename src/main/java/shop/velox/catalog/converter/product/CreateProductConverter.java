package shop.velox.catalog.converter.product;

import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import shop.velox.catalog.dto.product.CreateProductDto;
import shop.velox.catalog.model.ProductEntity;

@Mapper
public interface CreateProductConverter {

  @Mappings({
      @Mapping(target = "createdDateTime", ignore = true),
      @Mapping(target = "lastModifiedDateTime", ignore = true),
  })
  ProductEntity convert(CreateProductDto createProductDto, @Context String catalogCode);

  @AfterMapping
  default void fillCatalogCode(
      @MappingTarget ProductEntity.ProductEntityBuilder productEntityBuilder,
      @Context String catalogCode) {
    productEntityBuilder.catalogCode(catalogCode);
  }

}
