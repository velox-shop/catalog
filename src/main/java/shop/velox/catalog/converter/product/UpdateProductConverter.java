package shop.velox.catalog.converter.product;

import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import shop.velox.catalog.dto.product.UpdateProductDto;
import shop.velox.catalog.model.ProductEntity;

@Mapper
public interface UpdateProductConverter {

  @Mappings({
      @Mapping(target = "createdDateTime", ignore = true),
      @Mapping(target = "lastModifiedDateTime", ignore = true),
  })
  ProductEntity convert(UpdateProductDto updateProductDto,
      @Context ProductEntity oldProductEntity);

  @AfterMapping
  default void fillMissingProperties(
      @MappingTarget ProductEntity.ProductEntityBuilder entityBuilder,
      @Context ProductEntity oldProductEntity) {
    entityBuilder.code(oldProductEntity.getCode())
        .catalogCode(oldProductEntity.getCatalogCode())
        .id(oldProductEntity.getId())
        .version(oldProductEntity.getVersion());
  }

}
