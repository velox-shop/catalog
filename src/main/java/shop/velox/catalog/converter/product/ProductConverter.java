package shop.velox.catalog.converter.product;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

import java.util.Arrays;
import java.util.List;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import shop.velox.catalog.dto.product.ProductDto;
import shop.velox.catalog.model.ProductEntity;
import shop.velox.catalog.model.ProductStatus;

@Mapper
public interface ProductConverter {

  Logger log = LoggerFactory.getLogger(ProductConverter.class);

  @Mappings(
      @Mapping(target = "variants", expression = "java(mapVariants(productEntity, expandVariants, allowedStatuses))")
  )
  ProductDto convertEntityToDto(ProductEntity productEntity, @Context Boolean expandVariants,
      @Context List<ProductStatus> allowedStatuses);

  @Named("mapVariants")
  default List<ProductDto> mapVariants(ProductEntity productEntity, @Context Boolean expandVariants,
      @Context List<ProductStatus> allowedStatuses) {
    var statuses =
        isEmpty(allowedStatuses) ? Arrays.stream(ProductStatus.values()).toList() : allowedStatuses;
    log.debug("mapVariants with productEntity: {}, expandVariants: {} and allowedStatuses: {}",
        productEntity, expandVariants, statuses);
    if (isTrue(expandVariants)) {
      return productEntity.getVariants().stream()
          .filter(variant -> statuses.contains(variant.getStatus()))
          .map(v -> convertEntityToDto(v, expandVariants, statuses))
          .toList();
    } else {
      return List.of();
    }
  }
}
