package shop.velox.catalog.converter.catalog;

import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import shop.velox.catalog.dto.catalog.UpdateCatalogDto;
import shop.velox.catalog.model.CatalogEntity;

@Mapper
public interface UpdateCatalogConverter {

  @Mappings({
      @Mapping(target = "rootCategories", ignore = true),
      @Mapping(target = "createdDateTime", ignore = true),
      @Mapping(target = "lastModifiedDateTime", ignore = true),
  })
  CatalogEntity convert(UpdateCatalogDto updateCatalogDto,
      @Context CatalogEntity oldCatalogEntity);

  @AfterMapping
  default void fillMissingProperties(
      @MappingTarget CatalogEntity.CatalogEntityBuilder catalogEntityBuilder,
      @Context CatalogEntity oldCatalogEntity) {
    catalogEntityBuilder.code(oldCatalogEntity.getCode())
        .id(oldCatalogEntity.getId())
        .version(oldCatalogEntity.getVersion());
  }

}
