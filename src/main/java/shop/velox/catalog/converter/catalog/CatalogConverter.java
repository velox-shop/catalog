package shop.velox.catalog.converter.catalog;

import org.mapstruct.Context;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import shop.velox.catalog.dto.catalog.CatalogDto;
import shop.velox.catalog.model.CatalogEntity;
import shop.velox.commons.converter.Converter;

@Mapper(injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {
    CatalogCategoriesDecorator.class,
})
public interface CatalogConverter extends Converter<CatalogEntity, CatalogDto> {

  // See Small Print in https://projectlombok.org/features/experimental/FieldNameConstants
  @Mappings({
      @Mapping(source = ".", target = "rootCategories"),
  })
  CatalogDto convertEntityToDto(CatalogEntity catalogEntity,
      @Context CatalogConverterContext context);

  @Override
  default CatalogDto convertEntityToDto(CatalogEntity catalogEntity) {
    return convertEntityToDto(catalogEntity, CatalogConverterContext.builder().build());
  }
}
