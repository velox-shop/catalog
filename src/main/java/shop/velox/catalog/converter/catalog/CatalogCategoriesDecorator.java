package shop.velox.catalog.converter.catalog;

import static java.util.Collections.emptyList;

import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Context;
import org.springframework.stereotype.Component;
import shop.velox.catalog.dto.category.CategoryDto;
import shop.velox.catalog.model.CatalogEntity;
import shop.velox.catalog.service.CategoryService;

@Component
@RequiredArgsConstructor
@Slf4j
public class CatalogCategoriesDecorator {

  private final CategoryService categoryService;

  public List<CategoryDto> mapCategories(CatalogEntity catalog,
      @Context CatalogConverterContext context) {
    return getSubcategories(catalog.getCode(), catalog.getRootCategoryCodes(), context.getLevels(),
        context.getExpandProducts(), context.getExpandVariants());
  }

  List<CategoryDto> getSubcategories(String catalogCode, List<String> rootCategoriesCodes,
      int level, boolean expandProducts, boolean expandVariants) {
    log.info("getSubcategories with rootCategoriesIds: {}, level: {}", rootCategoriesCodes, level);
    if (level > 0) {
      return rootCategoriesCodes.stream()
          .map(
              subCategoryCode -> categoryService.getCategory(catalogCode, subCategoryCode,
                      expandProducts, level - 1, expandVariants)
                  .orElseThrow(() -> new RuntimeException(
                      String.format("Cannot find subcategory with code %s", subCategoryCode))))
          .collect(Collectors.toList());
    } else {
      return emptyList();
    }
  }

}
