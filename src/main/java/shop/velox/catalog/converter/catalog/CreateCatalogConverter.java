package shop.velox.catalog.converter.catalog;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import shop.velox.catalog.dto.catalog.CreateCatalogDto;
import shop.velox.catalog.model.CatalogEntity;

@Mapper
public interface CreateCatalogConverter {

  @Mappings({
      @Mapping(target = "rootCategories", ignore = true),
      @Mapping(target = "createdDateTime", ignore = true),
      @Mapping(target = "lastModifiedDateTime", ignore = true),
  })
  CatalogEntity convert(CreateCatalogDto createCatalogDto);

}
