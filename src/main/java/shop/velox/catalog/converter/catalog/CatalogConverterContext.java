package shop.velox.catalog.converter.catalog;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder(toBuilder = true)
@Jacksonized
@FieldNameConstants
public class CatalogConverterContext {

  @Builder.Default
  Integer levels = 0;

  @Builder.Default
  Boolean expandProducts = Boolean.FALSE;

  @Builder.Default
  Boolean expandVariants = Boolean.FALSE;

}
