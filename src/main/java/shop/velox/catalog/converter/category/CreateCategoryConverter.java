package shop.velox.catalog.converter.category;

import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import shop.velox.catalog.dto.category.CreateCategoryDto;
import shop.velox.catalog.model.CategoryEntity;

@Mapper
public interface CreateCategoryConverter {

  @Mappings({
      @Mapping(target = "subCategories", ignore = true),
      @Mapping(target = "createdDateTime", ignore = true),
      @Mapping(target = "lastModifiedDateTime", ignore = true),
  })
  CategoryEntity convert(CreateCategoryDto createCategoryDto, @Context String catalogCode);

  @AfterMapping
  default void fillCatalogCode(
      @MappingTarget CategoryEntity.CategoryEntityBuilder categoryEntityBuilder,
      @Context String catalogCode) {
    categoryEntityBuilder.catalogCode(catalogCode);
  }

}
