package shop.velox.catalog.converter.category;

import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import shop.velox.catalog.dto.category.UpdateCategoryDto;
import shop.velox.catalog.model.CategoryEntity;

@Mapper
public interface UpdateCategoryConverter {

  @Mappings({
      @Mapping(target = "subCategories", ignore = true),
      @Mapping(target = "createdDateTime", ignore = true),
      @Mapping(target = "lastModifiedDateTime", ignore = true),
  })
  CategoryEntity convert(UpdateCategoryDto createCategoryDto,
      @Context CategoryEntity oldCategoryEntity);

  @AfterMapping
  default void fillMissingProperties(
      @MappingTarget CategoryEntity.CategoryEntityBuilder catalogEntityBuilder,
      @Context CategoryEntity oldCategoryEntity) {
    catalogEntityBuilder.code(oldCategoryEntity.getCode())
        .catalogCode(oldCategoryEntity.getCatalogCode())
        .id(oldCategoryEntity.getId())
        .version(oldCategoryEntity.getVersion());
  }

}
