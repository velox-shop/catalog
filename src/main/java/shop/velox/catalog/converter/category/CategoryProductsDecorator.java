package shop.velox.catalog.converter.category;

import static java.util.Collections.emptyList;

import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Context;
import org.springframework.stereotype.Component;
import shop.velox.catalog.dto.product.ProductDto;
import shop.velox.catalog.model.CategoryEntity;
import shop.velox.catalog.model.ProductStatus;
import shop.velox.catalog.service.ProductService;

@Component
@RequiredArgsConstructor
@Slf4j
public class CategoryProductsDecorator {

  private final ProductService productService;

  public List<ProductDto> mapProducts(CategoryEntity category,
      @Context CategoryConverterContext context) {
    if (Boolean.TRUE.equals(context.getExpandProducts())) {
      String catalogCode = category.getCatalogCode();
      List<String> productCodes = category.getProductCodes();
      List<ProductDto> products = productService.getProducts(catalogCode, productCodes,
          context.getExpandVariants(), List.of(ProductStatus.ACTIVE));
      log.debug(
          "mapProducts with catalogCode: {}, categoryCode: {} productCodes: {} expandVariants: {} finds {} ACTIVE products",
          catalogCode, category.getCode(), productCodes, context.getExpandVariants(),
          products.size());
      return products;
    } else {
      return emptyList();
    }
  }

}
