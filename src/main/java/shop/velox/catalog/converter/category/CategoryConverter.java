package shop.velox.catalog.converter.category;

import static shop.velox.catalog.model.CategoryStatus.ACTIVE;

import java.util.List;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import shop.velox.catalog.dto.category.CategoryDto;
import shop.velox.catalog.model.CategoryEntity;
import shop.velox.commons.converter.Converter;

@Mapper(uses = {
    CategoryProductsDecorator.class
})
public interface CategoryConverter extends Converter<CategoryEntity, CategoryDto> {

  @Mappings({
      @Mapping(target = "subCategories", expression = "java(mapSubCategories(categoryEntity, context))"),
      @Mapping(source = ".", target = "products"),
  })
  CategoryDto convertEntityToDto(CategoryEntity categoryEntity,
      @Context CategoryConverterContext context);

  @Override
  default CategoryDto convertEntityToDto(CategoryEntity categoryEntity) {
    return convertEntityToDto(categoryEntity, CategoryConverterContext.builder().build());
  }

  default List<CategoryDto> mapSubCategories(CategoryEntity categoryEntity,
      CategoryConverterContext context) {
    Integer levels = context.getLevels();
    return levels > 0 ?
        categoryEntity.getSubCategories()
            .stream()
            .filter(c -> ACTIVE == c.getStatus())
            .map(c -> convertEntityToDto(c, context.toBuilder().levels(levels - 1).build()))
            .toList()
        : java.util.Collections.emptyList();
  }

}
