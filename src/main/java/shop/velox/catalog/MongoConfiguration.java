package shop.velox.catalog;

import static org.apache.commons.collections4.IterableUtils.isEmpty;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.mapping.event.AfterConvertCallback;
import org.springframework.data.mongodb.core.mapping.event.ValidatingMongoEventListener;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import shop.velox.catalog.model.CategoryEntity;
import shop.velox.catalog.utils.EntityUtils;

@Configuration
@Slf4j
public class MongoConfiguration {

  @Bean
  public ValidatingMongoEventListener validatingMongoEventListener() {
    return new ValidatingMongoEventListener(validator());
  }

  @Bean
  public LocalValidatorFactoryBean validator() {
    return new LocalValidatorFactoryBean();
  }

  @Bean
  public AfterConvertCallback<CategoryEntity> categoryEntityAfterConvertCallback() {
    return (entity, document, collection) -> {
      List<String> subCategoryCodes = entity.getSubCategoryCodes();
      List<CategoryEntity> subCategories = entity.getSubCategories();

      if (isEmpty(subCategories) || isEmpty(subCategoryCodes)) {
        return entity;
      }

      try {
        var sortedSubCategories = EntityUtils.reorderAndFilterEntitiesAsTheCodes(
            subCategoryCodes, subCategories, CategoryEntity::getCode
        );
        entity.setSubCategories(sortedSubCategories);
      } catch (IllegalArgumentException e) {
        log.error("Failed to reorder sub categories for category: {}", entity.getCode(), e);
      }
      return entity;
    };
  }
}
