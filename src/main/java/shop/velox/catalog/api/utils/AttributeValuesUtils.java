package shop.velox.catalog.api.utils;

import static java.util.Collections.emptyList;
import static org.apache.commons.collections4.ListUtils.emptyIfNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import shop.velox.catalog.model.Attribute;
import shop.velox.catalog.model.AttributeAssignment;
import shop.velox.catalog.model.AttributeTextValue;
import shop.velox.catalog.model.AttributeValue;
import shop.velox.catalog.model.TextAttributeAssignment;

@UtilityClass
@Slf4j
public class AttributeValuesUtils {

  public static String getSingleValueProperty(List<? extends AttributeAssignment<?>> attributeValues, String attributeCode) {
    return emptyIfNull(attributeValues).stream()
        .filter(attribute -> attributeCode.equals(attribute.getAttribute().getCode()))
        .findFirst()
        .map(TextAttributeAssignment.class::cast)
        .map(TextAttributeAssignment::getValues)
        .filter(CollectionUtils::isNotEmpty)
        .map(List::getFirst)
        .map(AttributeTextValue.class::cast)
        .map(AttributeTextValue::getName)
        .orElse(null);
  }

  public static List<String> getMultiVaueProperty(String attributeCode,
      List<? extends AttributeAssignment<?>> attributeValues) {
    return Optional.ofNullable(attributeValues)
        .orElse(Collections.emptyList())
        .stream()
        .filter(attributeAssignment -> attributeCode.equals(
            attributeAssignment.getAttribute().getCode()))
        .map(AttributeAssignment::getValues)
        .flatMap(List::stream)
        .filter(AttributeTextValue.class::isInstance)
        .map(AttributeTextValue.class::cast)
        .map(AttributeValue::getName)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }

  public List<? extends AttributeAssignment<?>> setSingleValueProperty(List<? extends AttributeAssignment<?>> oldAttributeValues, String attributeCode, String value) {
    Optional<? extends AttributeAssignment<?>> attributeAssignmentOptional = oldAttributeValues.stream()
        .filter(attribute -> attributeCode.equals(attribute.getAttribute().getCode()))
        .findFirst();
    if (attributeAssignmentOptional.isPresent()) {
      AttributeAssignment<String> attributeAssignment = (AttributeAssignment<String>) attributeAssignmentOptional.get();
      Optional<AttributeTextValue> attributeTextValueOptional = attributeAssignmentOptional
          .map(TextAttributeAssignment.class::cast)
          .map(TextAttributeAssignment::getValues)
          .filter(CollectionUtils::isNotEmpty)
          .map(List::getFirst)
          .map(AttributeTextValue.class::cast);
      attributeTextValueOptional
          .ifPresentOrElse(
              attributeTextValue -> attributeTextValue.setName(value),
              () -> {
                log.warn("Attribute value not found for attribute code: {}", attributeCode);
                attributeAssignment.setValues(
                    List.of(AttributeTextValue.builder()
                        .code(attributeCode)
                        .name(value)
                        .build()));
              }
          );
      return oldAttributeValues;
    } else {
      List<AttributeAssignment<?>> attributeValues = new ArrayList<>(oldAttributeValues);
      AttributeAssignment<String> attributeAssignment = TextAttributeAssignment.builder()
          .attribute(Attribute.builder().code(attributeCode).build())
          .values(List.of(AttributeTextValue.builder()
              .code(attributeCode)
              .name(value)
              .build()))
          .build();
      attributeValues.add(attributeAssignment);
      return attributeValues;
    }
  }

  public static void deleteSingleValueProperty(
      List<? extends AttributeAssignment<?>> attributeAssignments, String attributeCode) {
    attributeAssignments.stream()
        .filter(attribute -> attributeCode.equals(attribute.getAttribute().getCode()))
        .findFirst()
        .map(TextAttributeAssignment.class::cast)
        .ifPresent(attribute -> attribute.setValues(emptyList()));
  }

}
