package shop.velox.catalog.api.controller.impl;

import java.time.LocalDateTime;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.catalog.api.controller.CategoryController;
import shop.velox.catalog.dto.category.CategoryDto;
import shop.velox.catalog.dto.category.CreateCategoryDto;
import shop.velox.catalog.dto.category.UpdateCategoryDto;
import shop.velox.catalog.model.CategoryStatus;
import shop.velox.catalog.service.CategoryService;

@RestController
@RequiredArgsConstructor
@Slf4j
public class CategoryControllerImpl implements CategoryController {

  private final CategoryService categoryService;

  @Override
  public CategoryDto getCategoryByCode(String catalogCode,
      String categoryCode, Integer expandCategoryLevels, Boolean expandProduct,
      Boolean expandVariants) {
    log.debug(
        "getCategoryByCode: catalogCode={}, categoryCode={}, expandCategoryLevels={}, expandProduct={}, expandVariants={}",
        catalogCode, categoryCode, expandCategoryLevels, expandProduct, expandVariants);
    return categoryService.getCategory(catalogCode, categoryCode, expandProduct,
            expandCategoryLevels, expandVariants)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
  }


  @Override
  public Page<CategoryDto> getCategoriesByCatalogCode(String catalogCode,
      Integer expandCategoryLevels, Boolean expandProducts, Boolean expandVariants,
      String productCode, CategoryStatus categoryStatus, LocalDateTime notModifiedAfter,
      Pageable pageable) {
    log.debug(
        "getCategoriesByCatalogCode: catalogCode={}, expandCategoryLevels={}, expandProducts={}, expandVariants={}, productCode={}, categoryStatus: {}, notModifiedAfter: {},  pageable={}",
        catalogCode, expandCategoryLevels, expandProducts, expandVariants, productCode,
        categoryStatus, notModifiedAfter, pageable);
    return categoryService.getCategoriesByCatalogCode(catalogCode,
        expandProducts, expandCategoryLevels, expandVariants, productCode, categoryStatus,
        notModifiedAfter, pageable);
  }

  @Override
  public ResponseEntity<CategoryDto> createCategory(String categoryCode,
      CreateCategoryDto categoryDto) {
    var createdCategory = categoryService.createCategory(categoryCode, categoryDto);
    return ResponseEntity.status(HttpStatus.CREATED)
        .body(createdCategory);
  }

  @Override
  public ResponseEntity<CategoryDto> saveCategory(String catalogCode, String categoryCode,
      UpdateCategoryDto categoryDto) {
    return ResponseEntity.status(HttpStatus.OK)
        .body(categoryService.updateCategory(catalogCode, categoryCode, categoryDto));
  }

  @Override
  public ResponseEntity<Void> deleteCategory(String catalogCode, String categoryCode) {
    categoryService.deleteCategoryByCatalogCodeAndCode(catalogCode, categoryCode);
    return ResponseEntity.noContent()
        .build();
  }

}
