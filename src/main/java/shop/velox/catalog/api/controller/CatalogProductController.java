package shop.velox.catalog.api.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import shop.velox.catalog.dto.product.CreateProductDto;
import shop.velox.catalog.dto.product.ProductDto;
import shop.velox.catalog.dto.product.UpdateProductDto;
import shop.velox.catalog.model.ProductStatus;
import shop.velox.catalog.model.ProductType;

@Tag(name = "Product", description = "the product API")
@RequestMapping(value = "/catalogs/{catalogCode}/products", produces = APPLICATION_JSON_VALUE)
public interface CatalogProductController {

  String PARAM_NAME_ALLOWED_STATUSES = "allowedStatuses";
  String PARAM_NAME_EXPAND_VARIANTS = "expandVariants";
  String PARAM_NAME_PRODUCT_CODES = "productCodes";
  String PARAM_NAME_PRODUCT_TYPE = "productType";
  String PARAM_NAME_SEARCH_TERM = "searchTerm";

  @Operation(summary = "Fetch all products of a catalog")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation")
  })
  @GetMapping
  Page<ProductDto> getProducts(
      @Parameter(description = "Catalog Code. Cannot be empty.", required = true)
      @PathVariable("catalogCode") String catalogCode,

      @Parameter(description = "Optional Product Type Filter")
      @RequestParam(name = PARAM_NAME_PRODUCT_TYPE, required = false)
      @Nullable ProductType productType,

      @Parameter(description = "Optional search term")
      @RequestParam(name = PARAM_NAME_SEARCH_TERM, required = false) String searchTerm,

      @Parameter(description = "Should fetch also the variants of the products")
      @RequestParam(name = PARAM_NAME_EXPAND_VARIANTS, defaultValue = "true") boolean expandVariants,

      @Parameter(description = "Optional filter for the product status. If empty, all Statuses will be returned")
      @RequestParam(name = PARAM_NAME_ALLOWED_STATUSES, required = false)
      List<ProductStatus> allowedStatuses,

      Pageable pageable);

  @Operation(summary = "Fetch multiple products by catalog code and product code")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation")
  })
  @GetMapping(params = "productCodes")
  List<ProductDto> getProductsByCode(
      @Parameter(description = "Catalog Code. Cannot be empty.", required = true)
      @PathVariable("catalogCode") String catalogCode,

      @Size(min = 1, max = 50)
      @Parameter(description = "product codes. Cannot be empty.", required = true)
      @RequestParam(name = PARAM_NAME_PRODUCT_CODES) List<String> productCodes,

      @Parameter(description = "Should fetch also the variants of the products")
      @RequestParam(name = PARAM_NAME_EXPAND_VARIANTS, defaultValue = "true") boolean expandVariants,

      @Parameter(description = "Optional filter for the product status. If empty, all Statuses will be returned")
      @RequestParam(name = PARAM_NAME_ALLOWED_STATUSES, required = false)
      List<ProductStatus> allowedStatuses

  );


  @Operation(summary = "fetch product by catalog Code and product code")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "returns the product for given catalog id and product code",
          content = @Content(schema = @Schema(implementation = ProductDto.class))),
  })
  @GetMapping(value = "/{productCode:.+}/**")
  ProductDto getProductByCatalogCodeAndProductCode(
      @Parameter(description = "Catalog Code for the given product. Cannot be empty.", required = true)
      @PathVariable("catalogCode") String catalogCode,

      @Parameter(description = "Product Code. Cannot be empty.", required = true)
      @PathVariable("productCode") String productCode,

      @Parameter(description = "Optional filter for the status of the variants. If empty, all Statuses will be returned")
      @RequestParam(name = PARAM_NAME_ALLOWED_STATUSES, required = false)
      List<ProductStatus> allowedStatuses,

      HttpServletRequest request);


  @Operation(summary = "Creates a product")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201",
          description = "Product successfully created, the saved version is returned",
          content = @Content(schema = @Schema(implementation = ProductDto.class))),
      @ApiResponse(responseCode = "400",
          description = "Product not valid",
          content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "409",
          description = "Product Already exists",
          content = @Content(schema = @Schema()))
  })
  @PostMapping
  ResponseEntity<ProductDto> createProduct(
      @Parameter(description = "Desired Catalog Code. Cannot be empty.", required = true)
      @PathVariable("catalogCode") String catalogCode,

      @Parameter(description = "The product to create.")
      @Valid @RequestBody CreateProductDto productDto
  );

  @Operation(summary = "Updates a Product")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "Product successfully saved, the saved version is returned",
          content = @Content(schema = @Schema(implementation = ProductDto.class))),
      @ApiResponse(responseCode = "404",
          description = "Product not found",
          content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "400",
          description = "Product not valid",
          content = @Content(schema = @Schema())),
  })
  @PutMapping(value = "/{productCode:.+}/**")
  ResponseEntity<ProductDto> saveProduct(
      @Parameter(description = "Desired Catalog Code. Cannot be empty.", required = true)
      @PathVariable("catalogCode") String catalogCode,

      @Parameter(description = "Product Code. Cannot be empty.", required = true)
      @PathVariable("productCode") String productCode,

      @Parameter(description = "The product to create.")
      @Valid @RequestBody UpdateProductDto productDto,

      HttpServletRequest request
  );


  @Operation(summary = "Deletes a Product")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "204",
          description = "Product successfully deleted",
          content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "404",
          description = "Catalog or Product not found",
          content = @Content(schema = @Schema())),
  })
  @DeleteMapping(value = "/{productCode:.+}/**")
  ResponseEntity<Void> deleteProduct(
      @Parameter(description = "Code of the catalog. Cannot be empty.", required = true)
      @PathVariable("catalogCode") String catalogCode,

      @Parameter(description = "product Code. Cannot be empty.", required = true)
      @PathVariable("productCode") String productCode,

      HttpServletRequest request
  );

}
