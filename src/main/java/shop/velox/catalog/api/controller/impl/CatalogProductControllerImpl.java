package shop.velox.catalog.api.controller.impl;

import jakarta.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.catalog.api.controller.CatalogProductController;
import shop.velox.catalog.dto.product.CreateProductDto;
import shop.velox.catalog.dto.product.ProductDto;
import shop.velox.catalog.dto.product.UpdateProductDto;
import shop.velox.catalog.model.ProductStatus;
import shop.velox.catalog.model.ProductType;
import shop.velox.catalog.service.ProductService;

@RestController
@RequiredArgsConstructor
@Slf4j
public class CatalogProductControllerImpl implements CatalogProductController {

  private final ProductService productService;

  @Override
  public Page<ProductDto> getProducts(String catalogCode, @Nullable ProductType productType,
      @Nullable String searchTerm, boolean expandVariants, List<ProductStatus> allowedStatuses,
      Pageable pageable) {
    log.debug("Fetch all Products with term {} ant type: {} from catalog: {} with pageable: {}",
        searchTerm, productType, catalogCode, pageable);
    return productService.getProducts(catalogCode, productType, searchTerm, expandVariants,
        allowedStatuses, pageable);
  }

  @Override
  public List<ProductDto> getProductsByCode(String catalogCode, List<String> productCodes,
      boolean expandVariants, List<ProductStatus> allowedStatuses) {
    log.debug("Fetch Products with codes: {} from catalog: {}", productCodes, catalogCode);
    return productService.getProducts(catalogCode, productCodes, expandVariants, allowedStatuses);
  }

  @Override
  public ProductDto getProductByCatalogCodeAndProductCode(String catalogCode, String productCode,
      List<ProductStatus> allowedStatuses, HttpServletRequest request) {
    String realProductCode = getRealProductCode(productCode, request.getRequestURI());
    log.debug("Fetch Product with code: {} and statuses: {} from catalog: {}", allowedStatuses,
        realProductCode, catalogCode);
    return productService.getProductByCatalogCodeAndCode(catalogCode, realProductCode,
            allowedStatuses)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
  }

  private static String getRealProductCode(String productCode, String requestUri) {
    try {
      URI uri = new URI(requestUri);
      String uriPath = uri.getPath();
      int startIndex = uriPath.indexOf(productCode);
      String realProductCode = uriPath.substring(startIndex);
      if (!StringUtils.equals(productCode, realProductCode)) {
        log.info("productCode: {}, realProductCode: {}", productCode, realProductCode);
      }
      return realProductCode;
    } catch (Exception e) {
      log.error("Error while getting real product code", e);
      return productCode;
    }
  }

  @Override
  public ResponseEntity<ProductDto> createProduct(String catalogCode, CreateProductDto productDto) {
    log.debug("Creating Product: {}", productDto);
    var createdProduct = productService.createProduct(productDto, catalogCode);
    return ResponseEntity.status(HttpStatus.CREATED)
        .body(createdProduct);
  }

  @Override
  public ResponseEntity<ProductDto> saveProduct(String catalogCode, String productCode,
      UpdateProductDto productDto, HttpServletRequest request) {
    String realProductCode = getRealProductCode(productCode, request.getRequestURI());
    return ResponseEntity.status(HttpStatus.OK)
        .body(productService.updateProduct(catalogCode, realProductCode, productDto));
  }

  @Override
  public ResponseEntity<Void> deleteProduct(String catalogCode, String productCode,
      HttpServletRequest request) {
    String realProductCode = getRealProductCode(productCode, request.getRequestURI());
    productService.deleteProductByCatalogCodeAndCode(catalogCode, realProductCode);
    return ResponseEntity.noContent()
        .build();
  }

}
