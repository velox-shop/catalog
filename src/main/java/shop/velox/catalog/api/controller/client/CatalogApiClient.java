package shop.velox.catalog.api.controller.client;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;

import java.net.URI;
import java.util.Map;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.catalog.dto.catalog.CatalogDto;
import shop.velox.catalog.dto.catalog.CreateCatalogDto;
import shop.velox.catalog.dto.catalog.UpdateCatalogDto;
import shop.velox.commons.rest.response.RestResponsePage;

@Slf4j
public class CatalogApiClient {

  private final RestTemplate restTemplate;

  private final String baseUrl;

  public CatalogApiClient(RestTemplate restTemplate, String hostUrl) {
    this.restTemplate = restTemplate;
    this.baseUrl = hostUrl + "/catalog/v1/catalogs";
  }

  public ResponseEntity<CatalogDto> createCatalog(CreateCatalogDto payload) {

    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<CreateCatalogDto> httpEntity = new HttpEntity<>(payload, headers);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .build()
        .toUri();

    log.debug("createCatalog with URI: {}", uri);

    return restTemplate.exchange(uri, POST, httpEntity, CatalogDto.class);
  }

  public ResponseEntity<CatalogDto> saveCatalog(String catalogCode, UpdateCatalogDto payload) {

    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<UpdateCatalogDto> httpEntity = new HttpEntity<>(payload, headers);

    var uriPathParams = Map.of("catalogCode", catalogCode);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("{catalogCode}")
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("saveCatalog with URI: {}", uri);

    return restTemplate.exchange(uri, PUT, httpEntity, CatalogDto.class);
  }

  public ResponseEntity<Page<CatalogDto>> getCatalogs() {
    return getCatalogs(null);
  }

  public ResponseEntity<Page<CatalogDto>> getCatalogs(@Nullable Pageable pageable) {

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .queryParamIfPresent("page", Optional.ofNullable(pageable).map(Pageable::getPageNumber))
        .queryParamIfPresent("size", Optional.ofNullable(pageable).map(Pageable::getPageSize))
        .queryParamIfPresent("sort",
            Optional.ofNullable(pageable).map(Pageable::getSort).map(ApiClientUtils::toQueryParam))
        .build()
        .toUri();

    log.debug("getCatalogs with URI: {}", uri);

    ParameterizedTypeReference<RestResponsePage<CatalogDto>> responseType = new ParameterizedTypeReference<>() {
    };

    ResponseEntity<RestResponsePage<CatalogDto>> result = restTemplate.exchange(uri, GET,
        null/*httpEntity*/, responseType);

    return new ResponseEntity<>(result.getBody(), result.getHeaders(), result.getStatusCode());
  }

  public ResponseEntity<CatalogDto> getCatalog(String catalogCode,
      Integer expandCategoryLevels, Boolean expandProducts, Boolean expandVariants) {

    var uriPathParams = Map.of("catalogCode", catalogCode);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("{catalogCode}")
        .queryParamIfPresent("expandCategoryLevels", Optional.ofNullable(expandCategoryLevels))
        .queryParamIfPresent("expandProducts", Optional.ofNullable(expandProducts))
        .queryParamIfPresent("expandVariants", Optional.ofNullable(expandVariants))
        .buildAndExpand(uriPathParams).toUri();

    log.debug("getCatalog with URI: {}", uri);

    return restTemplate.exchange(uri, GET, null/*httpEntity*/, CatalogDto.class);
  }

}
