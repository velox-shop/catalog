package shop.velox.catalog.api.controller.client;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;

import java.io.Serializable;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.catalog.dto.category.CategoryDto;
import shop.velox.catalog.dto.category.CreateCategoryDto;
import shop.velox.catalog.dto.category.UpdateCategoryDto;
import shop.velox.catalog.model.CategoryStatus;
import shop.velox.commons.rest.response.RestResponsePage;

@Slf4j
public class CategoryApiClient {

  private final RestTemplate restTemplate;

  private final String baseUrl;

  public CategoryApiClient(RestTemplate restTemplate, String hostUrl) {
    this.restTemplate = restTemplate;
    this.baseUrl = hostUrl + "/catalog/v1/catalogs/{catalogCode}/categories";
  }

  public ResponseEntity<CategoryDto> getCategory(String catalogCode, String categoryCode,
      Integer expandCategoryLevels, Boolean expandProducts, Boolean expandVariants) {

    Map<String, ? extends Serializable> uriPathParams = Map.of(
        "catalogCode", catalogCode,
        "categoryCode", categoryCode);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("{categoryCode}")
        .queryParamIfPresent("expandCategoryLevels", Optional.ofNullable(expandCategoryLevels))
        .queryParamIfPresent("expandProducts", Optional.ofNullable(expandProducts))
        .queryParamIfPresent("expandVariants", Optional.ofNullable(expandVariants))
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("getCategory with URI: {}", uri);

    return restTemplate.exchange(uri, GET, null/*httpEntity*/, CategoryDto.class);
  }

  public ResponseEntity<? extends Page<CategoryDto>> getCategories(String catalogCode,
      Integer expandCategoryLevels, Boolean expandProducts, Boolean expandVariants,
      @Nullable String productCode) {
    return getCategories(catalogCode, expandCategoryLevels, expandProducts, expandVariants,
        productCode, CategoryStatus.ACTIVE, null, null);
  }

  public ResponseEntity<? extends Page<CategoryDto>> getCategories(String catalogCode,
      LocalDateTime notModifiedAfter, @Nullable CategoryStatus categoryStatus) {
    return getCategories(catalogCode, 0, false, false,
        null, categoryStatus, notModifiedAfter, null);
  }

  public ResponseEntity<? extends Page<CategoryDto>> getCategories(String catalogCode,
      Integer expandCategoryLevels, Boolean expandProducts, Boolean expandVariants,
      @Nullable String productCode, @Nullable CategoryStatus categoryStatus,
      @Nullable LocalDateTime notModifiedAfter,
      @Nullable Pageable pageable) {

    Map<String, ? extends Serializable> uriPathParams = Map.of("catalogCode", catalogCode);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .queryParamIfPresent("expandCategoryLevels", Optional.ofNullable(expandCategoryLevels))
        .queryParamIfPresent("expandProducts", Optional.ofNullable(expandProducts))
        .queryParamIfPresent("expandVariants", Optional.ofNullable(expandVariants))
        .queryParamIfPresent("productCode", Optional.ofNullable(productCode))
        .queryParamIfPresent("status", Optional.ofNullable(categoryStatus))
        .queryParamIfPresent("notModifiedAfter", Optional.ofNullable(notModifiedAfter))
        .queryParamIfPresent("page", Optional.ofNullable(pageable).map(Pageable::getPageNumber))
        .queryParamIfPresent("size", Optional.ofNullable(pageable).map(Pageable::getPageSize))
        .queryParamIfPresent("sort",
            Optional.ofNullable(pageable).map(Pageable::getSort).map(ApiClientUtils::toQueryParam))
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("getCategories with URI: {}", uri);

    ParameterizedTypeReference<RestResponsePage<CategoryDto>> responseType = new ParameterizedTypeReference<>() {
    };
    return restTemplate.exchange(uri, GET, null/*httpEntity*/, responseType);
  }

  public ResponseEntity<CategoryDto> createCategory(String catalogCode,
      CreateCategoryDto payload) {

    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<CreateCategoryDto> httpEntity = new HttpEntity<>(payload, headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of("catalogCode", catalogCode);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("createCategory with URI: {}", uri);

    return restTemplate.exchange(uri, POST, httpEntity, CategoryDto.class);
  }

  public ResponseEntity<CategoryDto> saveCategory(String catalogCode, String categoryCode,
      UpdateCategoryDto payload) {

    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<UpdateCategoryDto> httpEntity = new HttpEntity<>(payload, headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of(
        "catalogCode", catalogCode,
        "categoryCode", categoryCode);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("{categoryCode}")
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("updateCategory with URI: {}", uri);

    return restTemplate.exchange(uri, PUT, httpEntity, CategoryDto.class);
  }
}
