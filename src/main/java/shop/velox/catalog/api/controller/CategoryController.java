package shop.velox.catalog.api.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.time.LocalDateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import shop.velox.catalog.dto.category.CategoryDto;
import shop.velox.catalog.dto.category.CreateCategoryDto;
import shop.velox.catalog.dto.category.UpdateCategoryDto;
import shop.velox.catalog.model.CategoryStatus;

@Tag(name = "Category", description = "the category API")
@RequestMapping(value = "/catalogs/{catalogCode}/categories", produces = APPLICATION_JSON_VALUE)
public interface CategoryController {

  @Operation(summary = "return categories for given catalog Code and category Code, based on the filter conditions expandProduct/expandCategoryLevels")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "returns the category list for given catalog Code ",
          content = @Content(schema = @Schema(implementation = CategoryDto.class)))
  })
  @GetMapping(value = "/{categoryCode}")
  CategoryDto getCategoryByCode(
      @Parameter(description = "Catalog Code. Cannot be empty.", required = true)
      @PathVariable("catalogCode") String catalogCode,

      @Parameter(description = "Category Code. Cannot be empty.", required = true)
      @PathVariable("categoryCode") String categoryCode,

      @Parameter(description = "How many category levels will be populated")
      @RequestParam(value = "expandCategoryLevels", defaultValue = "0") Integer expandCategoryLevels,

      @Parameter(description = "if products DTOs have to be populated")
      @RequestParam(value = "expandProducts", defaultValue = "false") Boolean expandProduct,

      @Parameter(description = "Should fetch also the variants of the products")
      @RequestParam(defaultValue = "false") Boolean expandVariants
  );

  @Operation(summary = "return categories by catalog Code and expandCategoryLevels/productCode")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "returns the category list for given catalog id, based on the request parameters",
          content = @Content(schema = @Schema(implementation = CategoryDto.class)))
  })
  @GetMapping
  Page<CategoryDto> getCategoriesByCatalogCode(
      @Parameter(description = "Catalog Code. Cannot be empty.", required = true)
      @PathVariable("catalogCode") String catalogCode,

      @Parameter(description = "How many category levels will be populated")
      @RequestParam(value = "expandCategoryLevels", defaultValue = "0") Integer expandCategoryLevels,

      @Parameter(description = "if products DTOs have to be populated")
      @RequestParam(value = "expandProducts", defaultValue = "false") Boolean expandProduct,

      @Parameter(description = "Should fetch also the variants of the products")
      @RequestParam(defaultValue = "false") Boolean expandVariants,

      @Parameter(description = "Filtered categories have to contain given productCode")
      @RequestParam(required = false) String productCode,

      @Parameter(description = "Optional filter on the CategoryStatus")
      @RequestParam(required = false, value = "status") CategoryStatus categoryStatus,

      @Parameter(description = "Not Modified After this given Date")
      @RequestParam(required = false) LocalDateTime notModifiedAfter,

      Pageable pageable
  );


  @Operation(summary = "Creates a Category")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201",
          description = "Category successfully created, the saved version is returned",
          content = @Content(schema = @Schema(implementation = CategoryDto.class))),
      @ApiResponse(responseCode = "400",
          description = "Category not valid",
          content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "409",
          description = "Category Already exists",
          content = @Content(schema = @Schema()))
  })
  @PostMapping
  ResponseEntity<CategoryDto> createCategory(
      @Parameter(description = "Desired Catalog Code. Cannot be empty.", required = true)
      @PathVariable("catalogCode") String catalogCode,

      @Parameter(description = "The Category to create.")
      @Valid @RequestBody CreateCategoryDto categoryDto
  );

  @Operation(summary = "Updates a Category")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "Category successfully saved, the saved version is returned",
          content = @Content(schema = @Schema(implementation = CategoryDto.class))),
      @ApiResponse(responseCode = "404",
          description = "Category not found",
          content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "400",
          description = "Category not valid",
          content = @Content(schema = @Schema())),
  })
  @PutMapping(value = "/{categoryCode}")
  ResponseEntity<CategoryDto> saveCategory(
      @Parameter(description = "Desired Catalog Code. Cannot be empty.", required = true)
      @PathVariable("catalogCode") String catalogCode,

      @Parameter(description = "Category Code. Cannot be empty.", required = true)
      @PathVariable("categoryCode") String categoryCode,

      @Parameter(description = "The Category to save.")
      @Valid @RequestBody UpdateCategoryDto categoryDto
  );


  @Operation(summary = "Deletes a Category")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "204",
          description = "Category successfully deleted",
          content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "404",
          description = "Catalog or Category not found",
          content = @Content(schema = @Schema())),
  })
  @DeleteMapping(value = "/{categoryCode}")
  ResponseEntity<Void> deleteCategory(
      @Parameter(description = "Code of the catalog. Cannot be empty.", required = true)
      @PathVariable("catalogCode") String catalogCode,

      @Parameter(description = "Category Code. Cannot be empty.", required = true)
      @PathVariable("categoryCode") String categoryCode
  );

}
