package shop.velox.catalog.api.controller.client;

import static shop.velox.catalog.api.controller.CatalogProductController.PARAM_NAME_ALLOWED_STATUSES;
import static shop.velox.catalog.api.controller.CatalogProductController.PARAM_NAME_EXPAND_VARIANTS;
import static shop.velox.catalog.api.controller.CatalogProductController.PARAM_NAME_PRODUCT_TYPE;
import static shop.velox.catalog.api.controller.CatalogProductController.PARAM_NAME_SEARCH_TERM;

import java.io.Serializable;
import java.net.URI;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.catalog.api.controller.client.ProductApiClient.GetProductsRequestParams;

@UtilityClass
@Slf4j
public class ApiClientUtils {

  public static URI getProductsUri(String baseUrl, String catalogCode, GetProductsRequestParams requestParams) {
    Map<String, ? extends Serializable> uriPathParams = Map.of("catalogCode", catalogCode);
    var pageable = requestParams.getPageable();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .queryParamIfPresent(PARAM_NAME_PRODUCT_TYPE,
            Optional.ofNullable(requestParams.getProductType()))
        .queryParamIfPresent(PARAM_NAME_SEARCH_TERM,
            Optional.ofNullable(requestParams.getSearchTerm()))
        .queryParam(PARAM_NAME_EXPAND_VARIANTS, requestParams.isExpandVariants())
        .queryParam(PARAM_NAME_ALLOWED_STATUSES, requestParams.getAllowedStatuses().toArray())
        .queryParamIfPresent("page", Optional.ofNullable(pageable).map(Pageable::getPageNumber))
        .queryParamIfPresent("size", Optional.ofNullable(pageable).map(Pageable::getPageSize))
        .queryParamIfPresent("sort",
            Optional.ofNullable(pageable).map(Pageable::getSort).map(ApiClientUtils::toQueryParam))
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("searchProduct with URI: {}", uri);
    return uri;
  }

  public static String toQueryParam(Sort sort) {
    return sort.stream()
        .map(order -> order.getProperty() + "," + order.getDirection())
        .collect(Collectors.joining(","));
  }

}
