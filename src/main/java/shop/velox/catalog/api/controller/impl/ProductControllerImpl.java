package shop.velox.catalog.api.controller.impl;

import static org.springframework.http.HttpStatus.NOT_FOUND;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.catalog.api.controller.ProductController;
import shop.velox.catalog.dto.product.ProductDto;
import shop.velox.catalog.service.ProductService;

@RestController
@RequiredArgsConstructor
@Slf4j
public class ProductControllerImpl implements ProductController {

  private final ProductService productService;

  @Override
  public ProductDto getProductById(String productId) {
    log.info("getProductById: {}", productId);
    return productService.getProductById(productId)
        .orElseThrow(() -> new ResponseStatusException(NOT_FOUND));
  }
}
