package shop.velox.catalog.api.controller.client;

import static org.apache.commons.lang3.BooleanUtils.isTrue;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;
import static shop.velox.catalog.api.controller.CatalogProductController.PARAM_NAME_ALLOWED_STATUSES;
import static shop.velox.catalog.api.controller.CatalogProductController.PARAM_NAME_EXPAND_VARIANTS;
import static shop.velox.catalog.api.controller.CatalogProductController.PARAM_NAME_PRODUCT_CODES;
import static shop.velox.catalog.api.controller.client.ApiClientUtils.getProductsUri;

import jakarta.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.catalog.dto.product.CreateProductDto;
import shop.velox.catalog.dto.product.ProductDto;
import shop.velox.catalog.dto.product.UpdateProductDto;
import shop.velox.catalog.model.ProductStatus;
import shop.velox.catalog.model.ProductType;
import shop.velox.commons.rest.response.RestResponsePage;

@Slf4j
public class ProductApiClient {

  private final RestTemplate restTemplate;

  private final String baseUrl;

  public ProductApiClient(RestTemplate restTemplate, String hostUrl) {
    this.restTemplate = restTemplate;
    this.baseUrl = hostUrl + "/catalog/v1/catalogs/{catalogCode}/products";
  }

  public ResponseEntity<ProductDto> createProduct(CreateProductDto payload,
      String catalogCode) {

    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<CreateProductDto> httpEntity = new HttpEntity<>(payload, headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of("catalogCode", catalogCode);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("createProduct with URI: {}", uri);

    return restTemplate.exchange(uri, POST, httpEntity, ProductDto.class);
  }

  public ResponseEntity<ProductDto> saveProduct(UpdateProductDto payload,
      String catalogCode, String productCode) {

    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<UpdateProductDto> httpEntity = new HttpEntity<>(payload, headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of(
        "catalogCode", catalogCode,
        "productCode", productCode);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("{productCode}")
        .buildAndExpand(uriPathParams)
        .encode(StandardCharsets.UTF_8)
        .toUri();

    log.debug("saveProduct with URI: {}", uri);

    return restTemplate.exchange(uri, PUT, httpEntity, ProductDto.class);
  }

  @Deprecated // use getProducts with GetProductsRequestParams
  public ResponseEntity<? extends Page<ProductDto>> getProducts(String catalogCode,
      @Nullable ProductType productType, @Nullable Pageable pageable) {
    return getProducts(catalogCode, GetProductsRequestParams.builder()
        .productType(productType)
        .pageable(pageable)
        .build());
  }

  @Deprecated // use getProducts with GetProductsRequestParams
  public ResponseEntity<? extends Page<ProductDto>> getProducts(String catalogCode,
      Boolean expandVariants) {
    return getProducts(catalogCode, expandVariants, (Pageable) null);
  }

  @Deprecated // use getProducts with GetProductsRequestParams
  public ResponseEntity<? extends Page<ProductDto>> getProducts(String catalogCode,
      Boolean expandVariants, @Nullable Pageable pageable) {
    return getProducts(catalogCode, GetProductsRequestParams.builder()
        .expandVariants(isTrue(expandVariants))
        .pageable(pageable)
        .build());
  }

  @Deprecated // use getProducts with GetProductsRequestParams
  public ResponseEntity<? extends Page<ProductDto>> searchProducts(String catalogCode,
      @Nullable String searchTerm, Boolean expandVariants) {
    return getProducts(catalogCode, GetProductsRequestParams.builder()
        .searchTerm(searchTerm)
        .expandVariants(isTrue(expandVariants))
        .build());
  }

  @Deprecated // use getProducts with GetProductsRequestParams
  public ResponseEntity<? extends Page<ProductDto>> searchProducts(String catalogCode,
      @Nullable ProductType productType, @Nullable String searchTerm, Boolean expandVariants,
      @Nullable Pageable pageable) {
    return getProducts(catalogCode, GetProductsRequestParams.builder()
        .productType(productType)
        .searchTerm(searchTerm)
        .expandVariants(isTrue(expandVariants))
        .pageable(pageable)
        .build());
  }

  public ResponseEntity<? extends Page<ProductDto>> getProducts(String catalogCode,
      GetProductsRequestParams requestParams) {
    URI uri = getProductsUri(baseUrl, catalogCode, requestParams);
    ParameterizedTypeReference<RestResponsePage<ProductDto>> responseType = new ParameterizedTypeReference<>() {
    };
    return restTemplate.exchange(uri, GET, null/*httpEntity*/, responseType);
  }

  public ResponseEntity<ProductDto> getProduct(String catalogCode, String productCode) {
    return getProduct(catalogCode, productCode, List.of());
  }

  public ResponseEntity<ProductDto> getProduct(String catalogCode, String productCode,
      List<ProductStatus> allowedStatuses) {

    Map<String, ? extends Serializable> uriPathParams = Map.of(
        "catalogCode", catalogCode,
        "productCode", productCode);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("{productCode}")
        .queryParam(PARAM_NAME_ALLOWED_STATUSES, allowedStatuses.toArray())
        .buildAndExpand(uriPathParams)
        .encode(StandardCharsets.UTF_8)
        .toUri();

    log.debug("getProduct with URI: {}", uri);

    return restTemplate.exchange(uri, GET, null, ProductDto.class);
  }

  @Deprecated // use getProductsByCodes with GetProductsByCodesRequestParams
  public ResponseEntity<List<ProductDto>> getProductsByCodes(String catalogCode,
      List<String> productCodes) {
    return getProductsByCodes(catalogCode, GetProductsByCodesRequestParams.builder()
        .productCodes(productCodes)
        .expandVariants(true)
        .build());
  }

  public ResponseEntity<List<ProductDto>> getProductsByCodes(String catalogCode,
      GetProductsByCodesRequestParams requestParams) {

    Map<String, ? extends Serializable> uriPathParams = Map.of("catalogCode", catalogCode);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .queryParam(PARAM_NAME_PRODUCT_CODES, requestParams.getProductCodes().toArray())
        .queryParam(PARAM_NAME_EXPAND_VARIANTS, requestParams.isExpandVariants())
        .queryParam(PARAM_NAME_ALLOWED_STATUSES, requestParams.getAllowedStatuses().toArray())
        .buildAndExpand(uriPathParams).toUri();

    log.debug("getProductsByCodes with URI: {}", uri);

    ParameterizedTypeReference<List<ProductDto>> responseType = new ParameterizedTypeReference<>() {
    };

    return restTemplate.exchange(uri, GET, null, responseType);
  }

  @Data
  @Builder
  @Jacksonized
  @Validated
  public static class GetProductsByCodesRequestParams {

    @NotEmpty
    List<String> productCodes;

    @Builder.Default
    boolean expandVariants = true;

    @Builder.Default
    List<ProductStatus> allowedStatuses = List.of(ProductStatus.ACTIVE);
  }

  @Data
  @Builder
  @Jacksonized
  @Validated
  public static class GetProductsRequestParams {

    ProductType productType;

    String searchTerm;

    @Builder.Default
    boolean expandVariants = true;

    @Builder.Default
    List<ProductStatus> allowedStatuses = List.of(ProductStatus.ACTIVE);

    Pageable pageable;
  }


}
