package shop.velox.catalog.api.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import shop.velox.catalog.dto.product.ProductDto;

@Tag(name = "Product", description = "the product API")
@RequestMapping(value = "/products", produces = APPLICATION_JSON_VALUE)
public interface ProductController {

  @Operation(summary = "fetch product by id")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "returns the product for given product id",
          content = @Content(schema = @Schema(implementation = ProductDto.class))),
  })
  @GetMapping(value = "/{productId}")
  ProductDto getProductById(
      @Parameter(description = "Product id. Cannot be empty.", required = true)
      @PathVariable("productId") String productId);

}
