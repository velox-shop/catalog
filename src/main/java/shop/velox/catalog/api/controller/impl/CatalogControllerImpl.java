package shop.velox.catalog.api.controller.impl;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.catalog.api.controller.CatalogController;
import shop.velox.catalog.dto.catalog.CatalogDto;
import shop.velox.catalog.dto.catalog.CreateCatalogDto;
import shop.velox.catalog.dto.catalog.UpdateCatalogDto;
import shop.velox.catalog.service.CatalogService;

@RestController
@RequiredArgsConstructor
@Slf4j
public class CatalogControllerImpl implements CatalogController {

  private final CatalogService catalogService;

  @Override
  public Page<CatalogDto> getCatalogs(Pageable pageable) {
    return catalogService.getCatalogs(pageable);
  }

  @Override
  public CatalogDto getCatalogByCode(String catalogCode,
      Integer expandCategoryLevels, Boolean expandProduct, Boolean expandVariants) {
    log.info("getCatalogByCode with catalogCode:{}, expandCategoryLevels: {}, expandProduct: {}",
        catalogCode, expandCategoryLevels, expandProduct);
    return catalogService.getCatalogByCode(catalogCode, expandCategoryLevels,
            expandProduct, expandVariants)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
  }

  @Override
  public ResponseEntity<CatalogDto> createCatalog(CreateCatalogDto catalogDto) {
    var createdCatalog = catalogService.createCatalog(catalogDto);
    return ResponseEntity.status(CREATED)
        .body(createdCatalog);
  }

  @Override
  public ResponseEntity<CatalogDto> saveCatalog(String catalogCode,
      UpdateCatalogDto catalogDto) {
    return ResponseEntity.status(OK)
        .body(catalogService.updateCatalog(catalogCode, catalogDto));
  }

  @Override
  public ResponseEntity<Void> deleteCatalog(String catalogCode) {
    catalogService.deleteCatalogByCode(catalogCode);
    return ResponseEntity.status(NO_CONTENT)
        .build();
  }

}
