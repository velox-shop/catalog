package shop.velox.catalog.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import shop.velox.catalog.dto.catalog.CatalogDto;
import shop.velox.catalog.dto.catalog.CatalogDto.Fields;
import shop.velox.catalog.dto.catalog.CreateCatalogDto;
import shop.velox.catalog.dto.catalog.UpdateCatalogDto;

@Tag(name = "Catalog", description = "the Catalog API")
@RequestMapping(value = "/catalogs", produces = MediaType.APPLICATION_JSON_VALUE)
public interface CatalogController {

  @Operation(summary = "Get Catalogs, sorted by code", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "successful operation",
          content = @Content(schema = @Schema(implementation = CatalogDto.class))),
  })
  @GetMapping
  Page<CatalogDto> getCatalogs(
      @SortDefault.SortDefaults({@SortDefault(sort = Fields.code, direction = Sort.Direction.ASC)})
      Pageable pageable);


  @Operation(summary = "fetch a catalog by its code", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "returns the product list for given catalog code",
          content = @Content(schema = @Schema(implementation = CatalogDto.class))),
  })
  @GetMapping(value = "/{catalogCode}")
  CatalogDto getCatalogByCode(
      @Parameter(description = "Desired Catalog Code. Cannot be empty.", required = true)
      @PathVariable("catalogCode") String catalogCode,

      @Parameter(description = "How many category levels will be populated")
      @RequestParam(value = "expandCategoryLevels", defaultValue = "0") Integer expandCategoryLevels,

      @Parameter(description = "if products DTOs have to be populated")
      @RequestParam(value = "expandProducts", defaultValue = "false") Boolean expandProducts,

      @Parameter(description = "Should fetch also the variants of the products")
      @RequestParam(defaultValue = "true") Boolean expandVariants
  );


  @Operation(summary = "Creates a Catalog")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201",
          description = "Catalog successfully created, the saved version is returned",
          content = @Content(schema = @Schema(implementation = CatalogDto.class))),
      @ApiResponse(responseCode = "400",
          description = "Catalog not valid",
          content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "409",
          description = "Catalog Already exists",
          content = @Content(schema = @Schema()))
  })
  @PostMapping
  ResponseEntity<CatalogDto> createCatalog(
      @Parameter(description = "The catalog to create.")
      @Valid @RequestBody CreateCatalogDto catalogDto
  );


  @Operation(summary = "Updates a Catalog")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "Catalog successfully saved, the saved version is returned",
          content = @Content(schema = @Schema(implementation = CatalogDto.class))),
      @ApiResponse(responseCode = "404",
          description = "Catalog does not exist",
          content = @Content(schema = @Schema()))
  })
  @PutMapping(value = "/{catalogCode}")
  ResponseEntity<CatalogDto> saveCatalog(
      @Parameter(description = "Desired Catalog Code. Cannot be empty.", required = true)
      @PathVariable("catalogCode") String catalogCode,

      @Parameter(description = "The catalog to create.")
      @Valid @RequestBody UpdateCatalogDto catalogDto
  );

  @Operation(summary = "Deletes a Catalog")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "204",
          description = "Catalog successfully deleted",
          content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "404",
          description = "Catalog not found",
          content = @Content(schema = @Schema())),
  })
  @DeleteMapping(value = "/{catalogCode}")
  ResponseEntity<Void> deleteCatalog(
      @Parameter(description = "Code of the catalog to delete. Cannot be empty.", required = true)
      @PathVariable("catalogCode") String catalogCode
  );
}
