package shop.velox.catalog;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@SpringBootApplication(
    scanBasePackages = {"shop.velox.catalog", "shop.velox.commons"},
    exclude = {SecurityAutoConfiguration.class}
)
@RequiredArgsConstructor
@EnableMongoAuditing
public class CatalogApplication {

  public static void main(String[] args) {
    SpringApplication.run(CatalogApplication.class, args);
  }


}