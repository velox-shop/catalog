package shop.velox.catalog.dao;

import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import shop.velox.catalog.model.CategoryEntity;

public interface CategoryRepository extends MongoRepository<CategoryEntity, String>,
    CategoryRepositoryCustom {

  Optional<CategoryEntity> findByCodeAndCatalogCode(String code, String catalogCode);

  void deleteOneByCodeAndCatalogCode(String code, String catalogCode);

  boolean existsByCodeAndCatalogCode(String code, String catalogCode);
}
