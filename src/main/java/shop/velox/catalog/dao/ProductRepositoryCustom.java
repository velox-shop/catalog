package shop.velox.catalog.dao;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import shop.velox.catalog.model.ProductEntity;
import shop.velox.catalog.model.ProductStatus;
import shop.velox.catalog.model.ProductType;

public interface ProductRepositoryCustom {

  Page<ProductEntity> findProducts(String catalogCode, ProductType productType, String name,
      List<ProductStatus> allowedStatuses, Pageable pageable);

  List<ProductEntity> findProducts(String catalogCode, List<String> productCodes,
      List<ProductStatus> allowedStatuses);
}
