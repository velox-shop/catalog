package shop.velox.catalog.dao;

import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import shop.velox.catalog.model.ProductEntity;

public interface ProductRepository extends MongoRepository<ProductEntity, String>,
    ProductRepositoryCustom {

  Optional<ProductEntity> findOneByCodeAndCatalogCode(String code, String catalogCode);

  void deleteOneByCodeAndCatalogCode(String code, String catalogCode);

  boolean existsByCodeAndCatalogCode(String code, String catalogCode);

}
