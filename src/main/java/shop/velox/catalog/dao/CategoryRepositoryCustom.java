package shop.velox.catalog.dao;

import java.time.LocalDateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;
import shop.velox.catalog.model.CategoryEntity;
import shop.velox.catalog.model.CategoryStatus;

public interface CategoryRepositoryCustom {

  Page<CategoryEntity> find(String catalogCode,
      @Nullable String productCode, @Nullable CategoryStatus status,
      @Nullable LocalDateTime notModifiedAfter, Pageable pageable);

}
