package shop.velox.catalog.dao;

import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import shop.velox.catalog.model.CatalogEntity;

@Repository
public interface CatalogRepository extends MongoRepository<CatalogEntity, String> {

  Optional<CatalogEntity> findOneByCode(String code);

  void deleteByCode(String code);

  boolean existsByCode(String code);

}
