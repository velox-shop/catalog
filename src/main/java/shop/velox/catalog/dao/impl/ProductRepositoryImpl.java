package shop.velox.catalog.dao.impl;

import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static shop.velox.catalog.utils.EntityUtils.reorderAndFilterEntitiesAsTheCodes;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import shop.velox.catalog.dao.ProductRepositoryCustom;
import shop.velox.catalog.model.ProductEntity;
import shop.velox.catalog.model.ProductEntity.Fields;
import shop.velox.catalog.model.ProductStatus;
import shop.velox.catalog.model.ProductType;

@Repository
@RequiredArgsConstructor
public class ProductRepositoryImpl implements ProductRepositoryCustom {

  private final MongoTemplate mongoTemplate;

  @Override
  public Page<ProductEntity> findProducts(String catalogCode, ProductType productType, String name,
      List<ProductStatus> allowedStatuses, Pageable pageable) {
    Query query = new Query();

    query.addCriteria(Criteria.where("catalogCode").is(catalogCode));

    if (productType != null) {
      query.addCriteria(Criteria.where("type").is(productType));
    }

    if (StringUtils.hasText(name)) {
      query.addCriteria(Criteria.where("name").regex(name, "i"));
    }

    if (isNotEmpty(allowedStatuses)) {
      query.addCriteria(Criteria.where(Fields.status).in(allowedStatuses));
    }

    long total = mongoTemplate.count(query, ProductEntity.class);
    query.with(pageable);

    List<ProductEntity> products = mongoTemplate.find(query, ProductEntity.class);

    return new PageImpl<>(products, pageable, total);
  }

  @Override
  public List<ProductEntity> findProducts(String catalogCode, List<String> productCodes,
      List<ProductStatus> allowedStatuses) {
    Query query = new Query();

    query.addCriteria(Criteria.where(Fields.catalogCode).is(catalogCode));

    query.addCriteria(Criteria.where(Fields.code).in(productCodes));

    if (isNotEmpty(allowedStatuses)) {
      query.addCriteria(Criteria.where(Fields.status).in(allowedStatuses));
    }

    List<ProductEntity> products = mongoTemplate.find(query, ProductEntity.class);

    return reorderAndFilterEntitiesAsTheCodes(productCodes, products, ProductEntity::getCode);
  }
}
