package shop.velox.catalog.dao.impl;

import java.time.LocalDateTime;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import shop.velox.catalog.dao.CategoryRepositoryCustom;
import shop.velox.catalog.model.CategoryEntity;
import shop.velox.catalog.model.CategoryEntity.Fields;
import shop.velox.catalog.model.CategoryStatus;
import shop.velox.catalog.model.ProductEntity;

@Repository
@RequiredArgsConstructor
public class CategoryRepositoryImpl implements CategoryRepositoryCustom {

  private final MongoTemplate mongoTemplate;

  @Override
  public Page<CategoryEntity> find(String catalogCode,
      @Nullable String productCode, @Nullable CategoryStatus status,
      @Nullable LocalDateTime notModifiedAfter, Pageable pageable) {

    Query query = new Query();

    query.addCriteria(Criteria.where(Fields.catalogCode).is(catalogCode));

    if (productCode != null) {
      query.addCriteria(Criteria.where(Fields.productCodes).in(productCode));
    }

    if (status != null) {
      query.addCriteria(Criteria.where(Fields.status).is(status));
    }

    if (notModifiedAfter != null) {
      query.addCriteria(Criteria.where(Fields.lastModifiedDateTime).lt(notModifiedAfter));
    }

    long total = mongoTemplate.count(query, ProductEntity.class);
    query.with(pageable);

    List<CategoryEntity> categories = mongoTemplate.find(query, CategoryEntity.class);

    return new PageImpl<>(categories, pageable, total);
  }
}
