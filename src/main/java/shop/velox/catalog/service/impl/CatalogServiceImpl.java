package shop.velox.catalog.service.impl;

import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.catalog.converter.catalog.CatalogConverter;
import shop.velox.catalog.converter.catalog.CatalogConverterContext;
import shop.velox.catalog.converter.catalog.CreateCatalogConverter;
import shop.velox.catalog.converter.catalog.UpdateCatalogConverter;
import shop.velox.catalog.dao.CatalogRepository;
import shop.velox.catalog.dto.catalog.CatalogDto;
import shop.velox.catalog.dto.catalog.CreateCatalogDto;
import shop.velox.catalog.dto.catalog.UpdateCatalogDto;
import shop.velox.catalog.model.CatalogEntity;
import shop.velox.catalog.service.CatalogService;

@Service
@RequiredArgsConstructor
@Slf4j
public class CatalogServiceImpl implements CatalogService {

  private final CatalogRepository catalogRepository;

  private final CatalogConverter catalogConverter;

  private final CreateCatalogConverter createCatalogConverter;

  private final UpdateCatalogConverter updateCatalogConverter;

  @Override
  public Page<CatalogDto> getCatalogs(Pageable pageable) {
    return catalogRepository.findAll(pageable)
        .map(catalogConverter::convertEntityToDto);
  }

  @Override
  public Optional<CatalogDto> getCatalogByCode(String catalogCode, Integer expandCategoryLevels,
      Boolean expandProduct, Boolean expandVariants) {
    return catalogRepository.findOneByCode(catalogCode)
        .map(catalog -> catalogConverter.convertEntityToDto(catalog,
            CatalogConverterContext.builder()
                .levels(expandCategoryLevels)
                .expandProducts(expandProduct)
                .expandVariants(expandVariants)
                .build()));
  }

  @Override
  public CatalogDto createCatalog(CreateCatalogDto catalogDto) {
    var catalogEntityToSave = createCatalogConverter.convert(catalogDto);
    CatalogEntity savedCatalogEntity;
    try {
      savedCatalogEntity = catalogRepository.save(catalogEntityToSave);
    } catch (DuplicateKeyException e) {
      log.error(e.getMessage());
      throw new ResponseStatusException(CONFLICT);
    }
    return catalogConverter.convertEntityToDto(savedCatalogEntity);
  }

  @Override
  public void deleteCatalogByCode(String catalogCode) {
    if (catalogRepository.existsByCode(catalogCode)) {
      catalogRepository.deleteByCode(catalogCode);
    } else {
      throw new ResponseStatusException(NOT_FOUND);
    }
  }

  @Override
  public CatalogDto updateCatalog(String catalogCode, UpdateCatalogDto catalogDto) {
    CatalogEntity oldEntity = catalogRepository.findOneByCode(catalogCode)
        .orElseThrow(() -> new ResponseStatusException(NOT_FOUND));

    CatalogEntity entityToSave = updateCatalogConverter.convert(catalogDto, oldEntity);

    CatalogEntity savedEntity = catalogRepository.save(entityToSave);

    return catalogConverter.convertEntityToDto(savedEntity);
  }


}
