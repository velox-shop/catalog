package shop.velox.catalog.service.impl;

import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.catalog.converter.product.CreateProductConverter;
import shop.velox.catalog.converter.product.ProductConverter;
import shop.velox.catalog.converter.product.UpdateProductConverter;
import shop.velox.catalog.dao.ProductRepository;
import shop.velox.catalog.dto.product.CreateProductDto;
import shop.velox.catalog.dto.product.ProductDto;
import shop.velox.catalog.dto.product.UpdateProductDto;
import shop.velox.catalog.model.ProductEntity;
import shop.velox.catalog.model.ProductStatus;
import shop.velox.catalog.model.ProductType;
import shop.velox.catalog.service.ProductService;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductServiceImpl implements ProductService {

  private final ProductRepository productRepository;

  private final ProductConverter productConverter;

  private final CreateProductConverter createProductConverter;

  private final UpdateProductConverter updateProductConverter;

  @Override
  public List<ProductDto> getProducts(String catalogCode, List<String> productCodes,
      boolean expandVariants, List<ProductStatus> allowedStatuses) {
    return productRepository.findProducts(catalogCode, productCodes, allowedStatuses)
        .stream()
        .map(entity -> productConverter.convertEntityToDto(entity, expandVariants, allowedStatuses))
        .toList();
  }

  @Override
  public Page<ProductDto> getProducts(String catalogCode, @Nullable ProductType productType,
      @Nullable String searchText, boolean expandVariants, List<ProductStatus> allowedStatuses,
      Pageable pageable) {
    Page<ProductEntity> products = productRepository.findProducts(catalogCode, productType,
        searchText, allowedStatuses, pageable);
    return products.map(
        entity -> productConverter.convertEntityToDto(entity, expandVariants, allowedStatuses));
  }

  @Override
  public Optional<ProductDto> getProductByCatalogCodeAndCode(String catalogCode,
      String productCode, List<ProductStatus> allowedStatuses) {
    return productRepository.findOneByCodeAndCatalogCode(productCode, catalogCode)
        .map(entity -> productConverter.convertEntityToDto(entity, true,
            allowedStatuses));
  }

  @Override
  public Optional<ProductDto> getProductById(String productId) {
    return productRepository.findById(productId)
        .map(entity -> productConverter.convertEntityToDto(entity, true,
            Arrays.stream(ProductStatus.values()).toList()));
  }

  @Override
  public ProductDto createProduct(CreateProductDto productDto, String catalogCode) {
    var productEntityToSave = createProductConverter.convert(productDto, catalogCode);
    ProductEntity savedProductEntity;
    try {
      savedProductEntity = productRepository.save(productEntityToSave);
    } catch (DuplicateKeyException e) {
      log.error("When creating Product with catalogCode: {} and code: {}, error is: {}",
          catalogCode, productDto.getCode(), e.getMessage());
      throw new ResponseStatusException(CONFLICT);
    }
    return productConverter.convertEntityToDto(
        productRepository.findById(savedProductEntity.getId()).get(), true,
        Arrays.stream(ProductStatus.values()).toList());
  }

  @Override
  public void deleteProductByCatalogCodeAndCode(String catalogCode, String code) {
    if (productRepository.existsByCodeAndCatalogCode(code, catalogCode)) {
      productRepository.deleteOneByCodeAndCatalogCode(code, catalogCode);
    } else {
      throw new ResponseStatusException(NOT_FOUND);
    }
  }

  @Override
  public ProductDto updateProduct(String catalogCode, String productCode,
      UpdateProductDto updateProductDto) {
    ProductEntity oldEntity = productRepository.findOneByCodeAndCatalogCode(productCode,
            catalogCode)
        .orElseThrow(() -> new ResponseStatusException(NOT_FOUND));

    ProductEntity entityToSave = updateProductConverter.convert(updateProductDto, oldEntity);

    ProductEntity savedEntity = productRepository.save(entityToSave);

    return productConverter.convertEntityToDto(
        productRepository.findById(savedEntity.getId()).get(), true,
        Arrays.stream(ProductStatus.values()).toList());
  }

}
