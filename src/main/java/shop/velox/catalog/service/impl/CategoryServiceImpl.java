package shop.velox.catalog.service.impl;

import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.time.LocalDateTime;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.catalog.converter.category.CategoryConverter;
import shop.velox.catalog.converter.category.CategoryConverterContext;
import shop.velox.catalog.converter.category.CreateCategoryConverter;
import shop.velox.catalog.converter.category.UpdateCategoryConverter;
import shop.velox.catalog.dao.CategoryRepository;
import shop.velox.catalog.dto.category.CategoryDto;
import shop.velox.catalog.dto.category.CreateCategoryDto;
import shop.velox.catalog.dto.category.UpdateCategoryDto;
import shop.velox.catalog.model.CategoryEntity;
import shop.velox.catalog.model.CategoryStatus;
import shop.velox.catalog.service.CategoryService;

@Service
@RequiredArgsConstructor
@Slf4j
public class CategoryServiceImpl implements CategoryService {

  private final CategoryRepository categoryRepository;

  private final CategoryConverter categoryConverter;

  private final CreateCategoryConverter createCategoryConverter;

  private final UpdateCategoryConverter updateCategoryConverter;


  @Override
  public Page<CategoryDto> getCategoriesByCatalogCode(String catalogCode, Boolean expandProducts,
      Integer expandCategoryLevels, Boolean expandVariants, String productCode,
      CategoryStatus categoryStatus, LocalDateTime notModifiedAfter, Pageable pageable) {
    Page<CategoryEntity> categoryEntities = categoryRepository.find(
        catalogCode, productCode, categoryStatus, notModifiedAfter, pageable);

    return categoryEntities.map(
        categoryEntity -> categoryConverter.convertEntityToDto(categoryEntity,
            CategoryConverterContext.builder()
                .levels(expandCategoryLevels)
                .expandProducts(expandProducts)
                .expandVariants(expandVariants)
                .build()));
  }

  @Override
  public Optional<CategoryDto> getCategory(String catalogCode, String categoryCode,
      Boolean expandProducts, Integer expandCategoryLevels, Boolean expandVariants) {
    return categoryRepository.findByCodeAndCatalogCode(categoryCode, catalogCode)
        .map(categoryEntity -> categoryConverter.convertEntityToDto(categoryEntity,
            CategoryConverterContext.builder()
                .levels(expandCategoryLevels)
                .expandProducts(expandProducts)
                .expandVariants(expandVariants)
                .build()));
  }

  @Override
  public CategoryDto createCategory(String catalogCode, CreateCategoryDto categoryDto) {
    var categoryEntityToSave = createCategoryConverter.convert(categoryDto, catalogCode);
    CategoryEntity savedCategoryEntity;
    try {
      savedCategoryEntity = categoryRepository.save(categoryEntityToSave);
    } catch (DuplicateKeyException e) {
      log.error(e.getMessage());
      throw new ResponseStatusException(CONFLICT);
    }
    return categoryConverter.convertEntityToDto(savedCategoryEntity);
  }

  @Override
  public void deleteCategoryByCatalogCodeAndCode(String catalogCode, String code) {
    if (categoryRepository.existsByCodeAndCatalogCode(code, catalogCode)) {
      categoryRepository.deleteOneByCodeAndCatalogCode(code, catalogCode);
    } else {
      throw new ResponseStatusException(NOT_FOUND);
    }
  }

  @Override
  public CategoryDto updateCategory(String catalogCode, String categoryCode,
      UpdateCategoryDto updateCategoryDto) {
    CategoryEntity oldEntity = categoryRepository.findByCodeAndCatalogCode(categoryCode,
            catalogCode)
        .orElseThrow(() -> new ResponseStatusException(NOT_FOUND));

    CategoryEntity entityToSave = updateCategoryConverter.convert(updateCategoryDto, oldEntity);

    CategoryEntity savedEntity = categoryRepository.save(entityToSave);

    return categoryConverter.convertEntityToDto(savedEntity);
  }

}
