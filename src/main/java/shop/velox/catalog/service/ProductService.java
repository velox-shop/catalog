package shop.velox.catalog.service;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;
import org.springframework.security.access.prepost.PreAuthorize;
import shop.velox.catalog.dto.product.CreateProductDto;
import shop.velox.catalog.dto.product.ProductDto;
import shop.velox.catalog.dto.product.UpdateProductDto;
import shop.velox.catalog.model.ProductStatus;
import shop.velox.catalog.model.ProductType;

public interface ProductService {

  List<ProductDto> getProducts(String catalogCode, List<String> productCodes,
      boolean expandVariants, List<ProductStatus> allowedStatuses);

  Page<ProductDto> getProducts(String catalogCode, @Nullable ProductType productType,
      @Nullable String searchText, boolean expandVariants, List<ProductStatus> allowedStatuses,
      Pageable pageable);

  Optional<ProductDto> getProductByCatalogCodeAndCode(String catalogCode, String code,
      List<ProductStatus> allowedStatuses);

  Optional<ProductDto> getProductById(String id);

  @PreAuthorize("@veloxAuthorizationEvaluator.hasGlobalAdminAuthority(authentication)")
  ProductDto createProduct(CreateProductDto productDto, String catalogCode);

  @PreAuthorize("@veloxAuthorizationEvaluator.hasGlobalAdminAuthority(authentication)")
  void deleteProductByCatalogCodeAndCode(String catalogCode, String code);

  @PreAuthorize("@veloxAuthorizationEvaluator.hasGlobalAdminAuthority(authentication)")
  ProductDto updateProduct(String catalogCode, String productCode,
      UpdateProductDto updateProductDto);

}
