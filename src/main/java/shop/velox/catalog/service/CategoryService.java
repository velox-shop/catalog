package shop.velox.catalog.service;

import java.time.LocalDateTime;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import shop.velox.catalog.dto.category.CategoryDto;
import shop.velox.catalog.dto.category.CreateCategoryDto;
import shop.velox.catalog.dto.category.UpdateCategoryDto;
import shop.velox.catalog.model.CategoryStatus;

public interface CategoryService {

  Page<CategoryDto> getCategoriesByCatalogCode(String catalogCode, Boolean expandProducts,
      Integer expandCategoryLevels, Boolean expandVariants, String productCode,
      CategoryStatus categoryStatus, LocalDateTime notModifiedAfter, Pageable pageable);

  Optional<CategoryDto> getCategory(String catalogCode, String categoryCode, Boolean expandProduct,
      Integer expandCategoryLevels, Boolean expandVariants);

  @PreAuthorize("@veloxAuthorizationEvaluator.hasGlobalAdminAuthority(authentication)")
  CategoryDto createCategory(String catalogCode, CreateCategoryDto categoryDto);

  @PreAuthorize("@veloxAuthorizationEvaluator.hasGlobalAdminAuthority(authentication)")
  void deleteCategoryByCatalogCodeAndCode(String catalogCode, String code);

  @PreAuthorize("@veloxAuthorizationEvaluator.hasGlobalAdminAuthority(authentication)")
  CategoryDto updateCategory(String catalogCode, String categoryCode,
      UpdateCategoryDto updateCategoryDto);
}
