package shop.velox.catalog.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import shop.velox.catalog.dto.catalog.CatalogDto;
import shop.velox.catalog.dto.catalog.CreateCatalogDto;
import shop.velox.catalog.dto.catalog.UpdateCatalogDto;

public interface CatalogService {

  Page<CatalogDto> getCatalogs(Pageable pageable);

  Optional<CatalogDto> getCatalogByCode(String catalogCode, Integer expandCategoryLevels,
      Boolean expandProduct, Boolean expandVariants);

  @PreAuthorize("@veloxAuthorizationEvaluator.hasGlobalAdminAuthority(authentication)")
  CatalogDto createCatalog(CreateCatalogDto catalogDto);

  @PreAuthorize("@veloxAuthorizationEvaluator.hasGlobalAdminAuthority(authentication)")
  void deleteCatalogByCode(String catalogCode);

  @PreAuthorize("@veloxAuthorizationEvaluator.hasGlobalAdminAuthority(authentication)")
  CatalogDto updateCatalog(String catalogCode, UpdateCatalogDto catalogDto);

}
